/*
 * MCMCCounters.h
 *
 *  Created on: Nov 30, 2018
 *      Author: phaentu
 */

#ifndef MCMCCOUNTERS_H_
#define MCMCCOUNTERS_H_

#include "stringFunctions.h"
#include <sys/time.h>

class MCMCCounter{
public:
	long numUpdates;
	long numUpdatesAccepted;

	MCMCCounter(){
		numUpdates = 0;
		numUpdatesAccepted = 0;
	};

	void add(const bool & accepted){
		++numUpdates;
		numUpdatesAccepted += accepted;
	};

	int getPercentAcceptanceRate(){
		double x = (double) numUpdatesAccepted / (double) numUpdates;
		return (int) 100 * x;
	};

	std::string getPercentAcceptanceRateString(){
		return toString(getPercentAcceptanceRate()) + "%";
	};
};

class MCMCcounters{
public:
	double numIterations;
	double numLoci;
	double iteration;

	MCMCCounter N;
	MCMCCounter error;
	MCMCCounter mutation;
	MCMCCounter isNeutral;
	MCMCCounter s;
	MCMCCounter h;
	MCMCCounter tau;

	int prog, oldProg;
	struct timeval start, end;
	int runtime;

	MCMCcounters(long & NumIterations, int NumLoci){
		numIterations = NumIterations;
		numLoci = NumLoci;
		iteration = 0;
		prog = 0;
		oldProg = 0;
		runtime = 0;
		gettimeofday(&start, NULL);
	};
	~MCMCcounters(){};

	int getPercent(double x){
		return (int) 100 * x;
	}

	bool printProgress(long & Iteration){
		iteration = Iteration;
		prog = getPercent(iteration / numIterations);
		if(prog>oldProg){
			oldProg = prog;
			return true;
		} else return false;
	};

	std::string progress(){
		return toString(prog) + "%";
	};

	std::string elapsedMinutes(){
		gettimeofday(&end, NULL);
		runtime = ((int) (100*(float) (end.tv_sec  - start.tv_sec) / 60.0));
		return toString(runtime / 100.0);
	};
};



#endif /* MCMCCOUNTERS_H_ */
