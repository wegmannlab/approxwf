/*
 * TTrajectory.h
 *
 *  Created on: May 22, 2014
 *      Author: wegmannd
 */

#ifndef TTRAJECTORY_H_
#define TTRAJECTORY_H_

#include "TTransitionMatrix.h"
#include "stringFunctions.h"


class TTrajectoryStorage{
public:

	TTrajectoryStorage(int & Time, double & SelSize, double NeuSize){
		time = Time;
		selectedSize = SelSize;
		neutralSize = NeuSize;
	};
	int time;
	double selectedSize;
	double neutralSize;
};

class TTrajectory{
public:
	double twoN;
	TStatesU* u;
	std::vector<TTrajectoryStorage> storage;
	bool end;
	int zero;
	double zeroDouble;
	int lastState;
	std::vector<TTrajectoryStorage>::iterator iterator;

	TTrajectory(double TwoN, TStatesU* U){
		twoN = TwoN;
		u=U;
		end=true;
		zero = 0;
		zeroDouble = 0.0;
		lastState = -1;
	};

	void addSelectedSizes(int & Time, int state){
		if(state!=lastState){
			double tmp = (*u)[state] * twoN;
			storage.push_back(TTrajectoryStorage(Time, tmp, twoN-tmp));
			lastState = state;
		} else {
			storage.rbegin()->time += Time;
		}
	};

	void begin(){
		iterator=storage.begin();
		if(iterator!=storage.end()) end=false;
	};

	void next(){
		if(!end){
			++iterator;
			if(iterator==storage.end()) end=true;
		}
	};

	double& currentSelectedSize(){
		if(end) return zeroDouble;
		else return iterator->selectedSize;
	}

	double& currentNeutralSize(){
		if(end) return twoN;
		else return iterator->neutralSize;
	}

	int& currentTime(){
		if(end) return zero;
		else return iterator->time;
	};

	bool isEnd(){ return end;}

	void clear(){
		storage.clear();
		end=true;
		lastState = -1;
	};

	void print(){
		int count=0;
		for(iterator=storage.begin(); iterator!=storage.end(); ++iterator){
			std::cout << iterator->time << "\t" << iterator->selectedSize << "\t" << iterator->neutralSize << std::endl;
			count += iterator->time;
		}
		std::cout << "Total time=" << count << std::endl;
	}

	void write(std::string filename){
		std::ofstream out(filename.c_str());
		for(iterator=storage.begin(); iterator!=storage.end(); ++iterator){
			out << iterator->time << "\t" << iterator->selectedSize << "\t" << iterator->neutralSize << std::endl;
		}
		out.close();
	}
};


#endif /* TTRAJECTORY_H_ */
