/*
 * TLocus.h
 *
 *  Created on: Jun 19, 2015
 *      Author: wegmannd
 */

#ifndef TLOCUS_H_
#define TLOCUS_H_

#include "THMM.h"
#include <limits>
#include "MCMCCounters.h"


class WFParamPriors{
private:
	TRandomGenerator* randomGenerator;

	//N
	bool estimateN;
	double logN_cur, logN_old;
	int N_cur, twoN_cur;
	double logN_min, logN_max, logN_step, logN_oneOverDiff;

	//S
	bool estimateS;
	double s_mean, s_sd, s_step, s_minusTwoSdSquare;
	double s_pi, logRatio_pi_OneMinusPi; //prior probability that s != 0
	bool _doSModelChoice = true;
	double propRateSToZero, logPropRateSToZero;
	double propSFromZero_sd, propS_minusTwoSdSquare; //sd with which a move 0 _> s is proposed
	double log_s_sd, log_propSFromZero_sd;

	//h
	bool estimateH;
	double h_mean, h_sd, h_step, h_minusTwoSdSquare;

	//tau
	bool estimateTau;
	int tau_step;

	//error
	bool estimateErrorRate;
	double errorRate_cur, logErrorRate_min, logErrorRate_max, logErrorRate_step, logErrorRate_cur, logErrorRate_old;

	//mutations
	bool estimateMutationRate;
	double mutationRate_cur, logMutationRate_min, logMutationRate_max, logMutationRate_step, logMutationRate_cur, logMutationRate_old;

public:
	WFParamPriors(TParameters* myParameters, TRandomGenerator* RandomGenerator, TLog* logfile);
	void sampleN();
	void updateN();
	void resetN();
	void setN(double value);
	int& currentN(){return N_cur;};
	int& currentTwoN(){return twoN_cur;};
	double& currentLogN(){return logN_cur;};
	double& currentErrorRate(){return errorRate_cur;};
	double& currentLogErrorRate(){return logErrorRate_cur;};
	double& currentMutationRate(){return mutationRate_cur;};
	double& currentLogMutationRate(){return logMutationRate_cur;};
	double sampleS();
	double sampleH();
	void updateS(double & cur);
	void updateH(double & cur);
	double sampleErrorRate();
	void updateErrorRate();
	void resetErrorRate();
	void setErrorRate(double value);
	double sampleMutationRate();
	void updateMutationRate();
	void resetMutationRate();
	void setMutationRate(double value);
	bool& sIsEstimated(){return estimateS;};
	bool& doSModelChoice(){return _doSModelChoice;};
	bool& NIsEstimated(){return estimateN;};
	bool& hIsEstimated(){return estimateH;};
	bool& tauIsEstimated(){return estimateTau;};
	void updateTau(int & cur, int boundary);
	bool& errorRateIsEstimated(){return estimateErrorRate;};
	bool& mutationRateIsEstimated(){return estimateMutationRate;};
	double& get_s_pi(){ return s_pi; };
	double getLogPriorDensityForHastingsS(double & s);
	double getLogPriorDensityForHastingsH(double & h);
	double& getLogRatio_pi_OneMinusPi(){ return logRatio_pi_OneMinusPi; };
	double& getPropRateSToZero(){ return propRateSToZero; };
	double& getLogPropRateSToZero(){ return logPropRateSToZero; };
	double proposeSFromZero(){ return randomGenerator->getNormalRandom(0.0, propSFromZero_sd); };
	double getLogDensityProposedSFromZero(double & s);
};

class TLocus{
private:
	TRandomGenerator* randomGenerator;
	TTransitionMatrix* transMat;
	TTransitionMatrix* transMat_old;
	TTransitionMatrix* transMatNeutral;
	THMM* hmm;
	double curLL, oldLL;
	bool transMatInitialized, oldTransMatInitialized, HMMInitialized;
	bool transMatStorageInitialized, oldTransMatStorageInitialized;
	bool _knownToBeNeutral;
	bool isNeutral, isNeutral_old, s_wasUpdated;
	double s_cur, h_cur, s_old, h_old, mut_cur;
	int tau_cur, tau_old;
	int twoN;
	double priorDensS_cur, priorDensS_old;
	double priorDensH_cur, priorDensH_old;
	double randomForHastings;
	bool MCMCUpdateAccepted;
	bool estimateTau;

public:
	std::string name;
	TTimePointVector timePoints;

	TLocus(const std::string & Name, TRandomGenerator* RandomGenerator);
	TLocus(TLocus&& other); //Explicitly define move constructor
	~TLocus(){
		if(transMatStorageInitialized) delete transMat;
		if(oldTransMatStorageInitialized) delete transMat_old;
		if(HMMInitialized) delete hmm;
	};

	void setNextTimePointIsFirstInSeries(){ timePoints.setNextIsFirstInSeries(); };
	void setTimePointAsFirstInSeries(int TimePoint){ timePoints.setAsFirstInSeries(TimePoint); };
	void addTimepoint(int & TimePoint, std::string & Samples);
	void addTimepoint(int & TimePoint, int & numA, int & size);
	int getMinNumTimepointsAllelesAreObserved(double & minFreq);
	void setWFParams(int & TwoN, double & s, double & h, double & mutationRate);
	void initializeTransitionMatrix(TStatesU & u, double & gammaThreshold);
	void initializeTransitionMatrixPair(TStatesU & u, double & gammaThreshold);
	void clearTransitionMatrix();
	void setTransitionMatrix(TTransitionMatrix* TransMat);
	TTransitionMatrix* getTransitionMatrixPointer(){ return transMat; };
	void initializeTimepoints();
	void setAsKnownToBeNeutral(){ _knownToBeNeutral = true; };
	bool knownToBeNeutral(){ return _knownToBeNeutral; };
	void initialize(int & TwoN, double & s, double & h, double & mutationRate, TStatesU & u, double & gammaThreshold, double & errorRate);
	void initialize(int & TwoN, double & s, double & h, double & mutationRate, int & mutTime, TStatesU & u, double & gammaThreshold, double & errorRate);
	void initialize(TTransitionMatrix* TransMat, int & TwoN, double & s, double & h, double & mutationRate, TStatesU & u, double & gammaThreshold, double & errorRate);
	void initialize(WFParamPriors* priors, TStatesU & u, double & gammaThreshold, TTransitionMatrix* TransMatNeutral);
	void initializeHMM();
	void sampleNewSH(WFParamPriors* priors);
	void addUpdateToAcceptanceCounter(MCMCCounter & counters);
	void updateSForMCMCStep(WFParamPriors* priors);
	void addSHUpdateToAcceptanceCounter(MCMCCounter & counters);
	void updateHForMCMCStep(WFParamPriors* priors);
	void updateTransitionMatrixAndCalcLL();
	void resetTransitionMatrix();
	void conductSUpdateMCMCStep();
	void conductHUpdateMCMCStep();
	bool& lastMCMCUpdateAccepted(){return MCMCUpdateAccepted;};
	void updateTauForMCMCStep(WFParamPriors* priors);
	void conductTauUpdateMCMCStep();
	void updateN(int & TwoN);
	void setN(int & TwoN);
	void resetN(int & TwoN);
	void set_s(double & s);
	void updateErrorRate(TStatesU & u, double & errorRate);
	void resetErrorRate();
	void updateMutationRate(double & mutationRate);
	void resetMutationRate(double & mutationRate);
	void calcLogLikelihood();
	double& calcAndGetLogLikelihood();
	double& currentLikelihood(){return curLL;};
	double getLogLikelihoodDiff();

	void writeS(std::ofstream & out);
	void writeH(std::ofstream & out);
	void writeTau(std::ofstream & out);
	void simulateData(int & initialState, double & errorRate, TStatesU & u);
	void simulateDataWF(double & initialFreq, double & errorRate, double & mutationRate);
	void printTransMat(){transMat->printTransitionMatrix(1);};
	void printParams(){std::cout << "2N = " << twoN << ", s = " << s_cur << ", h = " << h_cur << std::endl;};
	void calculateStats(TStatSettings & settings);
	double get_fsi();
	double get_fsd();
	void writeStats(TStatSettings & settings, std::ofstream & file);
	void writeStatHeader(std::ofstream & file);
	void writeStatHeaderNoName(std::ofstream & file);
};


#endif /* TLOCUS_H_ */
