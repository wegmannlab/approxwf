/*
 * TApproxWF.h
 *
 *  Created on: Feb 12, 2014
 *      Author: wegmannd
 */

#ifndef TAPPROXWF_H_
#define TAPPROXWF_H_

#include "TParameters.h"
#include "TLog.h"
#include "TRandomGenerator.h"
#include "TLoci.h"
#include "stringFunctions.h"
#include <stdio.h>
#include <math.h>
#include <sys/time.h>

using namespace std;


class TApproxWF{
public:
	TLog* logfile;
	TParameters* myParameters;
	TRandomGenerator* randomGenerator;
	float* u;
	bool uInitialized;

	TApproxWF(TParameters & Params, TLog* Logfile);
	~TApproxWF(){
		if(uInitialized) delete[] u;
		delete randomGenerator;
	};
	void simulateTrajectories();
	void simulateTransitionTimeApprox(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout);
	void simulateTransitionTimeMalaspinas(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout);
	void simulateTransitionTimeWF(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout);
	void simulateTransitionTimeDiffusion(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout);
	void simulateTransitionTimeDiffusionKimura(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout);
	void test();
	void runHMMInference();
	void simulateData();
	void calcStats();
	void convertToWFABC();
	void calcLikelihoodSurface(std::string param);
	void writeTransitionMatrix();
	void sampleTrajectories();
};



#endif /* TAPPROXWF_H_ */
