/*
 * TLocus.cpp
 *
 *  Created on: Jun 19, 2015
 *      Author: wegmannd
 */

#include "TLocus.h"

//---------------------------------------------------------------------------------------------------
//WFParamPriors
//---------------------------------------------------------------------------------------------------
WFParamPriors::WFParamPriors(TParameters* myParameters, TRandomGenerator* RandomGenerator, TLog* logfile){
	randomGenerator = RandomGenerator;
	logfile->startIndent("Initializing priors:");

	//N
	if(myParameters->parameterExists("N")){
		estimateN = false;
		N_cur = myParameters->getParameterDouble("N");
		if(N_cur < 10) throw "N has to be at least 10!";
		logN_cur = log10(N_cur);
		logN_old = logN_cur;
		twoN_cur = 2.0 * N_cur;
		logfile->list("N = " + toString(N_cur));

	} else {
		estimateN = true;
		logN_min = myParameters->getParameterDoubleWithDefault("logN_min", 1.0);
		if(logN_min < 1) throw "logN_min has to be at least 1!";
		logN_max = myParameters->getParameterDoubleWithDefault("logN_max", 8.0);
		if(logN_min > logN_max) throw "logN_min has to be smaller than logN_max!";
		double logN_max_limit = log10(std::numeric_limits<int>::max());
		if(logN_max > logN_max_limit ) throw "logN_max has to be smaller than the largest value this computer can handle ("+ toString(logN_max_limit) +")!";

		logN_oneOverDiff = 1.0 / ((logN_min - logN_max)*log(10.0));
		logN_step = myParameters->getParameterDoubleWithDefault("logN_step", 0.05);
		if(logN_step <= 0.0) throw "logN_step has to be larger than 0!";
		sampleN();
		logfile->list("log10(N) ~ U(" + toString(logN_min) + "," + toString(logN_max) + ")");
	}

	//s
	//check if we have a fixed s
	if(myParameters->parameterExists("s")){
		estimateS = false;
		s_mean = myParameters->getParameterDouble("s");
		logfile->list("s = " + toString(s_mean));
		if(s_mean < -1.0 || s_mean > 1.0) throw "s has to be between -1 and 1!";
		s_pi = 0.5; //not used
		s_sd = 0.001;
		propRateSToZero = 0.1;
		propSFromZero_sd = 0.01;
	} else {
		//estimate s
		estimateS = true;
		s_pi = myParameters->getParameterDoubleWithDefault("pi", 0.1);
		s_mean = myParameters->getParameterDoubleWithDefault("s_mean", 0.0);
		if(s_mean < -1.0 || s_mean > 1.0) throw "s_mean has to be between -1 and 1!";
		s_sd = myParameters->getParameterDoubleWithDefault("s_sd", 0.1);
		if(s_sd <= 0.0) throw "s_sd has to be larger than 0!";

		if(myParameters->parameterExists("s_step"))
			s_step = myParameters->getParameterDouble("s_step");
		else s_step = 0.1 * s_sd;
		if(s_step <= 0.0) throw "s_step has to be larger than 0!";

		//model choice
		_doSModelChoice = !myParameters->parameterExists("noModelChoice");
		if(_doSModelChoice){
			logfile->startIndent("Will perform model choice between neutral and selected model using the following proposals (turn off with 'noModelChoice'):");
			propRateSToZero = myParameters->getParameterDoubleWithDefault("propSToZero", 0.1);
			propSFromZero_sd = myParameters->getParameterDoubleWithDefault("propSFromZero_sd", 0.01);
			logfile->list("P(s!=0) = " + toString(s_pi));
			logfile->list("s|s!=0 ~ N(" + toString(s_mean) + "," + toString(s_sd) + "), bounded within -1 and 1");
			logfile->endIndent();
		} else {
			logfile->list("Will not perform model choice and assume all loci are in the selected model. (parameter 'noModelChoice')");
		}
	}

	//calc tmp variables for s
	s_minusTwoSdSquare = - 2.0 * s_sd * s_sd;
	logRatio_pi_OneMinusPi = log(s_pi / (1.0 - s_pi));
	logPropRateSToZero = log(propRateSToZero);
	propS_minusTwoSdSquare = - 2.0 * propSFromZero_sd * propSFromZero_sd;
	log_s_sd = log(s_sd);
	log_propSFromZero_sd = log(propSFromZero_sd);

	//h
	if(myParameters->parameterExists("h")){
		estimateH = false;
		h_mean = myParameters->getParameterDouble("h");
		h_sd = 0.0;
		h_minusTwoSdSquare = 0.0;
		h_step = 0.0;
		logfile->list("h = " + toString(h_mean));
	} else {
		//estimate h
		estimateH = true;
		h_mean = myParameters->getParameterDoubleWithDefault("h_mean", 0.5);
		if(h_mean < 0.0 || h_mean > 1.0) throw "h_mean has to be between 0 and 1!";
		h_sd = myParameters->getParameterDoubleWithDefault("h_sd", 0.1);
		if(h_sd <= 0.0) throw "h_sd has to be larger than 0!";
		h_minusTwoSdSquare = - 2.0 * h_sd * h_sd;
		if(myParameters->parameterExists("h_step"))
			h_step = myParameters->getParameterDouble("h_step");
		else h_step = 0.1 * h_sd;
		if(h_step <= 0.0) throw "h_step has to be larger than 0!";
		logfile->list("h ~ N(" + toString(h_mean) +"," + toString(h_sd) + "), bounded within 0 and 1");
	}

	//error rate
	if(myParameters->parameterExists("estimateErrorRate") || myParameters->parameterExists("logErrorRate_min") || myParameters->parameterExists("logErrorRate_max") || myParameters->parameterExists("logErrorRate_step")){
		estimateErrorRate = true;
		logErrorRate_min = myParameters->getParameterDoubleWithDefault("logErrorRate_min", -4.0);
		if(logErrorRate_min < -20.0) throw "log10(error rate) has to be larger than -20!";
		logErrorRate_max = myParameters->getParameterDoubleWithDefault("logErrorRate_max", -0.3);
		if(logErrorRate_max > 0.3) throw "log10(error rate) has to be smaller than 0.3!";
		if(logErrorRate_max < logErrorRate_min) throw "Prior minimum > maximum for log10(error rate)!";
		logErrorRate_step = myParameters->getParameterDoubleWithDefault("logErrorRate_step", 0.01);
		if(logErrorRate_step <= 0.0) throw "Updating step for log10(error rate) has to be larger than 0.0!";
		if(logErrorRate_step > (logErrorRate_max - logErrorRate_min) / 2.0) throw "Updating step for log10(error rate) has to be less than half of the prior interval!";
		logfile->list("log10(error rate) ~ U(" + toString(logErrorRate_min) + ", " + toString(logErrorRate_max) + ")");
		logErrorRate_cur =  -2.0;
		sampleErrorRate();
		errorRate_cur = pow(10, logErrorRate_cur);
	} else {
		estimateErrorRate = false;
		errorRate_cur = myParameters->getParameterDoubleWithDefault("errorRate", 0.0);
		logfile->list("error rate = " + toString(errorRate_cur));
		if(errorRate_cur > 0.5 || errorRate_cur < 0.0) throw "Error rate has to be between 0.0 and 0.5!";
		logErrorRate_cur = log10(errorRate_cur);
		//set unused parameters
		logErrorRate_min = -1.0;
		logErrorRate_max = -1.0;
		logErrorRate_step = -1.0;
		logErrorRate_old = logErrorRate_cur;
	}

	//mutation rate
	if(myParameters->parameterExists("estimateMutRate") || myParameters->parameterExists("logMutRate_min") || myParameters->parameterExists("logMutRate_max") || myParameters->parameterExists("logMutRate_step")){
		estimateMutationRate = true;
		logMutationRate_min = myParameters->getParameterDoubleWithDefault("logMutRate_min", -6.0);
		if(logMutationRate_min < -20.0) throw "log10(mutation rate) has to be larger than -20!";
		logMutationRate_max = myParameters->getParameterDoubleWithDefault("logMutRate_max", -1.0);
		if(logMutationRate_max > -1.0) throw "log10(mutation rate) has to be smaller than -1.0!";
		if(logMutationRate_max < logMutationRate_min) throw "Prior minimum > maximum for log10(mutation rate)!";
		logMutationRate_step = myParameters->getParameterDoubleWithDefault("logMutRate_step", 0.01);
		if(logMutationRate_step <= 0.0) throw "Updating step for log10(mutation rate) has to be larger than 0!";
		if(logMutationRate_step > (logMutationRate_max - logMutationRate_min) / 2.0) throw "Updating step for log10(mutation rate) has to be less than half of the prior interval!";
		logfile->list("log10(error rate) ~ U(" + toString(logMutationRate_min) + ", " + toString(logMutationRate_max) + ")");
		logMutationRate_cur =  -2.0;
		sampleMutationRate();
		mutationRate_cur = pow(10, logMutationRate_cur);
	} else {
		estimateMutationRate = false;
		mutationRate_cur = myParameters->getParameterDouble("mutRate", 0.0);
		logfile->list("mutation rate = " + toString(mutationRate_cur));
		if(mutationRate_cur > 1.0 || mutationRate_cur < 0.0) throw "Mutation rate has to be between 0.0 and 1.0!";
		logMutationRate_cur = log10(mutationRate_cur);
		//set unused parameters
		logMutationRate_min = -1.0;
		logMutationRate_max = -1.0;
		logMutationRate_step = -1.0;
		logMutationRate_old = logMutationRate_cur;
	}

	//tau
	estimateTau = myParameters->parameterExists("estimateTau");
	if(estimateTau){
		tau_step = myParameters->getParameterDoubleWithDefault("tau_step", 10);
		if(tau_step <= 0) throw "tau_step has to be larger than 0!";
		logfile->list("tau ~ U[-inf, first observation]");
	}

	logfile->endIndent();

	//report updates
	logfile->startIndent("Updating parameters during MCMC as:");
	if(estimateN)
		logfile->list("log10(N') ~ Norm(log10(N), " + toString(logN_step) + "), mirrored at " + toString(logN_min) + " and " + toString(logN_max));
	if(estimateS){
		logfile->list("If s!= 0, s' ~ Norm(s, " + toString(s_step) + ") with probability " + toString(propRateSToZero) + ",  mirrored at -1 and 1, and s'=0 otherwise.");
		logfile->list("If s==0, s' ~ Norm(0.0, " + toString(propSFromZero_sd) + "),  mirrored at -1 and 1, and s'=0 otherwise.");
	}
	if(estimateH) logfile->list("h' ~ Norm(h, " + toString(h_step) + "), mirrored at 0 and 1");
	if(estimateErrorRate) logfile->list("log10(error rate') ~ Norm(log10(error rate), " + toString(logErrorRate_step) + "), mirrored at " + toString(logErrorRate_min) + " and " + toString(logErrorRate_max));
	if(estimateMutationRate) logfile->list("log10(mutation rate') ~ Norm(log10(mutation rate), " + toString(logMutationRate_step) + "), mirrored at " + toString(logMutationRate_min) + " and " + toString(logMutationRate_max));
	if(estimateTau) logfile->list("tau' ~ U[tau - " + toString(tau_step) + ", tau + " + toString(tau_step) + "], mirrored at the first observation");
	logfile->endIndent();
}

//--------- N ---------
void WFParamPriors::sampleN(){
	logN_old = logN_cur;
	logN_cur = randomGenerator->getRand(logN_min, logN_max);
	N_cur = pow(10, logN_cur);
	twoN_cur = 2.0 * N_cur;
}

void WFParamPriors::updateN(){
	logN_old = logN_cur;
	logN_cur = randomGenerator->getNormalRandom(logN_old, logN_step);

	if(logN_cur < logN_min) logN_cur = 2*logN_min - logN_cur;
	if(logN_cur > logN_max) logN_cur = 2*logN_max - logN_cur;
	N_cur = pow(10, logN_cur);
	twoN_cur = 2.0 * N_cur;
}

void WFParamPriors::resetN(){
	logN_cur = logN_old;
	N_cur = pow(10, logN_cur);
	twoN_cur = 2.0 * N_cur;
}

void WFParamPriors::setN(double value){
	logN_old = logN_cur;
	logN_cur = value;
	N_cur = pow(10, logN_cur);
	twoN_cur = 2.0 * N_cur;
}

//--------- s ---------
double WFParamPriors::sampleS(){
	if(!estimateS) return s_mean;
	else {
		double s = 2;
		while(s < -0.999999 || s > 0.999999)
			s = randomGenerator->getNormalRandom(s_mean, s_sd);
		return s;
	}
}

void WFParamPriors::updateS(double & cur){
	cur = randomGenerator->getNormalRandom(cur, s_step);
	if(cur < -0.999999) cur = - cur - 1.999998;
	if(cur > 0.999999) cur = 1.999998 - cur;
}

double WFParamPriors::getLogPriorDensityForHastingsS(double & s){
	//s -> is normally distributed with mean zero. The term 1/sqrt(2pi) is not relevant for the hastings ratio
	//and neither is the normalization due to truncation
	return (s - s_mean) * (s - s_mean) / s_minusTwoSdSquare - log_s_sd;
};

double WFParamPriors::getLogDensityProposedSFromZero(double & s){
	return s * s / propS_minusTwoSdSquare - log_propSFromZero_sd;
};

//--------- h ---------
double WFParamPriors::sampleH(){
	if(!estimateH) return h_mean;
	else {
		double h = -2.0;
		while(h < -1.0 || h > 1.0) h = randomGenerator->getNormalRandom(h_mean, h_sd);
		return h;
	}
}

void WFParamPriors::updateH(double & cur){
	if(estimateH){
		cur = randomGenerator->getNormalRandom(cur, h_step);
		//mirror!
		if(cur < -1.0) cur = -2.0 - cur;
		if(cur > 1.0) cur = 2.0 - cur;
	}
}

double WFParamPriors::getLogPriorDensityForHastingsH(double & h){
	//h -> is normally distributed. The term 1/(sigma*sqrt(2pi)) is not relevant for the hastings ratio
	//and neither is the normalization due to truncation
	return (h-h_mean)*(h-h_mean) / h_minusTwoSdSquare;
}

//--------- errorRate ---------
double WFParamPriors::sampleErrorRate(){
	logErrorRate_old = logErrorRate_cur;
	logErrorRate_cur = randomGenerator->getRand(logErrorRate_min, logErrorRate_max);
	errorRate_cur = pow(10, logErrorRate_cur);
	return errorRate_cur;
}

void WFParamPriors::updateErrorRate(){
	logErrorRate_old = logErrorRate_cur;

	//propose huge step once in a while
	if(randomGenerator->getRand() < 0.2){
		logErrorRate_cur = randomGenerator->getRand(logErrorRate_min, logErrorRate_max);
	}
	else logErrorRate_cur = randomGenerator->getNormalRandom(logErrorRate_old, logErrorRate_step);
	if(logErrorRate_cur < logErrorRate_min) logErrorRate_cur = logErrorRate_min + (logErrorRate_min - logErrorRate_cur);
	if(logErrorRate_cur > logErrorRate_max) logErrorRate_cur = logErrorRate_max - (logErrorRate_cur - logErrorRate_max);
	errorRate_cur = pow(10.0, logErrorRate_cur);
}

void WFParamPriors::resetErrorRate(){
	logErrorRate_cur = logErrorRate_old;
	errorRate_cur = pow(10.0, logErrorRate_cur);
}

void WFParamPriors::setErrorRate(double value){
	logErrorRate_old = logErrorRate_cur;
	logErrorRate_cur = value;
	errorRate_cur = pow(10.0, logErrorRate_cur);
}

//--------- mutationRate ---------
double WFParamPriors::sampleMutationRate(){
	logMutationRate_old = logMutationRate_cur;
	logMutationRate_cur = randomGenerator->getRand(logMutationRate_min, logMutationRate_max);
	mutationRate_cur = pow(10, logMutationRate_cur);
	return mutationRate_cur;
}

void WFParamPriors::updateMutationRate(){
	logMutationRate_old = logMutationRate_cur;
	logMutationRate_cur = randomGenerator->getNormalRandom(logMutationRate_cur, logMutationRate_step);
	if(logMutationRate_cur < logMutationRate_min) logMutationRate_cur = logMutationRate_min + (logMutationRate_min - logMutationRate_cur);
	if(logMutationRate_cur > logMutationRate_max) logMutationRate_cur = logMutationRate_max - (logMutationRate_cur - logMutationRate_max);
	mutationRate_cur = pow(10, logMutationRate_cur);
}

void WFParamPriors::resetMutationRate(){
	logMutationRate_cur = logMutationRate_old;
}

void WFParamPriors::setMutationRate(double value){
	logMutationRate_old = logMutationRate_cur;
	logMutationRate_cur = value;
}

//--------- Tau ---------
void WFParamPriors::updateTau(int & cur, int boundary){
	cur = randomGenerator->getRand(cur - tau_step, cur + tau_step + 1);
	//mirror!
	if(cur > boundary) cur = 2*boundary - cur;
}

//-------------------------------------------------------
//Tlocus
//-------------------------------------------------------
TLocus::TLocus(const std::string & Name, TRandomGenerator* RandomGenerator) :
randomGenerator(RandomGenerator),
transMat(NULL),
transMat_old(NULL),
transMatNeutral(NULL),
hmm(NULL),
curLL(0.0),
oldLL(0.0),
transMatInitialized(false),
oldTransMatInitialized(false),
HMMInitialized(false),
transMatStorageInitialized(false),
oldTransMatStorageInitialized(false),
_knownToBeNeutral(false),
isNeutral(true),
isNeutral_old(true),
s_wasUpdated(true),
s_cur(0.0),
h_cur(0.5),
s_old(0.0),
h_old(0.5),
mut_cur(0.0),
tau_cur(0.0),
tau_old(0.0),
twoN(0.0),
priorDensS_cur(0.0),
priorDensS_old(0.0),
priorDensH_cur(0.0),
priorDensH_old(0.0),
randomForHastings(0.0),
MCMCUpdateAccepted(false),
estimateTau(false),
name(Name),
timePoints(){}


TLocus::TLocus(TLocus&& other){
	//pilfer resources
	randomGenerator = other.randomGenerator;
	transMat = other.transMat;
	transMat_old = other.transMat_old;
	transMatNeutral = other.transMatNeutral;
	hmm = other.hmm;

	curLL = other.curLL;
	oldLL = other.oldLL;
	transMatInitialized = other.transMatInitialized;
	oldTransMatInitialized = other.oldTransMatInitialized;
	HMMInitialized = other.HMMInitialized;
	transMatStorageInitialized = other.transMatStorageInitialized;
	oldTransMatStorageInitialized = other.oldTransMatStorageInitialized;

	_knownToBeNeutral = other._knownToBeNeutral;
	isNeutral = other.isNeutral;
	isNeutral_old = other.isNeutral_old;
	s_wasUpdated = other.s_wasUpdated;
	s_cur = other.s_cur;
	h_cur = other.h_cur;
	s_old = other.s_old;
	h_old = other.h_old;
	mut_cur = other.mut_cur;
	tau_cur = other.tau_cur;
	tau_old = other.tau_old;

	twoN = other.twoN;
	priorDensS_cur = other.priorDensS_cur;
	priorDensS_old = other.priorDensS_old;
	priorDensH_cur = other.priorDensH_cur;
	priorDensH_old = other.priorDensH_old;
	randomForHastings = other.randomForHastings;
	MCMCUpdateAccepted = other.MCMCUpdateAccepted;
	estimateTau = other.estimateTau;

	name = std::move(other.name);
	timePoints = std::move(other.timePoints);

	//set other to default state
	other.randomGenerator = nullptr;
	other.transMat = nullptr;
	other.transMat_old = nullptr;
	other.hmm = nullptr;

	other.curLL = 0.0;
	other.oldLL = 0.0;
	other.transMatInitialized = false;
	other.oldTransMatInitialized = false;
	other.HMMInitialized = false;
	other.transMatStorageInitialized = false;
	other.oldTransMatStorageInitialized = false;

	other._knownToBeNeutral = false;	
	other.isNeutral = true;
	other.isNeutral_old = true;
	other.s_cur = 0.0;
	other.h_cur = 0.5;
	other.s_old = 0.0;
	other.h_old = 0.5;
	other.mut_cur = 0.0;
	other.tau_cur = 0.0;
	other.tau_old = 0.0;

	other.twoN = 0.0;
	other.priorDensS_cur = 0.0;
	other.priorDensS_old = 0.0;
	other.priorDensH_cur = 0.0;
	other.priorDensH_old = 0.0;
	other.randomForHastings = 0.0;
	other.MCMCUpdateAccepted = 0.0;
	other.estimateTau = 0.0;
};

void TLocus::addTimepoint(int & TimePoint, std::string & Samples){
	timePoints.addTimepoint(TimePoint, Samples);
};

void TLocus::addTimepoint(int & TimePoint, int & numA, int & size){
	timePoints.addTimepoint(TimePoint, numA, size);
};

int TLocus::getMinNumTimepointsAllelesAreObserved(double & minFreq){
	//count for each allele the number of timepoints their counts are > 0
	//return minimum of the two alleles
	return(timePoints.getMinNumTimepointsAllelesAreObserved(minFreq));
}

void TLocus::setWFParams(int & TwoN, double & s, double & h, double & mutationRate){
	twoN = TwoN;
	s_cur = s;
	h_cur = h;
	mut_cur = mutationRate;
	isNeutral = false;
}
void TLocus::initializeTransitionMatrix(TStatesU & u, double & gammaThreshold){
	transMat = new TTransitionMatrixApprox(twoN, s_cur, h_cur, mut_cur, &u, gammaThreshold, randomGenerator);
	transMatInitialized = true;
	transMatStorageInitialized = true;
}
void TLocus::initializeTransitionMatrixPair(TStatesU & u, double & gammaThreshold){
	transMat = new TTransitionMatrixApprox(twoN, s_cur, h_cur, mut_cur, &u, gammaThreshold, randomGenerator);
	transMatInitialized = true;
	transMatStorageInitialized = true;
	transMat_old = new TTransitionMatrixApprox(&u, gammaThreshold, randomGenerator);
	oldTransMatInitialized = true;
	oldTransMatStorageInitialized = true;
}
void TLocus::clearTransitionMatrix(){
	if(transMatInitialized){
		transMat->clear();
	}
	if(oldTransMatInitialized){
		transMat_old->clear();
	}
}
void TLocus::setTransitionMatrix(TTransitionMatrix* TransMat){
	if(transMatStorageInitialized) delete transMat;
	transMat = TransMat;
	transMatInitialized = true;
}

void TLocus::initializeTimepoints(){
	timePoints.initDeltaT();
	if(!timePoints.hasSamples()) throw "Locus '" + name + "' has no time point with samples!";
}
void TLocus::initialize(int & TwoN, double & s, double & h, double & mutationRate, TStatesU & u, double & gammaThreshold, double & errorRate){
	setWFParams(TwoN, s, h, mutationRate);
	initializeTimepoints();
	timePoints.prepareEmissionProbabilities(&u, errorRate, randomGenerator);
	initializeTransitionMatrix(u, gammaThreshold);
}
void TLocus::initialize(TTransitionMatrix* TransMat, int & TwoN, double & s, double & h, double & mutationRate, TStatesU & u, double & gammaThreshold, double & errorRate){
	setWFParams(TwoN, s, h, mutationRate);
	initializeTimepoints();
	timePoints.prepareEmissionProbabilities(&u, errorRate, randomGenerator);
	setTransitionMatrix(TransMat);
	//transMatInitialized = true;
}
void TLocus::initialize(int & TwoN, double & s, double & h, double & mutationRate, int & tau, TStatesU & u, double & gammaThreshold, double & errorRate){
	initialize(TwoN, s, h, mutationRate, u, gammaThreshold, errorRate);
	estimateTau = true;
	tau_cur = tau;
};

void TLocus::initialize(WFParamPriors* priors, TStatesU & u, double & gammaThreshold, TTransitionMatrix* TransMatNeutral){
	//time points
	initializeTimepoints();
	timePoints.prepareEmissionProbabilities(&u, priors->currentErrorRate(), randomGenerator);

	//set pointer to neutral transition matrix
	transMatNeutral = TransMatNeutral;

	//initialize s and h
	if(priors->sIsEstimated()){
		//initialize neutral
		twoN = priors->currentTwoN();

		//set to neutral
		if(_knownToBeNeutral || (priors->doSModelChoice() && priors->getPropRateSToZero() > 0.0))
			isNeutral = true;
		else
			isNeutral = false;
		s_cur = 0.00;

		priorDensS_cur = priors->getLogPriorDensityForHastingsS(s_cur);
		h_cur = priors->sampleH();
		priorDensH_cur = priors->getLogPriorDensityForHastingsH(h_cur);
		mut_cur = priors->currentMutationRate();
		initializeTimepoints();
		if(priors->tauIsEstimated()){
			estimateTau = true;
			tau_cur = timePoints.getFirstTimePointsWithSamples();
			priors->updateTau(tau_cur, timePoints.getFirstTimePointsWithSamples());
		}
	}

	//initialize transition matrix and HMM
	initializeTransitionMatrixPair(u, gammaThreshold);
	initializeHMM();

	//check if LL is nan, and if yes, choose new s and h
	calcAndGetLogLikelihood();
	int failed = 0;
	while(isNanOrInf(curLL)){
		isNeutral = false;
		if(failed == 20)
			throw "Failed to initialize locus '" + name + "', got LL = nan for 20 random starting values!";
		++failed;
		sampleNewSH(priors);
		calcAndGetLogLikelihood();
	}
};

void TLocus::initializeHMM(){
	if(HMMInitialized) delete hmm;
	if(isNeutral)
		hmm = new THMM(&timePoints, transMatNeutral);
	else
		hmm = new THMM(&timePoints, transMat);
	hmm->init();
	HMMInitialized = true;
};

void TLocus::sampleNewSH(WFParamPriors* priors){
	//this function is called when the initial LL is nan
	s_cur = priors->sampleS();
	priorDensS_cur = priors->getLogPriorDensityForHastingsS(s_cur);
	h_cur = priors->sampleH();
	priorDensH_cur = priors->getLogPriorDensityForHastingsH(h_cur);

	//update transmat
	updateTransitionMatrixAndCalcLL();
};

//----------------------------------------------------------------------
//Functions for MCMC updates
void TLocus::updateSForMCMCStep(WFParamPriors* priors){
	//update parameters and save prior density
	s_old = s_cur;
	isNeutral_old = isNeutral;
	bool doModelJump = randomGenerator->getRand() < priors->getPropRateSToZero();
	//update depending on neutral state
	if(isNeutral){
		if(doModelJump && !_knownToBeNeutral){
			//propose move 0 -> s'
			isNeutral = false;
			s_cur = priors->proposeSFromZero();

			//consists of P(s), P(s!=0) / P(S==0) and q(s -> 0) / q(0 -> s)
			//priorDensS_cur = priors->getLogPriorDensityForHastingsS(s_cur) + priors->getLogRatio_pi_OneMinusPi() + priors->getLogPropRateSToZero() - priors->getLogDensityProposedSFromZero(s_cur);
			priorDensS_cur = priors->getLogPriorDensityForHastingsS(s_cur) + priors->getLogRatio_pi_OneMinusPi() - priors->getLogDensityProposedSFromZero(s_cur);
			priorDensS_old = 0.0;
			s_wasUpdated = true;
		} else s_wasUpdated = false;
	} else {
		if(doModelJump){
			//propose move s -> 0
			isNeutral = true;
			s_cur = 0.0;

			//consists of P(s==0) / P(S!=0) and q(0 -> s) / q(s -> 0)
			//priorDensS_cur = - priors->getLogRatio_pi_OneMinusPi() - priors->getLogPropRateSToZero() + priors->getLogDensityProposedSFromZero(s_old);
			priorDensS_cur = - priors->getLogRatio_pi_OneMinusPi() + priors->getLogDensityProposedSFromZero(s_old);

			//also set old dens to P(s_old)
			priorDensS_old = priors->getLogPriorDensityForHastingsS(s_old);
		} else {
			//propose move s -> s'
			isNeutral = false;
			priors->updateS(s_cur);
			priorDensS_cur = priors->getLogPriorDensityForHastingsS(s_cur);
			priorDensS_old = priors->getLogPriorDensityForHastingsS(s_old);
		}
		s_wasUpdated = true;
	}

	//save random value for hastings to avoid interference when parallelizing
	randomForHastings = randomGenerator->getRand();
};

void TLocus::conductSUpdateMCMCStep(){
	if(s_wasUpdated){
		if(isNeutral)
			calcLogLikelihood();
		else
			updateTransitionMatrixAndCalcLL();

		//check acceptance
		double hastings = curLL + priorDensS_cur - oldLL - priorDensS_old;
		//std::cout << "neutraltransMat = " << transMatNeutral->getColSum(1, 1) << ", hastings s = " << s_old << " -> " << s_cur << " (" << isNeutral_old << " -> " << isNeutral << "), LL = " << oldLL << " -> " << curLL << " = " << curLL - oldLL << "; prior = " << priorDensS_old << " -> " << priorDensS_cur << " = " << priorDensS_cur - priorDensS_old << " ==> " << hastings << std::endl;

		if(log(randomForHastings) < hastings){
			MCMCUpdateAccepted = true;
		} else {
			MCMCUpdateAccepted = false;
			//reset
			if(!isNeutral) resetTransitionMatrix();
			else curLL = oldLL;

			s_cur = s_old;
			isNeutral = isNeutral_old;
		}
	}
};

void TLocus::updateHForMCMCStep(WFParamPriors* priors){
	//update parameters and save prior density
	h_old = h_cur;
	priorDensH_old = priorDensH_cur;
	priors->updateH(h_cur);
	priorDensH_cur = priors->getLogPriorDensityForHastingsH(h_cur);

	//save random value for hastings to avoid interference when parallelizing
	randomForHastings = randomGenerator->getRand();
}

void TLocus::updateTauForMCMCStep(WFParamPriors* priors){
	//update parameters
	tau_old = tau_cur;
	priors->updateTau(tau_cur, timePoints.getFirstTimePointsWithSamples());

	//save random value for hastings to avoid interference when parallelizing
	randomForHastings = randomGenerator->getRand();
}

void TLocus::updateTransitionMatrixAndCalcLL(){
	//save old transition matrix
	TTransitionMatrix* tmp = transMat_old;
	transMat_old = transMat;
	transMat = tmp;

	//update transition matrix
	if(!transMat->updateParameters(twoN, s_cur, h_cur, mut_cur)){
		//matrix exponential failed -> set very bad LL to reject update
		oldLL = curLL;
		curLL = -999999999999999.9;
	} else {
		//calculate new likelihood
		calcLogLikelihood();
	}
};

void TLocus::resetTransitionMatrix(){
	//reset transition matrix
	TTransitionMatrix* tmp = transMat;
	transMat = transMat_old;
	transMat_old = tmp;

	//reset likelihood
	curLL = oldLL;
}

void TLocus::addUpdateToAcceptanceCounter(MCMCCounter & counter){
	counter.add(MCMCUpdateAccepted);
};


void TLocus::addSHUpdateToAcceptanceCounter(MCMCCounter & counter){
	if(!isNeutral) counter.add(MCMCUpdateAccepted);
};

void TLocus::conductHUpdateMCMCStep(){
	double hastings;
	if(isNeutral){
		hastings = priorDensH_cur - priorDensH_old;
	} else {
		updateTransitionMatrixAndCalcLL();
		hastings = curLL + priorDensH_cur - oldLL - priorDensH_old;
	}

	//check acceptance
	if(log(randomForHastings) < hastings){
		MCMCUpdateAccepted = true;
	} else {
		MCMCUpdateAccepted = false;
		//reset
		h_cur = h_old;
		priorDensH_cur = priorDensH_old;
		resetTransitionMatrix();
	}
};

void TLocus::conductTauUpdateMCMCStep(){
	//no need to update transition matrix!
	//calculate new likelihood
	calcLogLikelihood();

	//check acceptance
	double hastings = curLL - oldLL;
	if(log(randomForHastings) > hastings){
		//in case we reject
		tau_cur = tau_old;
		curLL = oldLL;
		MCMCUpdateAccepted = false;
	} else MCMCUpdateAccepted = true;
}

void TLocus::updateN(int & TwoN){
	//save old transition matrix
	if(!isNeutral){
		TTransitionMatrix* tmp = transMat_old;
		transMat_old = transMat;
		transMat = tmp;
		oldLL = curLL;

		//update N
		twoN = TwoN;
		transMat->updateParameters(twoN, s_cur, h_cur, mut_cur);
	}
}

void TLocus::setN(int & TwoN){
	//update N
	twoN = TwoN;
	transMat->updateParameters(twoN, s_cur, h_cur, mut_cur);
}

void TLocus::resetN(int & TwoN){
	if(!isNeutral){
		twoN = TwoN;
		curLL = oldLL;
		//reset transition matrix
		TTransitionMatrix* tmp = transMat;
		transMat = transMat_old;
		transMat_old = tmp;
	}
}

void TLocus::set_s(double & s){
	//update N
	s_cur = s;
	transMat->updateParameters(twoN, s_cur, h_cur, mut_cur);
}

void TLocus::updateErrorRate(TStatesU & u, double & errorRate){
	timePoints.updateEmissionProbabilities(&u, errorRate, randomGenerator);
	oldLL = curLL;
}

void TLocus::resetErrorRate(){
	timePoints.resetEmissionProbabilities();
	curLL = oldLL;
}

void TLocus::updateMutationRate(double & mutationRate){
	if(!isNeutral){
		//save old transition matrix
		TTransitionMatrix* tmp = transMat_old;
		transMat_old = transMat;
		transMat = tmp;
		oldLL = curLL;

		//update transition matrix
		mut_cur = mutationRate;
		transMat->updateParameters(twoN, s_cur, h_cur, mut_cur);
	}
}

void TLocus::resetMutationRate(double & mutationRate){
	if(!isNeutral){
		mut_cur = mutationRate;
		curLL = oldLL;
		//reset transition matrix
		TTransitionMatrix* tmp = transMat;
		transMat = transMat_old;
		transMat_old= tmp;
	}
}

void TLocus::calcLogLikelihood(){
	//save old LL
	oldLL = curLL;

	//run HMM to get Likelihood
	if(isNeutral)
		hmm->reset(transMatNeutral);
	else
		hmm->reset(transMat);
	hmm->setUniformPrior();
	curLL = hmm->getLogLikelihood();
}

double& TLocus::calcAndGetLogLikelihood(){
	calcLogLikelihood();
	return curLL;
}

void TLocus::writeS(std::ofstream & out){
	out << "\t" << s_cur;
}
void TLocus::writeH(std::ofstream & out){
	out << "\t" << h_cur;
}
void TLocus::writeTau(std::ofstream & out){
	out << "\t" << tau_cur;
}
void TLocus::simulateData(int & initialState, double & errorRate, TStatesU & u){
	//initialize transition matrix
	if(!transMatInitialized) throw "Can not simulate data: transition matrix has not been initialized yet!";

	//go through all time points
	timePoints.beginInteration();
	int state = initialState;
	timePoints.currentSimulateData(u[state], errorRate, randomGenerator);

	while(timePoints.nextCheck()){
		//pick next state at random from transition matrix
		state = transMat->simulate(state, timePoints.currentDeltaT());
		//simulate data
		timePoints.currentSimulateData(u[state], errorRate, randomGenerator);
	}
}


void TLocus::simulateDataWF(double & initialFreq, double & errorRate, double & mutationRate){
	//go through all time points
	timePoints.beginInteration();

	//simulate data at first time point
	timePoints.currentSimulateData(initialFreq, errorRate, randomGenerator);

	//prepare simulation
	double onePlusS = (1.0 + s_cur);
	double onePlusSH = 1.0 + s_cur * h_cur;
	double f = initialFreq;
	double oneMinusf, homoNeutral, het, homoSelected, pSucess;
	double twoNDouble = twoN;

	//run simulation
	while(timePoints.nextCheck()){
		//iterate generation by generation to next time point
		for(int i=0; i<timePoints.currentDeltaT(); ++i){
			oneMinusf = 1.0 - f;
			homoNeutral = oneMinusf * oneMinusf;
			homoSelected = onePlusS * f * f;
			het = onePlusSH * f * oneMinusf;
			pSucess = (homoSelected + het) / (homoNeutral + 2*het + homoSelected);
			f = randomGenerator->getBiomialRand(pSucess, twoN) / twoNDouble;
			if(f == 0.0 || f == 1.0){
				if(mutationRate == 0.0) break;
				else {
					if(f == 1.0) f = 1.0 - randomGenerator->getBiomialRand(mutationRate, twoN) / twoN;
					else f = randomGenerator->getBiomialRand(mutationRate, twoN) / twoN;
				}
			}
		}
		//simulate data
		if(mutationRate == 0.0 && (f == 0.0 || f == 1.0)){
			do{
				timePoints.currentSimulateData(f, errorRate, randomGenerator);
			} while(timePoints.nextCheck());
			break;
		} else timePoints.currentSimulateData(f, errorRate, randomGenerator);
	}
}

void TLocus::calculateStats(TStatSettings & settings){
	timePoints.calcStats(settings);
};

double TLocus::get_fsi(){
	return timePoints.get_fsi();
};

double TLocus::get_fsd(){
	return timePoints.get_fsd();
};


void TLocus::writeStats(TStatSettings & settings, std::ofstream & file){
	timePoints.calcStats(settings);
	timePoints.writeStats(file);
}

void TLocus::writeStatHeader(std::ofstream & file){
	timePoints.writeStatHeader(file, name);
}

void TLocus::writeStatHeaderNoName(std::ofstream & file){
	std::string empty = "";
	timePoints.writeStatHeader(file, empty);
}
