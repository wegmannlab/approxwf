/*
 * THMM.cpp
 *
 *  Created on: May 29, 2014
 *      Author: wegmannd
 */

#include "THMM.h"

THMM::THMM(TTimePointVector* TimePoints, TTransitionMatrix* TransMat){
	//time points and transition matrix
	timePoints = TimePoints;
	transMat = TransMat;
	storageInitialized = false;
	init();
	prior = new double[numStates];
	//set switches
	forwardRun=false;
	likelihoodCalculated=false;
	priorSet=false;
	logLikelihood=-1;
}

void THMM::freeForward(){
	for(int i=0; i<storageSize; ++i){
		delete[] forward[i];
	}
	delete[] forward;
	delete[] forwardScale;
};

void THMM::init(){
	if(storageInitialized)
		freeForward();
	numTimePoints = timePoints->numTimePoints;
	forward = new double*[numTimePoints];
	storageSize = numTimePoints;
	numStates = transMat->size();
	//initialize forward variable
	for(int i=0; i<numTimePoints; ++i) forward[i] = new double[numStates];
	forwardScale = new double[numTimePoints];
	logCumulForwardScale = 0.0;
	storageInitialized = true;
	logLikelihood = 0.0;
}

void THMM::reset(TTransitionMatrix* TransMat){
	transMat = TransMat;
	//change sin number of time points
	if(timePoints->numTimePoints > storageSize){
		init();
	} else numTimePoints = timePoints->numTimePoints;
	//reset switches
	forwardRun = false;
	priorSet = false;
	likelihoodCalculated = false;
	logLikelihood = 0.0;
}

void THMM::setUniformPrior(int tp){
	//if(priorSet) throw "THMM: Prior already set! Forgot to clear?";
	double prior = 1.0/(double) numStates;
	double tmp = 0.0;
	for(int k=0; k<numStates; ++k){
		forward[tp][k] = prior * timePoints->getEmissionProb(tp,k);
		tmp += forward[tp][k];
	}
	forwardScale[tp] = 1.0;
	logLikelihood += log(tmp);

	priorSet = true;
}

void THMM::setPriorOneState(int state){
	if(priorSet) throw "THMM: Prior already set! Forgot to clear?";
	int tp=0;
	for(int k=0; k<numStates; ++k){
		forward[tp][k] = 0.0;
	}
	forward[tp][state] = 1;
	forwardScale[tp] = 1.0;
	logLikelihood = log(timePoints->getEmissionProb(tp,state));
	priorSet=true;
}

void THMM::runForward(){
	if(!priorSet) throw "THMM: Can not run forward: prior has not been set!";
	if(forwardRun) throw "THMM: forward run already done! Forgot to clear?";

	//perform iteration for remaining time points
	int oldTp;
	TSquareMatrixStorage* thisTransMat;

	timePoints->beginInteration();
	int tp=0;
	while(timePoints->nextCheck()){
		++tp;

		if(timePoints->curIsFirstInSeries()){ //in case of delatT = inf (e.g. another replicate)
			setUniformPrior(tp);
		} else {
			oldTp = tp-1;
			thisTransMat = transMat->getPointerToTransitionMatrix(timePoints->currentDeltaT());

			forwardScale[tp] = 0.0;
			for(int k=0; k<numStates; ++k){
				forward[tp][k] = 0.0;
				for(int ok=0; ok<numStates; ++ok){
					forward[tp][k] += forward[oldTp][ok] * (*thisTransMat)(ok, k);
				}

				forward[tp][k] *= timePoints->currentEmissionProb(k);
				forwardScale[tp] += forward[tp][k];
			}

			//scale forward values to prevent underflow: devide by sum
			if(timePoints->currentHasSamples()){
				for(int k=0; k<numStates; ++k){
					forward[tp][k] = forward[tp][k] / forwardScale[tp];
				}
				logLikelihood += log(forwardScale[tp]) + timePoints->currentEmissionScaleLog();
			}
		}
	}
}



/*
void THMM::sampleTrajectory(TTrajectory* trajectory, TRandomGenerator* randomGenerator){
	if(!forwardRun) runForward();
	trajectory->clear();

	//sample last state from forward -> forward is scaled such that sum across states = 1!
	double rand = randomGenerator->getRand();
	int state = 0;
	int tp = numTimePoints-1;
	double cumul = forward[tp][state];
	while(cumul < rand){
		++state;
		cumul += forward[tp][state];
	}
	int duration=1;
	trajectory->addSelectedSizes(duration, state);

	//now go backward to sample
	double* prob = new double[numStates];
	TSquareMatrixStorage* thisTransMat;

	timePoints->rbeginInteration();
	while(tp>0){
		--tp;
		//calculate probabilities
		thisTransMat = transMat->getPointerToTransitionMatrix(timePoints->rCurrentDeltaT());
		cumul = 0.0;
		for(int k=0; k<numStates; ++k){
			prob[k] = forward[tp][k] * (*thisTransMat)(k, state);
			cumul += prob[k];
		}
		//choose state
		rand = randomGenerator->getRand() * cumul;
		state = 0;
		cumul = 0;
		while(cumul < rand){
			++state;
			cumul += prob[state];
		}
		//add to trajectory
		trajectory->addSelectedSizes(timePoints->rCurrentDeltaT(), state);
		timePoints->previous();
	}
	delete[] prob;
}
*/
double THMM::getLogLikelihood(){
	if(!forwardRun){
		runForward();
	}
	return logLikelihood;
}

