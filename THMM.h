/*
 * THMM.h
 *
 *  Created on: May 29, 2014
 *      Author: wegmannd
 */

#ifndef THMM_H_
#define THMM_H_

#include "TTimePoints.h"
#include "TTransitionMatrix.h"
#include "TTrajectory.h"
#include "TRandomGenerator.h"

class THMM{
public:
	TTimePointVector* timePoints;
	double** forward;
	double* forwardScale;
	double logCumulForwardScale;
	TTransitionMatrix* transMat;
	int numStates;
	int numTimePoints;
	int storageSize;
	double* prior;
	bool priorSet;
	bool forwardRun;
	double logLikelihood;
	bool likelihoodCalculated;
	bool storageInitialized;

	THMM(TTimePointVector* TimePoints, TTransitionMatrix* TransMat);
	~THMM(){
		freeForward();
		delete[] prior;
	};
	void freeForward();
	void init();
	void reset(TTransitionMatrix* TransMat);
	void setUniformPrior(int tp=0);
	void setPriorOneState(int state);
	void runForward();
	//void sampleTrajectory(TTrajectory* trajectory, TRandomGenerator* randomGenerator);
	double getLogLikelihood();
};





#endif /* THMM_H_ */
