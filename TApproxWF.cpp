/*
 * TApproxWF.cpp
 *
 *  Created on: Feb 12, 2014
 *      Author: wegmannd
 */

#include "TApproxWF.h"


//---------------------------------------------------------------------------------------------------
//TApproxWF
//---------------------------------------------------------------------------------------------------
TApproxWF::TApproxWF(TParameters & Params,  TLog* Logfile){
	myParameters=&Params;
	logfile=Logfile;

	//initialize random generator
	logfile->listFlush("Initializing random generator ...");
	if(myParameters->parameterExists("fixedSeed")){
		randomGenerator=new TRandomGenerator(myParameters->getParameterLong("fixedSeed"), true);
	} else if(myParameters->parameterExists("addToSeed")){
		randomGenerator=new TRandomGenerator(myParameters->getParameterLong("addToSeed"), false);
	} else randomGenerator=new TRandomGenerator();
	logfile->flush(" done with seed ");
	logfile->flush(randomGenerator->usedSeed);
	logfile->write("!");

	//initialize others
	u=NULL;
	uInitialized=false;
}

void TApproxWF::simulateTrajectories(){
	logfile->startIndent("Simulating Trajectories:");
	//read parameters
	int N = myParameters->getParameterLong("N");
	if(N<2) throw "N has to be larger than 1 (currently: " + toString(N) + ")!";
	int twoN = 2*N;
	double s = myParameters->getParameterDouble("s");
	double h = 0.5;
	if(myParameters->parameterExists("h")) h=myParameters->getParameterDouble("h");
	if(h<0.0 || h>1.0) throw "h has to be between 0 and 1 (currently: " + toString(h) + ")!";
	logfile->startIndent("Wright-Fisher parameters set to:");
	logfile->list("Populations size N = " + toString(N) + " (2N = " + toString(twoN) + ")");
	logfile->list("Selection coefficient s = " + toString(s));
	logfile->list("Dominance h = " + toString(h));
	logfile->endIndent();

	//read max number of generations to run
	int maxGen = myParameters->getParameterIntWithDefault("maxGen", 999999);

	double initialFreq = myParameters->getParameterDouble("f");

	if(initialFreq == 0.0 || initialFreq == 1.0) throw "Initial state can not be an absorbing state (0.0 or 1.0)!";
	double endFreq = myParameters->getParameterDoubleWithDefault("fend", 1.0);
	if(initialFreq == endFreq) throw "Initial == ending frequency!";

	//output file
	std::string outName = myParameters->getParameterStringWithDefault("outName", "simulatedTrajectories");
	if(outName.length()<1) throw "Not a valid output name!";
	std::string filename = outName + "_statistics.txt";
	logfile->list("Writing output to '" + filename + "'");
	ofstream out(filename.c_str());
	if(!out) throw "Failed to open output file '" + filename + "'!";
	//write header
	out << "N\ts\th\tf_init\tf_end\tT" << endl;

	//print trajectories?
	bool writeTrajectories = myParameters->parameterExists("writeTrajectories");
	ofstream trajout;
	if(writeTrajectories){
		filename = outName + "_trajectories.txt";
		logfile->list("Writing trajectories to file '" + filename + "'");
		trajout.open(filename.c_str());
		if(!trajout) throw "Failed to open output file '" + filename + "'!";
		trajout << "N\ts\th\tf_init\ttrajectory" << endl;
	}

	int replicates = myParameters->getParameterLong("rep", false);
	if(replicates<1) replicates=1;

	std::string type = myParameters->getParameterStringWithDefault("type", "approx");
	if(type == "Ferrer") simulateTransitionTimeApprox(N, s, h, initialFreq, endFreq, maxGen, replicates, out, writeTrajectories, trajout);
	else if(type == "Malaspinas") simulateTransitionTimeApprox(N, s, h, initialFreq, endFreq, maxGen, replicates, out, writeTrajectories, trajout);
	else if(type == "WrightFisher") simulateTransitionTimeWF(N, s, h, initialFreq, endFreq, maxGen, replicates, out, writeTrajectories, trajout);
	else if(type == "Lacerda") simulateTransitionTimeDiffusion(N, s, h, initialFreq, endFreq, maxGen, replicates, out, writeTrajectories, trajout);
	else if(type == "Kimura") simulateTransitionTimeDiffusionKimura(N, s, h, initialFreq, endFreq, maxGen, replicates, out, writeTrajectories, trajout);
	else throw "Unknown type '" + type + "'! Must be either approx, WrightFisher or diffusion.";
	out.close();
	logfile->endIndent();

}

void TApproxWF::simulateTransitionTimeApprox(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout){
	int twoN = 2*N;
	//initialize states
	logfile->startIndent("Settings of WF approximation:");
	int numStates = myParameters->getParameterInt("nStates", false);
	if(numStates<2) numStates=51;
	logfile->list("Approximating with " + toString(numStates) + " states");
	bool unifstates = myParameters->parameterExists("uniformStates");
	if(unifstates) logfile->list("Distributing states uniformly");
	else logfile->list("Distributing states non-uniformly");
	logfile->listFlush("Initializing states ...");
	TStatesU u(numStates, unifstates);
	logfile->write(" done!");
	double gammaThreshold = myParameters->getParameterDoubleWithDefault("gammaThreshold", 10.0);
	logfile->list("Using approximate formulae when gamma = 2Ns(u[k+1] - u[k-1]) > " + toString(gammaThreshold));
	logfile->endIndent();

	//match frequency bin
	int initialState = u.getBestMatchingState(initialFreq);
	logfile->list("Initial frequency set to " +  toString(u[initialState]) + " (closest match to " + toString(initialFreq) + ")");
	int maxState = u.getBestMatchingState(endFreq);
	logfile->list("End frequency set to " +  toString(u[maxState]) + " (closest match to " + toString(endFreq) + ")");

	//initialize transition matrix
	TTransitionMatrixApprox transMat(twoN, s, h, 0.0, &u, gammaThreshold, randomGenerator);

	//run simulations
	logfile->listFlush("Simulating " + toString(replicates) + " trajectories with Approximate Wright-Fisher ... (0%)");
	int curState;
	int time;
	int prog, oldProg = 0;
	for(int i=0; i<replicates; ++i){
		curState = initialState;
		time = 0;
		if(writeTrajectories) trajout << N << "\t" << s << "\t" << h << "\t" << u[initialState];

		for(; time<maxGen; ++time){
			//iterate
			curState=transMat.simulateGeneration(curState);

			//check if we stop: fixation?
			if(curState == 0 || curState == (numStates -1)) break;
			//check if we stop: exit of interval?
			if((initialState < maxState && curState >= maxState) || (initialState > maxState && curState <= maxState)) break;

			//write trajectory
			if(writeTrajectories)  trajout << "\t" << u[curState];
		}
		if(writeTrajectories)  trajout << endl;

		//report
		out << N << "\t" << s << "\t" << h << "\t" << u[initialState] << "\t" << u[curState] << "\t" << time+1 << endl;
		prog = 100 * (double) (i+1) / (double) replicates;
		if(prog>oldProg){
			oldProg = prog;
			logfile->listOverFlush("Simulating " + toString(replicates) + " trajectories with Approximate Wright-Fisher ... (" + toString(prog) + "%)");
		}
	}
	logfile->overList("Simulating " + toString(replicates) + " trajectories with Approximate Wright-Fisher ... (100%)");
}

void TApproxWF::simulateTransitionTimeMalaspinas(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout){
	int twoN = 2*N;
	//initialize states
	logfile->startIndent("Settings of Malaspinas approximation:");
	int numStates = myParameters->getParameterInt("nStates", false);
	if(numStates<2) numStates = 100;
	logfile->list("Approximating with " + toString(numStates) + " states");
	bool unifstates = myParameters->parameterExists("uniformStates");
	if(unifstates) logfile->list("Distributing states uniformly");
	else logfile->list("Distributing states non-uniformly");
	logfile->listFlush("Initializing states ...");
	TStatesU u(numStates, unifstates);
	logfile->write(" done!");
	logfile->endIndent();

	//match frequency bin
	int initialState = u.getBestMatchingState(initialFreq);
	logfile->list("Initial frequency set to " +  toString(u[initialState]) + " (closest match to " + toString(initialFreq) + ")");
	int maxState = u.getBestMatchingState(endFreq);
	logfile->list("End frequency set to " +  toString(u[maxState]) + " (closest match to " + toString(endFreq) + ")");

	//initialize transition matrix
	TTransitionMatrixMalaspinas transMat(twoN, s, h, 0.0, &u, randomGenerator);

	//run simulations
	logfile->listFlush("Simulating " + toString(replicates) + " trajectories with Malaspinas Wright-Fisher ... (0%)");
	int curState;
	int time;
	int prog, oldProg = 0;
	for(int i=0; i<replicates; ++i){
		curState = initialState;
		time = 0;
		if(writeTrajectories) trajout << N << "\t" << s << "\t" << h << "\t" << u[initialState];

		for(; time<maxGen; ++time){
			//iterate
			curState=transMat.simulateGeneration(curState);

			//check if we stop: fixation?
			if(curState == 0 || curState == (numStates -1)) break;
			//check if we stop: exit of interval?
			if((initialState < maxState && curState >= maxState) || (initialState > maxState && curState <= maxState)) break;

			//write trajectory
			if(writeTrajectories)  trajout << "\t" << u[curState];
		}
		if(writeTrajectories)  trajout << endl;

		//report
		out << N << "\t" << s << "\t" << h << "\t" << u[initialState] << "\t" << u[curState] << "\t" << time+1 << endl;
		prog = 100 * (double) (i+1) / (double) replicates;
		if(prog>oldProg){
			oldProg = prog;
			logfile->listOverFlush("Simulating " + toString(replicates) + " trajectories with Malaspinas Wright-Fisher ... (" + toString(prog) + "%)");
		}
	}
	logfile->overList("Simulating " + toString(replicates) + " trajectories with Malaspinas Wright-Fisher ... (100%)");
}

void TApproxWF::simulateTransitionTimeWF(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout){
	int twoN = 2*N;
	//match frequency bin
	int initialState = round(initialFreq * (double) twoN);
	logfile->list("Initial frequency set to " +  toString(initialState / (double) twoN) + " (closest match to " + toString(initialFreq) + ")");
	int maxState = round(endFreq * (double) twoN);

	//initialize transition matrix
	logfile->listFlush("Initializing transition matrix ...");
	double zero = 0.0;
	TTransitionMatrixWF transMat(twoN, s, h, zero, randomGenerator);
	logfile->write(" done!");

	//run simulations
	logfile->listFlush("Simulating " + toString(replicates) + " trajectories with Wright-Fisher ... (0%)");
	int curState;

	int time;
	int prog, oldProg = 0;
	for(int i=0; i<replicates; ++i){
		curState = initialState;
		time = 0;
		if(writeTrajectories) trajout << N << "\t" << s << "\t" << h << "\t" << u[initialState];

		for(; time<maxGen; ++time){
			//iterate
			curState=transMat.simulateGeneration(curState);

			//check if we stop: fixation?
			if(curState == 0 || curState == twoN) break;
			//check if we stop: exit of interval?
			if((initialState < maxState && curState >= maxState) || (initialState < maxState && curState >= maxState)) break;

			//write trajectory
			if(writeTrajectories)  trajout << "\t" << u[curState];
		}
		if(writeTrajectories)  trajout << endl;

		//report
		out << N << "\t" << s << "\t" << h << "\t" << (double) initialState / (double) twoN << "\t" << (double) curState / (double) twoN << "\t" << time+1 << endl;
		prog = 100 * (double) (i+1) / (double) replicates;
		if(prog>oldProg){
			oldProg = prog;
			logfile->listOverFlush("Simulating " + toString(replicates) + " trajectories with Wright-Fisher ... (" + toString(prog) + "%)");
		}
	}
	logfile->overList("Simulating " + toString(replicates) + " trajectories with Wright-Fisher ... (100%)");
}

void TApproxWF::simulateTransitionTimeDiffusion(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout){
	int twoN = 2*N;
	//run simulations
	logfile->listFlush("Simulating " + toString(replicates) + " trajectories with diffusion approximation ... (0%)");
	double curF;
	long numStepsPerGen = 300;
	double dt = 1.0 / (double) numStepsPerGen;
	double time;
	double ax, bx;
	long maxSteps = maxGen * numStepsPerGen;

	int prog, oldProg = 0;
	for(int rep=0; rep<replicates; ++rep){
		curF = initialFreq;
		time = 0.0;

		if(writeTrajectories) trajout << N << "\t" << s << "\t" << h << "\t" << initialFreq;
		for(int i=0; i<maxSteps; ++i){
			time = time + dt;

			//iterate
			bx = curF * (1.0 - curF) * s * (curF + h - 2.0 * h * curF) / (1.0 + s * curF * (curF + 2.0 * h - 2.0 * h * curF));

			ax = curF * (1.0 - curF) / (double) twoN;

			curF += bx * dt + sqrt(ax * dt) * randomGenerator->getNormalRandom(0.0, 1.0);

			//check if we stop: fixation?
			if(curF <= 0.0 || curF >= 1.0) break;

			if(i % numStepsPerGen == 0){
				//check if we stop: exit of interval?
				if((initialFreq < endFreq && curF >= endFreq) || (initialFreq > endFreq && curF <= endFreq)) break;

				//write trajectory
				if(writeTrajectories)  trajout << "\t" << curF;
			}
		}
		if(writeTrajectories)  trajout << endl;

		//report
		out << N << "\t" << s << "\t" << h << "\t" << initialFreq;
		if(curF > 0.0) out << "\t" << curF << "\t" << time << std::endl;
		else out << "\t0.0\t" << time << std::endl;
		prog = 100 * (float) (rep+1) / (float) replicates;
		if(prog>oldProg){
			oldProg = prog;
			logfile->listOverFlush("Simulating " + toString(replicates) + " trajectories with diffusion approximation ... (" + toString(prog) + "%)");
		}
	}
	logfile->overList("Simulating " + toString(replicates) + " trajectories with diffusion approximation ... (100%)");
}

void TApproxWF::simulateTransitionTimeDiffusionKimura(int & N, double & s, double & h, double & initialFreq, double & endFreq, int & maxGen, int & replicates, ofstream & out, bool writeTrajectories, ofstream & trajout){
	int twoN = 2*N;
	//run simulations
	logfile->listFlush("Simulating " + toString(replicates) + " trajectories with Kimura diffusion approximation ... (0%)");
	double curF;
	long numStepsPerGen = 300;
	double dt = 1.0 / (double) numStepsPerGen;
	double time;
	double ax, bx;
	long maxSteps = maxGen * numStepsPerGen;

	int prog, oldProg = 0;
	for(int rep=0; rep<replicates; ++rep){
		curF = initialFreq;
		time = 0.0;

		if(writeTrajectories) trajout << N << "\t" << s << "\t" << h << "\t" << initialFreq;
		for(int i=0; i<maxSteps; ++i){
			time = time + dt;

			//iterate
			bx = curF * (1.0 - curF) * s * (curF + h - 2.0 * h * curF);

			ax = curF * (1.0 - curF) / (double) twoN;

			curF += bx * dt + sqrt(ax * dt) * randomGenerator->getNormalRandom(0.0, 1.0);

			//check if we stop: fixation?
			if(curF <= 0.0 || curF >= 1.0) break;

			if(i % numStepsPerGen == 0){
				//check if we stop: exit of interval?
				if((initialFreq < endFreq && curF >= endFreq) || (initialFreq > endFreq && curF <= endFreq)) break;

				//write trajectory
				if(writeTrajectories)  trajout << "\t" << curF;
			}
		}
		if(writeTrajectories)  trajout << endl;

		//report
		out << N << "\t" << s << "\t" << h << "\t" << initialFreq;
		if(curF > 0.0) out << "\t" << curF << "\t" << time << std::endl;
		else out << "\t0.0\t" << time << std::endl;
		prog = 100 * (float) (rep+1) / (float) replicates;
		if(prog>oldProg){
			oldProg = prog;
			logfile->listOverFlush("Simulating " + toString(replicates) + " trajectories with Kimura diffusion approximation ... (" + toString(prog) + "%)");
		}
	}
	logfile->overList("Simulating " + toString(replicates) + " trajectories with Kimura diffusion approximation ... (100%)");
}

void TApproxWF::test(){
	//initialize states
	int numStates = myParameters->getParameterInt("nStates", false);
	if(numStates<2) numStates=100;
	TStatesU u(numStates, myParameters->parameterExists("uniformStates"));


	int N = myParameters->getParameterLong("N");
	if(N<2) throw "N has to be larger than 1 (currently: " + toString(N) + ")!";
	int twoN = 2*N;
	double s=myParameters->getParameterDouble("s");
	double h=0.5;
	if(myParameters->parameterExists("h")) h=myParameters->getParameterDouble("h");
	if(h<0.0 || h>1.0) throw "h has to be between 0 and 1 (currently: " + toString(h) + ")!";
	logfile->startIndent("Wright-Fisher parameters set to:");
	logfile->list("Populations size N = " + toString(N) + " (2N = " + toString(twoN) + ")");
	logfile->list("Selection coefficient s = " + toString(s));
	logfile->list("Dominance h = " + toString(h));
	logfile->endIndent();

	//initialize transition matrix
	TTransitionMatrixApprox transMat(twoN, s, h, 0.0, &u, 10.0, randomGenerator);

	cout << "------------------------------------------" << endl;
	transMat.printTransitionMatrix(1);
	cout << "------------------------------------------" << endl;

}

void TApproxWF::runHMMInference(){
	//initialize states
	int numStates = myParameters->getParameterInt("nStates", false);
	if(numStates<2) numStates=51;
	logfile->list("Using " + toString(numStates) + " states.");

	bool unifStates = myParameters->parameterExists("uniformStates");
	if(unifStates) logfile->list("States are distributed equidistantly.");

	TStatesU u(numStates, unifStates);

	//initialize loci
	TLoci loci(myParameters, randomGenerator, logfile);
	loci.runMCMC(myParameters, u);
}

void TApproxWF::simulateData(){
	//initialize loci
	TLoci loci(myParameters, randomGenerator, logfile);
	loci.initializeWFParameters(myParameters);

	//run simulations
	std::string type = myParameters->getParameterStringWithDefault("type", "Ferrer");
	if(type == "Ferrer"){
		logfile->startIndent("Simulating data using the approximate WF algorithm:");
		//initialize states
		int numStates = myParameters->getParameterInt("nStates", false);
		if(numStates<2) numStates=51;
		TStatesU u(numStates, myParameters->parameterExists("uniformStates"));
		double gammaThreshold = myParameters->getParameterDoubleWithDefault("gammaThreshold", 10.0);
		loci.simulateData(myParameters, u, gammaThreshold);
	} else if(type == "WrightFisher"){
		logfile->startIndent("Simulating data using the classic WF algorithm:");
		loci.simulateDataWF(myParameters);
	} else if(type == "Kimura"){
		throw "Simulating data using diffusion approximation has not yet been implemented ...";
	} else throw "Unknown type '" + type + "'! Must be either Ferrer or WrightFisher.";
	logfile->endIndent();
}

void TApproxWF::calcStats(){
	//make sure loci is defined
	myParameters->getParameterString("loci");
	TLoci loci(myParameters, randomGenerator, logfile);
	std::string outName = myParameters->getParameterStringWithDefault("outName", "ApproxWF") + "_";
	std::ofstream stats;
	stats.open((outName + "statistics.txt").c_str());
	int run=0;

	//calc and write stats
	bool transpose = myParameters->parameterExists("transpose");
	if(transpose){
		loci.writeStatHeaderTransposed(stats);
		loci.writeStatsTransposed(stats, run);
	} else {
		loci.writeStatHeader(stats);
		loci.writeStats(stats, run);
	}

	//close file
}

void TApproxWF::convertToWFABC(){
	//make sure loci is defined
	std::string outName = myParameters->getParameterString("loci") + ".wfabc";
	if(myParameters->parameterExists("outName")){
		outName = myParameters->getParameterString("outName") + ".wfabc";
	}
	TLoci loci(myParameters, randomGenerator, logfile);
	loci.printWFABCFormat(outName);
}

void TApproxWF::calcLikelihoodSurface(std::string param){
	//initialize states
	int numStates = myParameters->getParameterInt("nStates", false);
	if(numStates<2) numStates=100;
	TStatesU u(numStates, myParameters->parameterExists("uniformStates"));

	//initialize loci
	TLoci loci(myParameters, randomGenerator, logfile);
	if(param == "N")
		loci.calcLikelihoodSurface_N(myParameters, u);
	else if(param == "s")
		loci.calcLikelihoodSurface_s(myParameters, u);
	else throw "Unknown parameterm " + param + " to calculate likelihood surface!";
}

void TApproxWF::writeTransitionMatrix(){
	//read parameters
	logfile->startIndent("Wright-Fisher parameters set to:");
	int N = myParameters->getParameterLong("N");
	if(N<2) throw "N has to be larger than 1 (currently: " + toString(N) + ")!";
	int twoN = 2*N;
	logfile->list("Populations size N = " + toString(N) + " (2N = " + toString(twoN) + ")");

	double s = myParameters->getParameterDouble("s");
	logfile->list("Selection coefficient s = " + toString(s));

	double h = 0.5;
	if(myParameters->parameterExists("h")) h=myParameters->getParameterDouble("h");
	if(h<0.0 || h>1.0) throw "h has to be between 0 and 1 (currently: " + toString(h) + ")!";
	logfile->list("Dominance h = " + toString(h));

	int t = myParameters->getParameterIntWithDefault("gen", 1);
	logfile->list("Number of generations t = " + toString(t));
	logfile->endIndent();

	//type
	std::string type = myParameters->getParameterStringWithDefault("type", "Ferrer");

	//output file
	std::string outName = myParameters->getParameterStringWithDefault("outName", "transmat");
	outName += "_" + type + "_N_" + toString(N) + "_s_" + toString(s) + "_h_" + toString(h) + "_t_" + toString(t) + ".txt";


	outName = "transmat_" + type + "_N_" + toString(N) + "_s_" + toString(s) + "_h_" + toString(h) + "_t_" + toString(t) + ".txt";

	logfile->list("Writing transition matrix to '" + outName + "'");
	ofstream out(outName.c_str());
	if(!out) throw "Failed to open output file '" + outName + "'!";

	//run!
	if(type == "Ferrer"){
		//initialize states
		int numStates = myParameters->getParameterInt("nStates", false);
		if(numStates<2) numStates=51;
		logfile->list("Using " + toString(numStates) + " states.");
		bool unifStates = myParameters->parameterExists("uniformStates");
		if(unifStates) logfile->list("States are distributed equidistantly.");
		TStatesU u(numStates, unifStates);


		//read gamma threshold
		double gammaThreshold = myParameters->getParameterDoubleWithDefault("gammaThreshold", 100.0);

		//write header to output file
		u.write(out);

		//initialize transition matrix
		logfile->listFlush("Initializing transition matrix ...");
		TTransitionMatrixApprox transMat(twoN, s, h, 0.0, &u, gammaThreshold, randomGenerator);
		transMat.fillExponential(t);
		logfile->write(" done!");

		//writing transition matrix
		transMat.writeTransitionMatrix(t, out);
	} else if(type == "Malaspinas"){
			//initialize states
			int numStates = myParameters->getParameterInt("nStates", false);
			if(numStates<2) numStates=100;
			logfile->list("Using " + toString(numStates) + " states.");
			bool unifStates = myParameters->parameterExists("uniformStates");
			if(unifStates) logfile->list("States are distributed equidistantly.");
			TStatesU u(numStates, unifStates);

			//write header to output file
			u.write(out);

			//initialize transition matrix
			logfile->listFlush("Initializing Malaspinas transition matrix ...");
			TTransitionMatrixMalaspinas transMat(twoN, s, h, 0.0, &u, randomGenerator);
			transMat.fillExponential(t);
			logfile->write(" done!");

			//writing transition matrix
			transMat.writeTransitionMatrix(t, out);
	} else if(type == "WrightFisher"){
		//initialize transition matrix
		logfile->listFlush("Initializing transition matrix ...");
		double zero = 0.0;
		TTransitionMatrixWF transMat(twoN, s, h, zero, randomGenerator);
		transMat.fillExponential(t);
		logfile->write(" done!");

		//write header to output file
		out << "0.0";
		for(int i=1; i<=twoN; ++i){
			out << "\t" << (double) i / (double) twoN;
		}
		out << std::endl;

		//writing transition matrix
		transMat.writeTransitionMatrix(t, out);
	} else throw "Unknown type '" + type + "'! Must be either Ferrer, Malaspinas or WrightFisher.";

	//clean up
	out.close();
}


void TApproxWF::sampleTrajectories(){
	/*
	//read params of model
	WFParameters wfp(myParameters);

	TTransitionMatrix* transMat;
	TStatesU* u=NULL;

	//initialize states
	int numStates = myParameters->getParameterInt("nStates", false);
	if(numStates<2) numStates=100;

	u = new TStatesU(numStates, wfp, myParameters->parameterExists("uniformStates"));

	//initialize transition matrix
	transMat = new TTransitionMatrixApprox(wfp, u, randomGenerator);

	//initialize time points
	std::string sampleFile = myParameters->getParameterString("samples");
	int Tmut = myParameters->getParameterInt("Tmut");

	TTimePointVector tp(Tmut, 1, sampleFile, u, randomGenerator);
	THMM hmm(&tp, transMat);

	std::string outName = myParameters->getParameterString("outName");

	int replicates=myParameters->getParameterLong("rep", false);
	if(replicates<1) replicates=1;

	TTrajectory tj(&wfp, u);
	for(int i=0; i<replicates; ++i){
		hmm.reset();
		hmm.setPriorOneState(1);
		hmm.sampleTrajectory(&tj, randomGenerator);
		tj.write(outName + "_" + toString(i+1) +".txt");
	}

	delete u;
	delete transMat;
*/
}





















