/*
 * ApproxWF.cpp
 *
 *  Created on: Feb 12, 2014
 *      Author: wegmannd
 */

#include <time.h>
#include "TParameters.h"
#include "TApproxWF.h"
#include <sys/time.h>


int main(int argc, char* argv[]){
	struct timeval start, end;
    gettimeofday(&start, NULL);

    //log file
    TLog logfile;
	logfile.newLine();
	logfile.write("              Approximate Wright-Fisher");
	logfile.write("              =========================");
	logfile.newLine();
	logfile.write("   Written by Daniel Wegmann & Christoph Leuenberger");
	logfile.write("          University of Fribourg - Switzerland");
	logfile.write("   Available @ https://bitbucket.org/phaentu/approxwf");
	logfile.write("          Check online documentation for help!");
	logfile.newLine();
	logfile.newLine();

	try{
		if (argc<2){
			throw "Arguments missing!";
		} else {
			//read parameters from the command line
			TParameters myParameters(argc, argv, &logfile);

			//verbose?
			bool verbose=myParameters.parameterExists("verbose");
			logfile.setVerbose(verbose);
			if(!verbose) logfile.listNoFile("Running in silent mode (use 'verbose' to get a status report on screen)");

			//open log file that handles the output
			std::string logFilename=myParameters.getParameterString("logFile", false);
			if(logFilename.length()>0){
				cout << "- writing log to '" << logFilename << endl;
				logfile.openFile(logFilename.c_str());
				logfile.writeFileOnly(" Approximate Wright-Fisher ");
				logfile.writeFileOnly("***************************");
				if(myParameters.hasInputFileBeenRead())
					logfile.listFileOnly("- parameters read from inputfile '" + myParameters.getInputFile() + "'");
			}

			//what to do?
			TApproxWF approxWF(myParameters, &logfile);
			std::string task=myParameters.getParameterString("task");
			if(task=="simTrajectories") approxWF.simulateTrajectories();
			else if(task=="transmat") approxWF.writeTransitionMatrix();
			else if(task=="test") approxWF.test();
			else if(task=="trajectories") approxWF.sampleTrajectories();
			else if(task=="estimate") approxWF.runHMMInference();
			else if(task=="simulate") approxWF.simulateData();
			else if(task=="statistics") approxWF.calcStats();
			else if(task=="convertWFABC") approxWF.convertToWFABC();
			else if(task=="LLSurface_N") approxWF.calcLikelihoodSurface("N");
			else if(task=="LLSurface_s") approxWF.calcLikelihoodSurface("s");
			else throw "Unknown task '" + task + "'!";

			//end of program
			//write unsused parameters
			std::string unusedParams=myParameters.getListOfUnusedParameters();
			if(unusedParams!="") logfile.warning("The following parameters were not used: " + unusedParams + "!");
		}
	 }
	 catch (std::string & error){
		 logfile.error(error);
		 return 1;
	 }
	 catch (const char* error){
		 logfile.error(error);
		 return 1;
	 }
	 catch (...){
		 cerr << endl << endl << "ERROR: unhandeled error!" << endl;
		 return 1;
	 }

	 gettimeofday(&end, NULL);
	 float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	 logfile.list("Program terminated in ", runtime, " min!");
	 logfile.close();
	 return 0;
}


