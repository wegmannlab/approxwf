/*
 * TLoci.cpp
 *
 *  Created on: Jun 13, 2014
 *      Author: wegmannd
 */

#include "TLoci.h"

//-------------------------------------------------------
//Tloci
//-------------------------------------------------------
//TODO: write function to read WF-ABC files

TLoci::TLoci(TParameters* myParameters, TRandomGenerator* RandomGenerator, TLog* Logfile){
	randomGenerator = RandomGenerator;
	logfile = Logfile;
	priorsInitialized = false;
	priors = NULL;
	transMatNeutral = NULL;
	transMatNeutralInitialized = false;

	//read file with locus definitions, if given
	//allow to formats:
	// - one locus per row (default)
	// - one time point per row (transposed)
	logfile->startIndent("Initializing loci:");
	size_t maxReadLoci = myParameters->getParameterLongWithDefault("maxReadLoci", 1000000);
	logfile->list("Will read up to " + toString(maxReadLoci) + " loci.");

	if(myParameters->parameterExists("loci")){
		//allow only a single file
		std::vector<std::string> filenames;
		myParameters->fillParameterIntoVector("loci", filenames, ',');
		if(filenames.size() > 1)
			throw "Only allow a single loci file when using default file format!";

		//allow for filtering
		int minNumTimepointsFilter = myParameters->getParameterIntWithDefault("minNumTimePointFilter", 0);
		double minFreq = 0.0;
		if(minNumTimepointsFilter > 0){
			minFreq = myParameters->getParameterDoubleWithDefault("minFreqFilter", 0.05);
			logfile->list("Will only keep loci for which both alleles have a sample frequency > " + toString(minFreq) + " at " + toString(minNumTimepointsFilter) + " timepoints or more.");
		}
		if(minNumTimepointsFilter < 0)
			throw "minNumTimepointsFilter must be >= 0!";

		double min_fs = myParameters->getParameterDoubleWithDefault("minFsFilter", 0.0);
		if(min_fs > 0.0)
			logfile->list("Will only keep loci for which either fsi or fsd > " + toString(min_fs) + ".");

		//now read file
		lociFilename = filenames[0];
		readLociFromFileDefault(filenames[0], maxReadLoci, minNumTimepointsFilter, minFreq, min_fs);

		//check if we have replicates
		if(myParameters->parameterExists("replicates")){
			std::vector<int> replicates;
			myParameters->fillParameterIntoVector("replicates", replicates, ',');
			std::string tmp;
			concatenateString(replicates, tmp, ", ");
			logfile->list("Will consider independent replicates to start at timepoints " + tmp + ".");

			//check that first is used
			if(replicates[0] != allTimePoints[0])
				throw "First timepoint must be the start of a replicate!";

			//check all replicates have at least two time points
			std::vector<int>::iterator timeIt=allTimePoints.begin();
			int numTimepointsInReplicate = 2;
			int oldStart = *timeIt - 1;
			for(std::vector<int>::iterator it = replicates.begin(); it != replicates.end(); ++it){
				if(*it <= oldStart) throw "Starts of replicates must be ordered!";
				oldStart = *it;
				while(*timeIt < *it){
					++timeIt;
					++numTimepointsInReplicate;
				}
				if(*timeIt == *it){
					if(numTimepointsInReplicate < 2) throw "Replicates must have at least two time points!";
					//set start in all loci
					for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
						lociIt->setTimePointAsFirstInSeries(*it);
				} else throw "Time point " + toString(*it) + " does not exist!";
			}
		}
	} else if(myParameters->parameterExists("lociTransposed")){
		//allow for multiple replicates
		std::vector<std::string> filenames;
		myParameters->fillParameterIntoVector("lociTransposed", filenames, ',');

		//read first file
		lociFilename = filenames[0];
		readLociFromFileTransposed(filenames[0], maxReadLoci);

		//read others, if present. Always shift time to make sure these time points appear
		if(filenames.size() > 1){
			int shift;
			for(unsigned int f=1; f<filenames.size(); ++f){
				//make sure next time point is accepted as first in series
				for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
					lociIt->setNextTimePointIsFirstInSeries();
				}
				//get shift
				shift = (*allTimePoints.rbegin());
				shift += exp10(ceil(log10((float) shift)));
				addTimePointsFromFile(filenames[f], shift);
			}

		}
	} else if(myParameters->parameterExists("nLoci")){
		createLociFromParameters(myParameters);
	} else throw "The loci for which data is to be simulated have to be either defined in a file (with argument 'loci' or 'lociTransposed'), or specified using the arguments 'nLoci', 'sampleSize' and 'timePoints'!";
	logfile->endIndent();

	//check if some loci are known to be neutral
	if(myParameters->parameterExists("knownNeutral")){
		std::string filename = myParameters->getParameterString("knownNeutral");
		readLociKnownToBeNeutral(filename);
	}

	//fill pointers to loci
	pointerToLocus = new TLocus*[loci.size()];
	pointersToLociInitialized = true;
	int i=0;
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt, ++i){
		pointerToLocus[i] = &(*lociIt);
	}
};

void TLoci::readLociFromFileTransposed(std::string & filename, size_t maxReadLoci){
	logfile->listFlush("Reading loci from file '", filename, "' ...");
		std::ifstream input(filename.c_str());
		if(!input) throw "File " + filename + " could not be opened!";

		//read file into vectors
		std::string line;
		std::vector<std::string> vec;
		int parsedTime;
		int oldTime;
		long num = 0;
		bool first = true;
		bool firstInSeries = true;
		unsigned int numColumns;
		std::vector<std::string>::iterator it;
		while(input.good() && !input.eof()){
			++num;
			std::getline(input, line);
			line=extractBefore(line, "//");
			if(!line.empty()){
				fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
				if(first){
					//read header and construct loci
					numColumns = vec.size();
					numLoci = numColumns - 1;
					if(numLoci > maxReadLoci)
						numLoci = maxReadLoci;
					it = vec.begin();	++it;
					size_t numAdded = 0;
					for(;it!=vec.end(); ++it, ++numAdded){
						loci.push_back(TLocus(*it, randomGenerator));
						if(numAdded >= maxReadLoci)
							break;
					}
					first = false;
				} else {
					//add time points
					if(vec.size() != numColumns) throw "Wrong number of entries in file " + filename + " on line " + toString(num) + " (expecting " + toString(numColumns) + " instead of " + toString(vec.size()) + ")!";
					//parse time
					it = vec.begin();
					parsedTime = stringToInt(*it);
					if(!firstInSeries && parsedTime <= oldTime) throw "Time points do not appear to be ordered in file '" + filename + "' on line " + toString(num) + "!";
					oldTime = parsedTime;
					allTimePoints.push_back(parsedTime);
					++it;
					//add samples
					for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt, ++it)
						lociIt->addTimepoint(parsedTime, *it);
					firstInSeries = false;
				}
			}
		}
		input.close();

		//initialize time points
		for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
			lociIt->initializeTimepoints();

		logfile->write(" done!");
		logfile->conclude("Read a total of ", loci.size(), " loci");
		logfile->conclude("Read data at ", allTimePoints.size(), " different time points");

		if(loci.size()<1) throw "No loci have been defined!";
		if(allTimePoints.size()<2) throw "Less than 2 time points have been specified";
}


void TLoci::readLociFromFileDefault(std::string & filename, size_t maxReadLoci, int minNumTimepointsFilter, double & minFreq, double & min_fs){
	//check if file is zipped
	std::istream* input;
	std::string reportString;
	if(readAfterLast(filename,'.') == "gz"){
		reportString = "Reading loci from zipped transposed file '" + filename + "' ...";
		input = new gz::igzstream(filename.c_str());
	} else {
		reportString = "Reading loci from transposed file '" + filename + "' ...";
		input = new std::ifstream(filename.c_str());
	}
	logfile->listFlush(reportString);
	if(!(*input)) throw "File " + filename + " could not be opened!";

	//open output file of accepted loci
	std::string acceptedFileName = extractUntilLast(filename, '.') + "accepted.txt";
	std::ofstream acceptedOut(acceptedFileName.c_str());

	//read file into vectors
	std::string line;
	std::vector<std::string> vec;
	int parsedTime;
	int oldTime;
	long num = 0;
	bool first = true;
	bool firstInSeries = true;
	unsigned int numColumns;
	std::vector<std::string>::iterator it;
	int numLociFiltered = 0;
	std::string lineCopy;

	while(input->good() && !input->eof()){
		++num;
		std::getline(*input, line);
		line=extractBefore(line, "//");
		if(!line.empty()){
			lineCopy = line;
			fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);

			//each line contains name of locus, followed by data per time point
			if(first){
				acceptedOut << lineCopy << "\n";
				//read header to know time points
				it = vec.begin();	++it; //skip locus header
				for(;it!=vec.end(); ++it){
					parsedTime = stringToInt(*it);
					if(!firstInSeries && parsedTime <= oldTime) throw "Time points do not appear to be ordered in file '" + filename + "' on line " + toString(num) + "!";
					oldTime = parsedTime;
					allTimePoints.push_back(parsedTime);
					firstInSeries = false;
				}
				numColumns = allTimePoints.size() + 1;
				first = false;
			} else {
				if(vec.size() != numColumns) throw "Wrong number of entries in transposed file " + filename + " on line " + toString(num) + " (expecting " + toString(numColumns) + " instead of " + toString(vec.size()) + ")!";

				//add locus
				it = vec.begin();
				loci.emplace_back(*it, randomGenerator);
				++it;
				std::vector<TLocus>::reverse_iterator newLocus = loci.rbegin();

				//add data
				for(std::vector<int>::iterator timeIt=allTimePoints.begin(); timeIt!=allTimePoints.end(); ++timeIt, ++it){
					newLocus->addTimepoint(*timeIt, *it);
				}

				//filter on allele frequency
				if(minNumTimepointsFilter > 0){
					if(newLocus->getMinNumTimepointsAllelesAreObserved(minFreq) < minNumTimepointsFilter){
						loci.pop_back();
						++numLociFiltered;
						continue;
					}
				}

				//initialize time points
				newLocus->initializeTimepoints();

				//filter on statistics fsd and fsi
				if(min_fs > 0.0){
					newLocus->calculateStats(statSettings);

					//std::cout << std::endl << newLocus->name << ": " << newLocus->get_fsd() << ", " << newLocus->get_fsi() << ", " << fabs(newLocus->get_fsd() - newLocus->get_fsi()) << std::endl;

					if(newLocus->get_fsd() < min_fs && newLocus->get_fsi() < min_fs){
						loci.pop_back();
						++numLociFiltered;
						continue;
					}
				}

				//writ accepted loci
				acceptedOut << lineCopy << "\n";
			}

			//regularly report progress
			if(num % 100000 == 0)
				logfile->listOverFlush(reportString + " (" + toString(num) + " parsed)");

			//check if we read enough loci
			if(loci.size() >= maxReadLoci)
				break;
		}
	}

	//close stream
	delete input;
	acceptedOut.close();

	logfile->write(" done!");
	std::string tmp;
	concatenateString(allTimePoints, tmp, ", ");
	logfile->conclude("Read data at ", allTimePoints.size(), " different time points (" + tmp + ").");
	numLoci = loci.size();
	logfile->conclude("Read a total of ", numLoci, " loci.");
	logfile->conclude("Filtered a total of ", numLociFiltered, " loci.");

	if(numLoci<1) throw "No loci have been defined!";
	if(allTimePoints.size()<2) throw "Less than 2 time points have been specified";
}

void TLoci::addTimePointsFromFile(std::string & filename, int shiftTimeBy){
	logfile->listFlush("Adding additional time points from file '", filename, "' ...");
		std::ifstream input(filename.c_str());
		if(!input) throw "File " + filename + " could not be opened!";

		//prepare map to loci from previous files
		bool* locusUsed;
		int* internalLocusIndex;

		//read file into vectors
		std::string line;
		std::vector<std::string> vec;
		int parsedTime, oldTime;
		int num = 0;
		int index;
		int col;
		int numLocusColumns;
		bool first = true;
		bool firstLocus = true;
		unsigned int numColumns;
		int oldNumTimepoints = allTimePoints.size();
		std::vector<std::string>::iterator it;
		while(input.good() && !input.eof()){
			++num;
			std::getline(input, line);
			line=extractBefore(line, "//");
			if(!line.empty()){
				fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
				if(first){
					//read header and check loci
					numColumns = vec.size();
					numLocusColumns = numColumns - 1;
					locusUsed = new bool[numLocusColumns];
					internalLocusIndex = new int[numLocusColumns];
					it = vec.begin();	++it;
					col = 0;
					for(;it!=vec.end(); ++it, ++col){
						//check if locus exists
						index = getLocusIndexFromName(*it);
						if(index >= 0){
							locusUsed[col] = true;
							internalLocusIndex[col] = index;
						}
					}
					first = false;
				} else {
					//add time points
					if(vec.size() != numColumns) throw "Wrong number of entries in file " + filename + " on line " + toString(num) + " (expecting " + toString(numColumns) + " instead of " + toString(vec.size()) + ")!";
					//parse time
					it = vec.begin();
					parsedTime = stringToInt(*it) + shiftTimeBy;
					if(!firstLocus && parsedTime <= oldTime) throw "Time points do not appear to be ordered in file '" + filename + "' on line " + toString(num) + "!";
					if(parsedTime < 0) throw "Time points of additional replicates have to be > 0 in file '" + filename + "' on line " + toString(num) + "!";
					oldTime = parsedTime;
					allTimePoints.push_back(parsedTime);
					++it;

					//add samples
					col = 0;
					for(;it!=vec.end(); ++it, ++col){
						if(locusUsed[col]){
							loci[internalLocusIndex[col]].addTimepoint(parsedTime, *it);
						}
					}
					firstLocus = false;
				}
			}
		}
		input.close();

		//initialize time points
		for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
			lociIt->initializeTimepoints();

		logfile->write(" done!");
		logfile->conclude("Read data at ", allTimePoints.size() - oldNumTimepoints, " additional time points");
}

void TLoci::readLociKnownToBeNeutral(std::string & filename){
	//setting loci to be known to be neutral from file
	logfile->listFlush("Reading loci known to be neutral from file '", filename, "' ...");
	std::ifstream input(filename.c_str());
	if(!input) throw "File " + filename + " could not be opened!";

	//read file into vectors
	std::string line;
	std::vector<std::string> vec;
	int num = 0;
	int numLoci = 0;
	while(input.good() && !input.eof()){
		++num;
		std::getline(input, line);
		if(!line.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
			if(vec.size() != 1){
				throw "Expected one entry on line " + toString(num) + " in file '" + filename + "' but found " + toString(vec.size()) + "!";
			}

			//check if locus exists
			std::vector<TLocus>::iterator it;
			for(it = loci.begin(); it != loci.end(); ++it){				
				if(it->name == vec[0]){
					it->setAsKnownToBeNeutral();
					++numLoci;
					break;
				}
			}
			if(it == loci.end()){
				logfile->warning("Locus '" + vec[0] + "' does not exists!");
			}
		}				
	}
	logfile->write(" done!");
	logfile->conclude("Set " + toString(numLoci) + " loci as known to be neutral.");	
}

void TLoci::createLociFromParameters(TParameters* myParameters){
	//read num loci and sample size
	int nLoci = myParameters->getParameterInt("nLoci");
	if(nLoci < 1) throw "nLoci has to be at least 1!";
	int sampleSize = myParameters->getParameterInt("sampleSize");
	if(sampleSize < 1) throw "sampleSize has to be at least 1!";

	//create loci
	std::string name;
	for(int l=0; l<nLoci; ++l){
		name = "L" + toString(l+1);
		loci.push_back(TLocus(name, randomGenerator));
	}

	//read and create time points
	std::string timePoints = myParameters->getParameterString("timePoints");
	std::string original = timePoints;
	std::size_t pos = timePoints.find_first_of('-');
	if(pos != std::string::npos){
		//a sequence of format 20-100:4 (from-to:step)
		//read from
		int from = stringToInt(timePoints.substr(0, pos));
		timePoints.erase(0, pos+1);
		//read to
		pos = timePoints.find_first_of(':');
		if(pos == std::string::npos) throw "Unable to understand format of time points '" + original + "'!";
		int to = stringToInt(timePoints.substr(0, pos));
		if(to <= from) throw "Unable to understand format of time points '" + original + "', from >= to!";
		//read step
		timePoints.erase(0, pos+1);
		int step = stringToInt(timePoints);
		if(step <= 0) throw "Unable to understand format of time points '" + original + "', step <= 0!";
		//no create time points
		for(int i = from; i <= to; i=i+step)
			allTimePoints.push_back(i);
		if(allTimePoints.size() < 2) throw "Unable to understand format of time points '" + original + "', the sequence results in < 2 time points!";
	} else  {
		//a list of comma separated values
		pos = timePoints.find_first_of(',');
		fillVectorFromString(timePoints, allTimePoints, ',');
		std::sort(allTimePoints.begin(), allTimePoints.end());
		if(allTimePoints.size() < 2) throw "Unable to understand format of time points '" + original + "', < 2 time points specified!";
	}
	//add time points
	int numA = 0;
	for(std::vector<int>::iterator timeIt = allTimePoints.begin(); timeIt!=allTimePoints.end(); ++timeIt){
		for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
			lociIt->addTimepoint(*timeIt, numA,  sampleSize);
	}
	//initialize time points
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
		lociIt->initializeTimepoints();
}

void TLoci::initializePriors(TParameters* myParameters){
	if(priorsInitialized) delete priors;
	priors = new WFParamPriors(myParameters, randomGenerator, logfile);
	priorsInitialized = true;
}

int TLoci::getLocusIndexFromName(std::string & name){
	int index = 0;
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
		if(lociIt->name == name) return index;
		++index;
	}
	return -1;
}

void TLoci::runMCMC(TParameters* myParameters, TStatesU & u){
	logfile->startIndent("Preparing MCMC run:");
	long numIterations = myParameters->getParameterLongWithDefault("MCMCLength", 100000);
	logfile->list("Will perform " + toString(numIterations) + " iterations");
	if(numIterations < 1) throw "MCMC has to be at least 1 iteration long!";

	//initialize number of cores to be used
	int numCores = 1;
	if(myParameters->parameterExists("nCores")){
		numCores=myParameters->getParameterInt("nCores");
		if(numCores<1) throw "The number of requested cores is " + toString(numCores) + ", which is < 1!";
	}
	logfile->list("Will split calculations across " + toString(numCores) + " cores");

	//initialize priors
	if(!priorsInitialized) initializePriors(myParameters);

	//choose random values for each locus
	//also initialize HMM and calculate current LogLikelihood
	logfile->startIndent("Choosing starting values and calculating initial likelihood:");

	//N: initial values specified?
	if(myParameters->parameterExists("initialLogN")){
		priors->setN(myParameters->getParameterDouble("initialLogN"));
	}
	logfile->list("Starting with log(N) = " + toString(priors->currentLogN()));

	//error rate: initial values specified?
	if(priors->errorRateIsEstimated()){
		if(myParameters->parameterExists("initialLogErrorRate")){
			priors->setErrorRate(myParameters->getParameterDouble("initialLogErrorRate"));
		}
		logfile->list("Starting with log(error rate) = " + toString(priors->currentLogErrorRate()));
	}

	//mutation rate: initial values specified?
	if(priors->mutationRateIsEstimated()){
		if(myParameters->parameterExists("initialLogMutationRate")){
			priors->setMutationRate(myParameters->getParameterDouble("initialLogMutationRate"));
		}
		logfile->list("Starting with log(mutation rate) = " + toString(priors->currentLogMutationRate()));
	}

	//get gamma threshold
	double gammaThreshold = myParameters->getParameterDoubleWithDefault("gammaThreshold", 100.0);

	//initializing neutral transition matrix
	transMatNeutral = new TTransitionMatrixApprox(priors->currentTwoN(), 0.0, 0.0, priors->currentMutationRate(), &u, gammaThreshold, randomGenerator);
	transMatNeutralInitialized = true;

	//initialize s and h
	logfile->listFlush("Choosing s values and calculating initial likelihood ... (0%)");
	double curLL = 0.0;
	double oldLL;
	int counter = 1;
	int oldProg = 0;
	int prog;

	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt, ++counter){
		lociIt->initialize(priors, u, gammaThreshold, transMatNeutral);
		curLL += lociIt->currentLikelihood();
		prog = 100 * (float) counter / (float) loci.size();
		if(prog > oldProg){
			oldProg = prog;
			logfile->listOverFlush("Choosing s values and calculating initial likelihood ... (" + toString(prog) + "%)");
		}
	}

	logfile->overList("Choosing s values and calculating initial likelihood ... done! ");
	logfile->conclude("initial scaled log likelihood = " + toString(curLL));
	logfile->endIndent();

	//open output file and print
	logfile->startIndent("Preparing output:");
	std::string filename = myParameters->getParameterString("outName", false);
	if(filename.empty()){
		filename = extractBeforeLast(lociFilename, ".");
		filename += "_MCMC_output.txt";
	}

	logfile->list("Will write MCMC chain to '" + filename + "'");
	std::ofstream out(filename.c_str());
	printMCMCHeader(out);
	printMCMC(out, 0, curLL);

	int lastPrinted = 0;
	int sampling = 1;
	if(myParameters->parameterExists("sampling"))
		sampling = myParameters->getParameterInt("sampling");
	if(sampling == 1) logfile->list("Will write every iteration");
	else if(sampling == 2) logfile->list("Will write every 2nd iteration");
	else if(sampling == 3) logfile->list("Will write every 3rd iteration");
	else logfile->list("Will write every " + toString(sampling) + "th iteration");
	logfile->endIndent();
	logfile->endIndent();

	//prepare variables
	double hastings;
	MCMCcounters counters(numIterations, loci.size());

	//prepare output
	logfile->startIndent("Running an MCMC chain of length " + toString(numIterations) + ":");
	std::string progressString = "0% in 0.0 min";
	logfile->listFlush(progressString);

	//run iterations
	for(long i=1; i<(numIterations+1); ++i){
		//--------------------------------------------------
		//Update s
		//--------------------------------------------------
		if(priors->sIsEstimated()){
			//update s at each locus
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
				lociIt->updateSForMCMCStep(priors);
			}
			//now run MCMC step for each locus -> can now be parallelized, as they are independent
			#pragma omp parallel for num_threads(numCores)
			for(unsigned int l=0; l<loci.size(); ++l){
				pointerToLocus[l]->conductSUpdateMCMCStep();
			}
			//count acceptance
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
				lociIt->addSHUpdateToAcceptanceCounter(counters.s);
		}

		//--------------------------------------------------
		//Update h
		//--------------------------------------------------
		if(priors->hIsEstimated()){
			//update h at each locus
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
				lociIt->updateHForMCMCStep(priors);
			}
			//now run MCMC step for each locus -> can now be parallelized, as they are independent
			#pragma omp parallel for num_threads(numCores)
			for(unsigned int l=0; l<loci.size(); ++l){
				pointerToLocus[l]->conductHUpdateMCMCStep();
			}
			//count acceptance
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
				lociIt->addSHUpdateToAcceptanceCounter(counters.h);
		}

		//--------------------------------------------------
		//Update tau for each locus individually
		//--------------------------------------------------
		if(priors->tauIsEstimated()){
			//update tau for each locus
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
				lociIt->updateTauForMCMCStep(priors);
			}
			//no run MCMC step for each locus -> can now be parallelized, as they are independent
			#pragma omp parallel for num_threads(numCores)
			for(unsigned int l=0; l<loci.size(); ++l){
				pointerToLocus[l]->conductTauUpdateMCMCStep();
			}

		}

		//--------------------------------------------------
		//update curLL after locus specific updates
		//--------------------------------------------------
		if(priors->sIsEstimated() || priors->hIsEstimated() || priors->tauIsEstimated()){
			oldLL = 0.0;
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
				oldLL += lociIt->currentLikelihood();
			}
			curLL = oldLL;
		} else oldLL = curLL;

		//--------------------------------------------------
		//Now update N
		//--------------------------------------------------
		if(priors->NIsEstimated()){
			//update N
			priors->updateN();

			//update neutral transmat
			transMatNeutral->updateParameters(priors->currentTwoN(), 0.0, 0.0, priors->currentMutationRate());

			//calculate likelihood
			#pragma omp parallel for num_threads(numCores)
			for(unsigned int l=0; l<loci.size(); ++l){
				pointerToLocus[l]->updateN(priors->currentTwoN());
				pointerToLocus[l]->calcLogLikelihood();
			}

			//get new likelihood
			curLL = 0.0;
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
				curLL += lociIt->currentLikelihood();
			}

			//check acceptance
			hastings = curLL - oldLL;
			if(log(randomGenerator->getRand()) < hastings){
				//accept
				counters.N.add(true);
				oldLL = curLL;
			} else {
				//reject
				counters.N.add(false);
				priors->resetN();
				transMatNeutral->updateParameters(priors->currentTwoN(), 0.0, 0.0, priors->currentMutationRate());
				for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
					lociIt->resetN(priors->currentTwoN());
				}
				curLL = oldLL;
			}
		}

		//--------------------------------------------------
		//Now update error rate
		//--------------------------------------------------
		if(priors->errorRateIsEstimated()){
			//likelihood before is given by output from N update
			//update error rate
			priors->updateErrorRate();

			//calculate likelihood
			#pragma omp parallel for num_threads(numCores)
			for(unsigned int l=0; l<loci.size(); ++l){
				pointerToLocus[l]->updateErrorRate(u, priors->currentErrorRate());
				pointerToLocus[l]->calcLogLikelihood();
			}

			//get new likelihood
			oldLL = curLL;
			curLL = 0.0;
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
				curLL += lociIt->currentLikelihood();
			}

			//check acceptance
			hastings = curLL - oldLL;

			if(log(randomGenerator->getRand()) < hastings){
				//accept
				counters.error.add(true);
			} else {
				//reject
				counters.error.add(false);
				priors->resetErrorRate();

				for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
					lociIt->resetErrorRate();
				}
				curLL = oldLL;
			}
		}

		//--------------------------------------------------
		//Now update mutation rate
		//--------------------------------------------------
		if(priors->mutationRateIsEstimated()){
			//likelihood before is given by output from last update
			//update mutation rate
			priors->updateMutationRate();

			//update neutral transmat
			transMatNeutral->updateParameters(priors->currentTwoN(), 0.0, 0.0, priors->currentMutationRate());

			//calculate likelihood
			#pragma omp parallel for num_threads(numCores)
			for(unsigned int l=0; l<loci.size(); ++l){
				pointerToLocus[l]->updateMutationRate(priors->currentMutationRate());
				pointerToLocus[l]->calcLogLikelihood();
			}

			//get new likelihood
			oldLL = curLL;
			curLL = 0.0;
			for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
				curLL += lociIt->currentLikelihood();
			}

			//check acceptance
			hastings = curLL - oldLL;
			if(log(randomGenerator->getRand()) < hastings){
				//accept
				counters.mutation.add(true);
			} else {
				//reject
				counters.mutation.add(false);
				priors->resetMutationRate();
				transMatNeutral->updateParameters(priors->currentTwoN(), 0.0, 0.0, priors->currentMutationRate());
				for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
					lociIt->resetMutationRate(priors->currentMutationRate());
				}
				curLL = oldLL;
			}
		}

		//--------------------------------------------------
		//Write output and update counters
		//--------------------------------------------------
		//write to file
		++lastPrinted;
		if(lastPrinted == sampling){
			printMCMC(out, i, curLL);
			lastPrinted = 0;
		}

		//print progress
		if(counters.printProgress(i)){
			progressString = counters.progress() + " in " + counters.elapsedMinutes() + " min. LL = " + toString(curLL) + ". Acceptance rates:";
			if(priors->sIsEstimated()) progressString += " s = " + counters.s.getPercentAcceptanceRateString();
			if(priors->hIsEstimated()) progressString += " h = " + counters.h.getPercentAcceptanceRateString();
			if(priors->tauIsEstimated()) progressString += " tau = " + counters.tau.getPercentAcceptanceRateString();
			if(priors->NIsEstimated()) progressString += " N = " + counters.N.getPercentAcceptanceRateString();
			if(priors->mutationRateIsEstimated()) progressString += " mu = " + counters.mutation.getPercentAcceptanceRateString();
			if(priors->errorRateIsEstimated()) progressString += " eps = " + counters.error.getPercentAcceptanceRateString();
			logfile->listOverFlush(progressString + "   ");
		}
	}

	//close output file
	if(lastPrinted>0) printMCMC(out, numIterations, curLL);
	out.close();

	//print summary
	logfile->newLine();
	logfile->conclude("100% done in " + counters.elapsedMinutes() + "min");
	if(priors->sIsEstimated()) logfile->conclude("Acceptance rate for s updates was " + counters.s.getPercentAcceptanceRateString());
	if(priors->hIsEstimated())logfile->conclude("Acceptance rate for h updates was " + counters.h.getPercentAcceptanceRateString());
	if(priors->tauIsEstimated()) logfile->conclude("Acceptance rate for tau updates was " + counters.tau.getPercentAcceptanceRateString());
	if(priors->NIsEstimated()) logfile->conclude("Acceptance rate for N updates was " + counters.N.getPercentAcceptanceRateString());
	if(priors->mutationRateIsEstimated()) logfile->conclude("Acceptance rate for mutation rate (mu) updates was " + counters.mutation.getPercentAcceptanceRateString());
	if(priors->errorRateIsEstimated()) logfile->conclude("Acceptance rate for error rate (eps)  updates was " + counters.error.getPercentAcceptanceRateString());

	logfile->endIndent();
}

void TLoci::printMCMCHeader(std::ofstream & out){
	out << "index\tLL";
	if(priors->NIsEstimated()) out << "\tlog10(N)\tN";
	if(priors->errorRateIsEstimated()) out << "\tlog10(errorRate)";
	if(priors->mutationRateIsEstimated()) out << "\tlog10(mutationRate)";
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
		if(!lociIt->knownToBeNeutral()){
			if(priors->sIsEstimated()) out << "\ts_" << lociIt->name;		
			if(priors->hIsEstimated()) out << "\th_" << lociIt->name;
		}
		if(priors->tauIsEstimated()) out << "\ttau_" << lociIt->name;
	}
	out << std::endl;
}

void TLoci::printMCMC(std::ofstream & out, long iteration, double & curLL){
	out << iteration << "\t" << curLL;
	if(priors->NIsEstimated()){
		out << "\t" << priors->currentLogN() << "\t" << priors->currentN();
	}
	if(priors->errorRateIsEstimated()){
		out << "\t" << priors->currentLogErrorRate();
	}
	if(priors->mutationRateIsEstimated()){
		out << "\t" << priors->currentLogMutationRate();
	}
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
		if(!lociIt->knownToBeNeutral()){
			if(priors->sIsEstimated()) lociIt->writeS(out);
			if(priors->hIsEstimated()) lociIt->writeH(out);
		}
		if(priors->tauIsEstimated()) lociIt->writeTau(out);
	}
	out << std::endl;
}

void TLoci::initializeWFParameters(TParameters* myParameters){
	logfile->startIndent("Simulating data with the following parameters:");
	//read N
	int N = myParameters->getParameterLong("N");
	if(N < 2) throw "the population size N has to be larger than 2!";
	int twoN = 2 * N;
	logfile->list("Populations size N = " + toString(N) + " (2N = " + toString(twoN) + ")");

	//read s
	std::vector<std::string> string_vec;
	std::vector<double> s;
	myParameters->fillParameterIntoVector("s", string_vec, ',');
	repeatIndexes(string_vec, s);
	//parseSequenceWithCounts(string_vec, s);

	if(s.size()==1){
		//a single value
		logfile->list("Selection coefficient s = " + toString(s[0]));
		double selc = stringToDouble(string_vec[0]);
		for(unsigned int i=1; i<loci.size(); ++i)
			s.push_back(selc);
	} else {
		if(s.size() != loci.size())
			throw "Number of selection coefficients s is neither one nor does it match number of loci!";
		std::string tmp;
		concatenateString(s, tmp, ", ");
		logfile->list("Selection coefficients s = " + tmp);
	}

	//read h
	std::vector<double> h;
	myParameters->fillParameterIntoVector("h", string_vec, ',');
	repeatIndexes(string_vec, h);
	//parseSequenceWithCounts(string_vec, h);

	if(h.size()==1){
		logfile->list("Dominance h = " + toString(h[0]));
		for(unsigned int i=1; i<loci.size(); ++i)
			h.push_back(h[0]);
	} else {
		if(h.size() != loci.size())
			throw "Number of dominance values h is neither one nor does it match the number of loci!";
		std::string tmp;
		concatenateString(h, tmp, ", ");
		logfile->list("Dominance h = " + tmp);
	}
	logfile->endIndent();

	//mutation rate
	double mutationRate = myParameters->getParameterDoubleWithDefault("mutationRate", 0.0);
	if(mutationRate < 0.0  || mutationRate > 1.0) throw "Mutation rate has to be between 0 and 1!";
	logfile->list("Mutation rate = " + toString(mutationRate));

	//initialize parameters for each locus
	logfile->listFlush("Initializing parameters for each locus ...");
	std::vector<double>::iterator it, hIt;
	it = s.begin(); hIt = h.begin();
	for(lociIt=loci.begin();lociIt!=loci.end(); ++lociIt, ++it, ++hIt){
		lociIt->setWFParams(twoN, *it, *hIt, mutationRate);
		lociIt->initializeTimepoints();
	}
	logfile->write(" done!");
}

void TLoci::fillGrid(double* & grid, double _min, double _max, int positions){
	grid = new double[positions];
	double step = (_max - _min) / (positions - 1.0);
	for(int i=0; i<positions; ++i)
		grid[i] = _min + i*step;
};

void TLoci::calcLikelihoodSurface_N(TParameters* myParameters, TStatesU & u){
	logfile->startIndent("Calculating Likelihood surface:");

	//only work with first locus!
	if(loci.size()>1) logfile->warning("Will use same s for all loci!");

	//open output file and print header
	logfile->startIndent("Preparing output:");
	std::string filename = myParameters->getParameterStringWithDefault("outName", "likelihood_surface_N.txt");
	logfile->list("Will write likelihood surface to '" + filename + "'");
	std::ofstream out(filename.c_str());
	out << "log10N\ttwoN\tLL" << std::endl;
	logfile->endIndent();

	//read parameters
	double s = myParameters->getParameterDouble("s");
	double h = myParameters->getParameterDouble("h");
	int pos = myParameters->getParameterInt("positions");
	double mutationRate = myParameters->getParameterDoubleWithDefault("mutRate", 0.0);
	double errorRate = myParameters->getParameterDoubleWithDefault("errorRate", 0.0);

	//read gamma threshold
	double gammaThreshold = myParameters->getParameterDoubleWithDefault("gammaThreshold", 10.0);

	//set N grid
	double* log10N;
	fillGrid(log10N, myParameters->getParameterDoubleWithDefault("minLogN", 1.0), myParameters->getParameterDoubleWithDefault("maxLogN", 7.0), pos);

	//initialize first N
	TTransitionMatrix* transMat;
	logfile->listFlush("Initializing loci ...");
	//calc transmat first locus
	lociIt = loci.begin();
	int twoN = 2.0 * pow(10, log10N[0]);
	lociIt->initialize(twoN, s, h, mutationRate, u, gammaThreshold, errorRate);
	lociIt->initializeHMM();
	lociIt->calcLogLikelihood();
	double LL = lociIt->currentLikelihood();
	transMat = lociIt->getTransitionMatrixPointer();
	++lociIt;

	//reuse transmat on others
	for(; lociIt != loci.end(); ++lociIt){
		lociIt->initialize(transMat, twoN, s, h, mutationRate, u, gammaThreshold, errorRate);
		lociIt->initializeHMM();
		lociIt->calcLogLikelihood();
		LL += lociIt->currentLikelihood();
	}
	out << log10N[0] << "\t" << twoN << "\t" << LL << std::endl;
	logfile->write(" done!");
	logfile->conclude("first logLikelihood = " + toString(LL));

	//run over grid
	std::string progressString = "Calculating likelihood surface at " + toString(pos) + " positions ...";
	logfile->listFlush(progressString);
	int prog, oldProg = 0;

	for(int i=1; i<pos; ++i){
		//calc transmat first locus
		lociIt = loci.begin();
		twoN = 2.0 * pow(10, log10N[i]);
		lociIt->setN(twoN);
		lociIt->calcLogLikelihood();
		LL = lociIt->currentLikelihood();
		transMat = lociIt->getTransitionMatrixPointer();
		++lociIt;

		//reuse transmat for other loci
		for(; lociIt != loci.end(); ++lociIt){
			lociIt->setTransitionMatrix(transMat);
			lociIt->calcLogLikelihood();
			LL += lociIt->currentLikelihood();
		}
		out << log10N[i] << "\t" << twoN << "\t" << LL << std::endl;

		//report progress
		prog = 100 * (float) i / (float) pos;
		if(prog>oldProg){
			oldProg = prog;
			logfile->listOverFlush(progressString + '(' + toString(prog) + "%)");
		}
	}
	logfile->overList(progressString + " done! ");

	//close output file and clean up
	out.close();
	delete[] log10N;
}


void TLoci::calcLikelihoodSurface_s(TParameters* myParameters, TStatesU & u){
	logfile->startIndent("Calculating Likelihood surface on s:");

	//only work with first locus!
	if(loci.size()>1) logfile->warning("Will use same s for all loci!");

	//open output file and print header
	logfile->startIndent("Preparing output:");
	std::string filename = myParameters->getParameterStringWithDefault("outName", "likelihood_surface_s.txt");
	logfile->list("Will write likelihood surface to '" + filename + "'");
	std::ofstream out(filename.c_str());
	out << "s\tLL" << std::endl;
	logfile->endIndent();

	//read parameters
	int N = myParameters->getParameterDouble("N");
	int twoN = 2 * N;
	double h = myParameters->getParameterDouble("h");
	int pos = myParameters->getParameterInt("positions");
	double mutationRate = myParameters->getParameterDoubleWithDefault("mutRate", 0.0);
	double errorRate = myParameters->getParameterDoubleWithDefault("errorRate", 0.0);

	//read gamma threshold
	double gammaThreshold = myParameters->getParameterDoubleWithDefault("gammaThreshold", 10.0);

	//set N grid
	double* s;
	fillGrid(s, myParameters->getParameterDoubleWithDefault("min_s", -0.1), myParameters->getParameterDoubleWithDefault("max_s", 0.1), pos);

	//initialize first s
	TTransitionMatrix* transMat;
	logfile->listFlush("Initializing loci ...");
	//calc transmat first locus
	lociIt = loci.begin();

	lociIt->initialize(twoN, s[0], h, mutationRate, u, gammaThreshold, errorRate);
	lociIt->initializeHMM();
	lociIt->calcLogLikelihood();
	double LL = lociIt->currentLikelihood();
	transMat = lociIt->getTransitionMatrixPointer();
	++lociIt;

	//reuse transmat on others
	for(; lociIt != loci.end(); ++lociIt){
		lociIt->initialize(transMat, twoN, s[0], h, mutationRate, u, gammaThreshold, errorRate);
		lociIt->initializeHMM();
		lociIt->calcLogLikelihood();
		LL += lociIt->currentLikelihood();
	}
	out << s[0] << "\t" << LL << std::endl;
	logfile->write(" done!");
	logfile->conclude("first logLikelihood = " + toString(LL));

	//run over grid
	std::string progressString = "Calculating likelihood surface at " + toString(pos) + " positions ...";
	logfile->listFlush(progressString);
	int prog, oldProg = 0;

	for(int i=1; i<pos; ++i){
		//calc transmat first locus
		lociIt = loci.begin();
		lociIt->set_s(s[i]);
		lociIt->calcLogLikelihood();
		LL = lociIt->currentLikelihood();
		transMat = lociIt->getTransitionMatrixPointer();
		++lociIt;

		//reuse transmat for other loci
		for(; lociIt != loci.end(); ++lociIt){
			lociIt->setTransitionMatrix(transMat);
			lociIt->calcLogLikelihood();
			LL += lociIt->currentLikelihood();
		}
		out << s[i] << "\t" << LL << std::endl;

		//report progress
		prog = 100 * (float) i / (float) pos;
		if(prog>oldProg){
			oldProg = prog;
			logfile->listOverFlush(progressString + '(' + toString(prog) + "%)");
		}
	}
	logfile->overList(progressString + " done! ");

	//close output file and clean up
	out.close();
	delete[] s;
}

void TLoci::simulateData(TParameters* myParameters, TStatesU & u, double & gammaThreshold){
	//initializing transition matrices
	logfile->listFlush("Initializing transition matrices ... (0%)");
	int counter = 1;
	int prog = 0;
	int oldProg = 0;
	for(lociIt=loci.begin();lociIt!=loci.end(); ++lociIt, ++counter){
		lociIt->initializeTransitionMatrixPair(u, gammaThreshold);
		prog = 100.0 * (double) counter / (double) loci.size();
		if(prog>oldProg){
			oldProg = prog;
			logfile->listOverFlush("Initializing transition matrices ... (" + toString(prog) + "%)");
		}
	}
	logfile->overList("Initializing transition matrices ... done!  ");

	//initial frequency
	std::vector<std::string> string_vec;
	std::vector<double> f;
	std::vector<int> initialStates;
	myParameters->fillParameterIntoVector("f", string_vec, ',');
	if(string_vec.size()==1 && string_vec[0]=="RAND"){
		logfile->list("Starting simulations at random initial frequencies");
		for(unsigned int i=0; i<loci.size(); ++i)
			initialStates.push_back(u.getBestMatchingState(randomGenerator->getRand()));
	} else {
		repeatIndexes(string_vec, f);
		if(f.size()==1){
			int state = u.getBestMatchingState(f[0]);
			logfile->list("Starting simulations at initial frequency (best matching state) f = " + toString(u[state]));
			for(unsigned int i=0; i<loci.size(); ++i)
				initialStates.push_back(state);
		} else {
			if(f.size() != loci.size())
				throw "Number of initial frequencies f is neither one nor does it match the number of loci!";
			//get matching states
			for(std::vector<double>::iterator itF=f.begin(); itF!=f.end(); ++itF)
				initialStates.push_back(u.getBestMatchingState(*itF));
			std::vector<int>::iterator stateIt=initialStates.begin();
			std::string tmp = toString(u[*stateIt]); ++stateIt;
			for(; stateIt!=initialStates.end(); ++stateIt)
				tmp += ", " + toString(u[*stateIt]);
			logfile->list("Starting simulations at initial frequencies (best matching states) f = " + tmp);
		}
	}

	//error rate
	double errorRate = myParameters->getParameterDoubleWithDefault("errorRate", 0.0);
	if(errorRate < 0.0  || errorRate > 0.5) throw "Error rate has to be between 0 and 0.5!";
	logfile->list("Using an error rate of " + toString(errorRate));

	//output
	std::string outName = myParameters->getParameterStringWithDefault("outName", "ApproxWF_simulation") + "_";
	bool outputAsWFABC = myParameters->parameterExists("WFABCformat");

	//do we calculate statistics?
	bool calcStats = myParameters->parameterExists("calcStats");
	std::ofstream stats;
	if(calcStats){
		stats.open((outName + "statistics.txt").c_str());
		writeStatHeader(stats);
	}

	//run simulations
	std::string filename;
	int rep = myParameters->getParameterInt("rep", false);
	if(rep<1) rep = 1;
	logfile->startIndent("Performing " + toString(rep) + " simulation(s):");
	for(int i=0; i<rep; ++i){
		//perform simulation
		logfile->listFlush("Running simulation " + toString(i+1) + " ...");
		std::vector<int>::iterator stateIt=initialStates.begin();
		for(lociIt=loci.begin();lociIt!=loci.end(); ++lociIt, ++stateIt){
			lociIt->simulateData(*stateIt, errorRate, u);
		}
		logfile->write(" done!");

		//write output
		if(outputAsWFABC) printWFABCFormat(outName + toString(i) + ".txt");
		else print(outName + toString(i) + ".txt");
		if(calcStats) writeStats(stats, i);
	}
	if(calcStats) stats.close();
	logfile->endIndent();
}

void TLoci::simulateDataWF(TParameters* myParameters){
	//initial frequency
	std::vector<std::string> string_vec;
	std::vector<double> f;
	myParameters->fillParameterIntoVector("f", string_vec, ',');
	if(string_vec.size()==1 && string_vec[0]=="RAND"){
		logfile->list("Starting simulations at random initial frequencies");
		for(unsigned int i=0; i<loci.size(); ++i)
			f.push_back(randomGenerator->getRand());
	} else {
		repeatIndexes(string_vec, f);
		if(f.size()==1){
			logfile->list("Starting simulations at initial frequency f = " + toString(f[0]));
			for(unsigned int i=0; i<loci.size(); ++i)
				f.push_back(f[0]);
		} else {
			if(f.size() != loci.size())
				throw "Number of initial frequencies f is neither one nor does it match the number of loci!";
			std::vector<double>::iterator it=f.begin();
			std::string tmp = toString(*it); ++it;
			for(; it!=f.end(); ++it)
				tmp += ", " + toString(*it);
			logfile->list("Starting simulations at initial frequencies f = " + tmp);
		}
	}

	//error rate
	double errorRate = myParameters->getParameterDoubleWithDefault("errorRate", 0.0);
	if(errorRate < 0.0  || errorRate > 0.5) throw "Error rate has to be between 0 and 0.5!";
	logfile->list("Using an error rate of " + toString(errorRate));

	//mutation rate
	double mutationRate = myParameters->getParameterDoubleWithDefault("mutRate", 0.0);
	if(mutationRate < 0.0  || mutationRate > 1.0) throw "Mutation rate has to be between 0 and 1!";
	logfile->list("Using a mutation rate of " + toString(mutationRate));

	//output
	std::string outName = myParameters->getParameterStringWithDefault("outName", "WF_simulation") + "_";
	bool outputAsWFABC = myParameters->parameterExists("WFABCformat");

	//do we calculate statistics?
	bool calcStats = myParameters->parameterExists("calcStats");
	std::ofstream stats;
	if(calcStats){
		stats.open((outName + "statistics.txt").c_str());
		writeStatHeader(stats);
	}

	//run simulations
	std::string filename;
	int rep = myParameters->getParameterInt("rep", false);
	if(rep<1) rep = 1;
	logfile->startIndent("Performing " + toString(rep) + " simulation(s):");
	for(int i=0; i<rep; ++i){
		//perform simulation
		logfile->listFlush("Running simulation " + toString(i+1) + " ...");
		std::vector<double>::iterator freqIt=f.begin();
		for(lociIt=loci.begin();lociIt!=loci.end(); ++lociIt, ++freqIt){
			lociIt->simulateDataWF(*freqIt, errorRate, mutationRate);
		}
		logfile->write(" done!");

		//write output
		if(outputAsWFABC) printWFABCFormat(outName + toString(i) + ".txt");
		else print(outName + toString(i) + ".txt");
		if(calcStats) writeStats(stats, i);
	}
	logfile->endIndent();
	if(calcStats) stats.close();

}


void TLoci::print(std::string filename){
	logfile->listFlush("Writing simulated data to file '" + filename + " ...");
	std::ofstream out(filename.c_str());
	if(!out) throw "Failed to open output file '" + filename + "'!";

	//write header with time points
	out << "time";
	for(std::vector<int>::iterator timeIt = allTimePoints.begin(); timeIt!=allTimePoints.end(); ++timeIt)
			out << "\t" << *timeIt;
	out << "\n";

	//write one locus per line
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
		out << lociIt->name;

		//write time points
		for(std::vector<int>::iterator timeIt = allTimePoints.begin(); timeIt!=allTimePoints.end(); ++timeIt){
			out << "\t" << lociIt->timePoints.getSampleStringAt(*timeIt);
		}
		out << "\n";
	}
	out.close();

	logfile->write(" done!");
}

void TLoci::printWFABCFormat(std::string filename){
	std::ofstream out(filename.c_str());
	if(!out) throw "Failed to open output file '" + filename + "'!";

	//write number of loci and time points
	out << loci.size() << "\t" << allTimePoints.size() << std::endl;

	//write time points
	std::vector<int>::iterator timeIt = allTimePoints.begin();
	out << *timeIt; ++timeIt;
	for(; timeIt!=allTimePoints.end(); ++timeIt)
		out << ',' << *timeIt;
	out << std::endl;

	//write loci
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
		//write sample sizes
		timeIt = allTimePoints.begin();
		out << lociIt->timePoints.getSampleSizeAt(*timeIt); ++timeIt;
		for(; timeIt!=allTimePoints.end(); ++timeIt){
			out << "," << lociIt->timePoints.getSampleSizeAt(*timeIt);
		}
		out << std::endl;
		//write numA
		timeIt = allTimePoints.begin();
		out << lociIt->timePoints.getNumAAt(*timeIt); ++timeIt;
		for(; timeIt!=allTimePoints.end(); ++timeIt){
			out << "," << lociIt->timePoints.getNumAAt(*timeIt);
		}
		out << std::endl;
	}
	out.close();
}

void TLoci::writeStatHeader(std::ofstream & file){
	file << "run";
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
		lociIt->writeStatHeader(file);
	file << std::endl;
};

void TLoci::writeStatHeaderTransposed(std::ofstream & file){
	file << "Locus";
	lociIt->writeStatHeaderNoName(file);
	file << std::endl;
};


void TLoci::writeStats(std::ofstream & file, int & run){
	file << run;
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt)
		lociIt->writeStats(statSettings, file);
	file << std::endl;
}

void TLoci::writeStatsTransposed(std::ofstream & file, int & run){
	for(lociIt = loci.begin(); lociIt!=loci.end(); ++lociIt){
		file << lociIt->name;
		lociIt->writeStats(statSettings, file);
		file << "\n";
	}
};
