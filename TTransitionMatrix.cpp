/*
 * TTransitionMatrix.cpp
 *
 *  Created on: May 2, 2014
 *      Author: wegmannd
 */

#include "TTransitionMatrix.h"

//---------------------------------------------------------------------------------------------------
//TStatesU
//---------------------------------------------------------------------------------------------------
//TStatesU::TStatesU(int NumStates, WFParameters & WfParams, bool UniformStates){
TStatesU::TStatesU(int NumStates, bool UniformStates){
	numStates = NumStates;
	uniformStates = UniformStates;

	//initialize states
	u = new double[numStates];
	uInitialized=true;
	u[0] = 0.0;
	if(uniformStates){
		for(int k=1; k<numStates; ++k)	u[k] = k / (numStates-1.0);
	} else {
		for(int k=1; k<numStates; ++k){
			double x = (k - 0.5) / (double) numStates;
			u[k] = u[k-1] + x*(1-x);
		}
		for(int k=1; k<numStates; ++k) u[k] = u[k] / u[numStates-1];
	}



	 // Version with 1 and N-1 step

	//initialize states
	/*
	u = new double[numStates];
	uInitialized=true;
	u[0]=0.0;
	u[numStates-1]=1.0;
	//first and last are 1/2N and (2N-1)/2N
	u[1] = 1.0 / WfParams.twoNDouble;
	u[numStates-2] = 1.0 - 1.0 / WfParams.twoNDouble;
	//fill in all others
	if(uniformStates){
		double step = (u[numStates-2] - u[1]) / (numStates-3.0);
		for(int k=2; k<(numStates-2); ++k)	u[k] = (k-1) * step + u[1];
	} else {
		double* steps = new double[numStates-3];
		double x;
		double sum = 0;
		for(int k=0; k<(numStates-3); ++k){
			x = (k + 0.5) / (double) (numStates-3); //-1.5?
			steps[k] = x*(1-x);
			sum += steps[k];
		}
		double diff = u[numStates-2] - u[1];
		for(int k=0; k<(numStates-3); ++k){
			steps[k] = steps[k] / sum * diff;
		}
		for(int k=2; k<(numStates-2); ++k){
			u[k] = u[k-1] + steps[k-2];
		}
		delete[] steps;
	}
*/
}

void TStatesU::print(){
	for(int k=0; k<numStates; ++k){
		std::cout << k << "\t";
		printf("%1.10e", u[k]);
		std::cout << std::endl;
	}
}

void TStatesU::write(std::ofstream & out){
	out << u[0];
	for(int k=1; k<numStates; ++k){
		out << "\t" << u[k];
	}
	out << std::endl;
}

int TStatesU::getBestMatchingState(double f){
	//find state that is closest
	if(uniformStates){
		return round(f / (u[2]-u[1]));
	} else {
		//implement simples algo...
		int k=1;
		while(u[k]<f) ++k;
		//above or below?
		if(u[k]-f < f-u[k-1]) return k;
		else return k-1;
	}
}

//---------------------------------------------------------------------------------------------------
//TTransitionMatrix
//---------------------------------------------------------------------------------------------------
TTransitionMatrix::TTransitionMatrix(TRandomGenerator* RandomGenerator){
	randomGenerator = RandomGenerator;
	initialized = false;
}
TTransitionMatrix::TTransitionMatrix(int & twoN, double & s, double & h, double & mutationRate, TRandomGenerator* RandomGenerator){
	randomGenerator = RandomGenerator;
	initialized = false;
	calcTransitionMatrixOneGeneration(twoN, s, h, mutationRate);
}

bool TTransitionMatrix::updateParameters(int twoN, double s, double h, double mutationRate){
	//delete all transition matrices calculated
	if(initialized) clear();
	return calcTransitionMatrixOneGeneration(twoN, s, h, mutationRate);

}
void TTransitionMatrix::fillExponential(int & deltaT){
	if(!initialized) throw "TTransitionMatrix has not yet been initialized!";
	//check if it exists already
	if(transitionMatrices.find(deltaT)==transitionMatrices.end()){
		//Use existing to compute transition matrix. Specifically, build up series Q(2), Q(4), Q(8), ... generations from generation Q(1)
		//Then can then be used to calculate Q(x)
		int largestBinary = log(deltaT)/log(2.0);
		int largestNatural = pow(2.0, largestBinary);
		if(largestNatural==deltaT){
			//just square matrix for t=deltaT/2
			int need = largestNatural / 2;
			fillExponential(need);
			transitionMatrices.emplace(deltaT,new TSquareMatrixStorage(transitionMatrices[1]->size));
			transitionMatrices.at(deltaT)->fillFromSquare(*transitionMatrices.find(need)->second);
		} else {
			//use largest and add missing recursively
			fillExponential(largestNatural);
			int diff = deltaT - largestNatural;
			fillExponential(diff);
			transitionMatrices.emplace(deltaT,new TSquareMatrixStorage);
			transitionMatrices.at(deltaT)->fillFromProduct(*transitionMatrices.find(largestNatural)->second, *transitionMatrices.find(diff)->second);
		}
	}
}

//-------------------------------------------------------------------
//access matrices / simulate from matrices
TSquareMatrixStorage* TTransitionMatrix::getPointerToTransitionMatrix(int & deltaT){
	tmIt=transitionMatrices.find(deltaT);
	if(tmIt==transitionMatrices.end()){
		fillExponential(deltaT);
		tmIt=transitionMatrices.find(deltaT);
	}
	return tmIt->second;
}

int TTransitionMatrix::simulate(int & curState, int & deltaT){
	double* transP = (*getPointerToTransitionMatrix(deltaT))[curState];
	double r=randomGenerator->getRand();
	int newState=0;
	double cumul=transP[newState];
	while(r > cumul){
		++newState;
		cumul+=transP[newState];
	}
	return newState;
}
int TTransitionMatrix::simulateGeneration(int & curState){
	double* transP = (*transitionMatrices[1])[curState];
	double r=randomGenerator->getRand();
	int newState=0;
	double cumul=transP[newState];
	int lastMinusOne = transitionMatrices[1]->size - 1;
	while(r > cumul && newState < lastMinusOne){
		++newState;
		cumul+=transP[newState];
	}
	return newState;
}
//-------------------------------------------------------------------
//printing
void TTransitionMatrix::printTransitionMatrix(int deltaT){
	tmIt=transitionMatrices.find(deltaT);
	if(tmIt==transitionMatrices.end()) throw "Transition matrix for " + toString(deltaT) +" generations has not been calculate yet!";
	tmIt->second->print();
}

void TTransitionMatrix::writeTransitionMatrix(int deltaT, std::ofstream & out){
	tmIt=transitionMatrices.find(deltaT);
	if(tmIt==transitionMatrices.end()) throw "Transition matrix for " + toString(deltaT) +" generations has not been calculate yet!";
	tmIt->second->write(out);
}

void TTransitionMatrix::printRowSums(int deltaT){
	tmIt=transitionMatrices.find(deltaT);
	if(tmIt==transitionMatrices.end()) throw "Transition matrix for " + toString(deltaT) +" generations has not been calculate yet!";

	for(int i=0; i<tmIt->second->size; ++i){
		double sum=0;
		for(int j=0; j<tmIt->second->size; ++j){
			sum+=tmIt->second->at(i,j);
		}
		std::cout << sum << std::endl;
	}
}

void TTransitionMatrix::printColSums(int deltaT){
	tmIt=transitionMatrices.find(deltaT);
	if(tmIt==transitionMatrices.end()) throw "Transition matrix for " + toString(deltaT) +" generations has not been calculate yet!";
	for(int j=0; j<tmIt->second->size; ++j){
		double sum=0;
		for(int i=0; i<tmIt->second->size; ++i){
			sum+=tmIt->second->at(i,j);
		}
		std::cout << sum << std::endl;
	}
}

double TTransitionMatrix::getColSum(int deltaT, int col){
	tmIt=transitionMatrices.find(deltaT);
	if(tmIt==transitionMatrices.end()) throw "Transition matrix for " + toString(deltaT) +" generations has not been calculate yet!";
	if(col >= tmIt->second->size) throw "Transition matrix does not contain column " + toString(col) + "!";

	double sum=0;
	for(int i=0; i<tmIt->second->size; ++i){
		sum+=tmIt->second->at(i,col);
	}
	return sum;
}

void TTransitionMatrix::printCalculatedDeltaTs(){
	std::cout << "deltaTs for which the transition matrices have been calculated: ";
	for(tmIt=transitionMatrices.begin(); tmIt!=transitionMatrices.end(); ++tmIt){
		if(tmIt!=transitionMatrices.begin()) std::cout << ", ";
		std::cout << tmIt->first;
	}
}
//---------------------------------------------------------------------------------------------------
//TTransitionMatrixApprox
//---------------------------------------------------------------------------------------------------
TTransitionMatrixApprox::TTransitionMatrixApprox(TStatesU* statesU, TRandomGenerator* RandomGenerator):TTransitionMatrix(RandomGenerator){
	u = statesU;
	initialized = false;
	init();
}

TTransitionMatrixApprox::TTransitionMatrixApprox(TStatesU* statesU, double gammaThreshold, TRandomGenerator* RandomGenerator):TTransitionMatrixApprox(statesU, RandomGenerator){
	gammaThresholdForLargeGammaApproximation = gammaThreshold;
}

TTransitionMatrixApprox::TTransitionMatrixApprox(int twoN, double s, double h, double mutationRate, TStatesU* statesU, double gammaThreshold, TRandomGenerator* RandomGenerator):TTransitionMatrixApprox(statesU, RandomGenerator){
	gammaThresholdForLargeGammaApproximation = gammaThreshold;
	calcTransitionMatrixOneGeneration(twoN, s, h, mutationRate);
}

void TTransitionMatrixApprox::init(){
	integrationWeightsInitialized=false;
	gammaThresholdForLargeGammaApproximation = 10.0;
}
//-------------------------------------------------------------------
//initialize
void TTransitionMatrixApprox::initializeIntegrationWeights(){
	//Weights according to extended Simpson's rule
	if(!integrationWeightsInitialized){
		integrationSteps = 11;
		integrationWeights = new double[integrationSteps];
		integrationWeights[0] = 1.0/3.0;
		for(int i=1; i<(integrationSteps-1); i=i+2){
			integrationWeights[i] = 4.0/3.0;
			integrationWeights[i+1] = 2.0/3.0;
		}
		integrationWeights[integrationSteps-1] = 1.0/3.0;
		integrationWeightsInitialized = true;
	}
}
void TTransitionMatrixApprox::initializeTransitionMatrixQ(){
	//NOTE: first row and last col are zero as we do not allow for mutation / back mutation
	inititalTransitionMatrixQ.resize(u->numStates, 1);
	inititalTransitionMatrixQ(0, 0) = 0.0;
	inititalTransitionMatrixQ(0, 1) = 0.0;
	inititalTransitionMatrixQ(u->numStates-1, u->numStates-1) = 0.0;
	inititalTransitionMatrixQ(u->numStates-1, u->numStates-2) = 0.0;
}
//-------------------------------------------------------------------
//calculate transition matrices
void TTransitionMatrixApprox::calcInitialTransitionMatrixQ(int & twoN, double & s, double & h, double & mutationRate){
	//prepare Q
	initializeTransitionMatrixQ();

	if(s < 1.0E-7 && s > -1.0E-7){
		//Neutral case
		fillQNeutral(twoN, mutationRate);
	} else {
		 //case in which there is selection
		 if(s > 0.0) fillQ(twoN, s, h, mutationRate);
		 else {
			 //if s is negative, calculate for other allele
			 double s_neg = - s / (1.0 + s);
			 double h_neg = 1.0 - h;
			 fillQ(twoN, s_neg, h_neg, mutationRate);

			 //and now swap frequencies
			 double tmp;
			 for(int k=0; k<(u->numStates-1); ++k){
				 tmp = inititalTransitionMatrixQ(k, k+1);
				 inititalTransitionMatrixQ(k, k+1)  = inititalTransitionMatrixQ(u->numStates - k - 1, u->numStates - k - 2);
				 inititalTransitionMatrixQ(u->numStates - k - 1, u->numStates - k - 2) = tmp;
			 }
		 }
	 }

	//set diagonal
	inititalTransitionMatrixQ(0, 0) = - inititalTransitionMatrixQ(0, 1);
	for(int k=1; k<(u->numStates-1); ++k){
		inititalTransitionMatrixQ(k, k) = - inititalTransitionMatrixQ(k, k-1) - inititalTransitionMatrixQ(k, k+1);
	}
	inititalTransitionMatrixQ(u->numStates-1, u->numStates-1) = - inititalTransitionMatrixQ(u->numStates-1, u->numStates-2);

	//std::cout << std::endl << "------------------Q------------------------" << std::endl;
	//inititalTransitionMatrixQ.print();
	//inititalTransitionMatrixQ.printDiag();
	//std::cout << "------------------Q------------------------" << std::endl;
}

void TTransitionMatrixApprox::fillQNeutral(int & twoN, double & mutationRate){
	double E_minT, tmp, P_down;
	double fourN = 2.0 * (double) twoN;
	for(int k=1; k<(u->numStates-1); ++k){
		if((*u)[k-1]>0.0) tmp = (*u)[k-1] * log((*u)[k-1]/(*u)[k]);
		else tmp = 0.0;
		E_minT = ((*u)[k+1] - (*u)[k])*( tmp + (1.0-(*u)[k-1]) * log((1.0-(*u)[k-1])/(1.0-(*u)[k])));

		if((*u)[k+1]<1.0) tmp = (1.0-(*u)[k+1]) * log((1.0-(*u)[k+1])/(1.0-(*u)[k]));
		else tmp = 0.0;
		E_minT += ((*u)[k] - (*u)[k-1])*((*u)[k+1] * log((*u)[k+1]/(*u)[k]) + tmp);
		E_minT *= fourN / ((*u)[k+1] - (*u)[k-1]);

		P_down = ((*u)[k+1]-(*u)[k]) / ((*u)[k+1]-(*u)[k-1]);

		//store in matrix
		inititalTransitionMatrixQ(k, k-1) = P_down / E_minT;
		inititalTransitionMatrixQ(k, k+1) = (1.0 - P_down) / E_minT;
	}

	//Introduce mutations
	if(mutationRate > 0.0){
		//calculate transition form 0 -> u[1] with mutation rate
		inititalTransitionMatrixQ(0, 1) = mutationRate /(*u)[1];
		inititalTransitionMatrixQ(u->numStates - 1, u->numStates - 2) = mutationRate / (1.0 - (*u)[u->numStates-2]);
	 }
}

void TTransitionMatrixApprox::fillQ(int & twoN, double & s, double & h, double & mutationRate){
	//make sure integration weights are initialized
	initializeIntegrationWeights();

	//prepare variables
	double fourN = 2.0 * (double) twoN;
	double sigma_k, sigma_bar_k, u1, x, u2, y, pDown, pUp, increment, tmp, tmp2, tmp3, integralDown, integralUp;
	double integrationStepsMinusOne = integrationSteps - 1.0;
	double E_minT;
	double M_k, fourNByM;

	//loop over states
	for(int k=1; k<(u->numStates-1); ++k){
		//prepare variables
		u1 = (*u)[k-1];
		x = (*u)[k];
		u2 = (*u)[k+1];

		sigma_bar_k = s * (h + x * (1.0 - 2.0 * h));
		sigma_k = s * (2.0*h + x * (1.0 - 2.0 * h));
		M_k = fourN * sigma_bar_k / sigma_k - 1.0;

		fourNByM = fourN / M_k;

		//check if we use approximation
		if(twoN * sigma_k * (u2 - u1) > gammaThresholdForLargeGammaApproximation){
			//cal integral down
			if(k == 1){
				integralDown = 1.0 / (fourN * s * s * h * h * x);
			} else {
				tmp = (1.0 + sigma_k * x);
				integralDown =  tmp * tmp / (M_k * sigma_k * sigma_bar_k * x * (1.0 - x));
			}

			//calc integral up
			if(k == u->numStates-2){
				//tmp = (1.0 + sigma_k) / (sigma_k * (1.0 - h));
				tmp = (1.0 + s) / (s * (1.0 - h));
				integralUp = tmp * (log(fourN / tmp * (1.0 - x)) + 0.577215665);
			} else {
				tmp = (1.0 + sigma_k * u2);
				integralUp = (log(u2/x) - (1.0 + sigma_k) * log((1.0 - u2) / (1.0 - x))
						- tmp * tmp / (M_k * sigma_k * u2 * (1.0 - u2))) / sigma_bar_k;
			}

			//calc transition prob
			inititalTransitionMatrixQ(k, k-1) = 0.0;
			inititalTransitionMatrixQ(k, k+1) = 1.0 / (integralDown + integralUp);
		} else {
			//numerical integration
			tmp2 = 1.0 + sigma_k * u1;
			tmp3 = 1.0 + sigma_k * u2;

			//calculate pDown
			pDown = (pow(tmp3 / (1.0 + sigma_k * x ), M_k) - 1.0)
				  / (pow(tmp3 / tmp2, M_k) - 1.0);
			pUp = 1.0 - pDown;

			//Numerical integration: lower segment
			increment = (x - u1) / integrationStepsMinusOne;
			integralDown = 0.0;
			for(int i=1; i<integrationSteps; ++i){
				y = u1 + i*increment + 1.0E-50;
				tmp = (1.0 + sigma_k * y);
				integralDown += integrationWeights[i] *  tmp / (y * (1.0 - y)) * (pow(tmp / tmp2, M_k) - 1.0);
			}
			integralDown *= increment;

			//Numerical integration: upper segment
			increment = (u2 - x) / integrationStepsMinusOne;
			integralUp = 0.0;
			for(int i=0; i<(integrationSteps-1); ++i){
				y = x + i*increment - 1.0E-50;
				tmp = (1.0 + sigma_k * y);
				integralUp += integrationWeights[i] *  tmp / (y * (1.0 - y)) * (1.0 - pow(tmp / tmp3, M_k));
			}
			integralUp *= increment;

			//calc transition probs and fill matrix
			E_minT = (integralDown * pDown + integralUp * pUp) * fourNByM / sigma_k;

			inititalTransitionMatrixQ(k, k-1) = pDown / E_minT;
			inititalTransitionMatrixQ(k, k+1) = pUp / E_minT;
		}
	}

	//Introduce mutations
	if(mutationRate > 0.0){
		double minusM = 1.0 - 2.0 * (double) twoN;
		//calculate transition form 0 -> u[1] with mutation rate
		double sx = s * (h + (*u)[1]/2.0 * (1.0 - 2.0*h));
		double pUp = 1.0;
		//check if we do not use approximation
		if(twoN * sx * (*u)[1] < 10.0){
			pUp = (1.0 - pow(1.0 + sx / (double) twoN, minusM)) / (1.0 - pow(1.0 + sx * (*u)[1], minusM));
		}
		inititalTransitionMatrixQ(0, 1) = (double) twoN * mutationRate * pUp;

		//calculate transition form 1 -> u[K] with mutation rate
		sx = s * (h + (*u)[u->numStates-2]/2.0 * (1.0 - 2.0*h));
		double pDown = 0.0;
		double tmp = pow(1.0 + sx, minusM);
		//check if we do not use approximation
		if((double) twoN * sx * (*u)[u->numStates-2] < 10.0){
			pDown = (pow(1.0 + sx * (1.0 - 1.0 / (double) twoN), minusM) - tmp) / (pow(1.0 + sx * (*u)[u->numStates-2], minusM) - tmp);
		}
		inititalTransitionMatrixQ(u->numStates - 1, u->numStates - 2) = (double) twoN * mutationRate * pDown;
	 }
}

bool TTransitionMatrixApprox::calcTransitionMatrixOneGeneration(int & twoN, double & s, double & h, double & mutationRate){
	//first calc initial transition matrix Q
	calcInitialTransitionMatrixQ(twoN, s, h, mutationRate);

	transitionMatrices.emplace(1,new TSquareMatrixStorage);
	initialized = true;

	return transitionMatrices.at(1)->fillAsExponential(inititalTransitionMatrixQ);

/*
	std::cout << "------------------Q------------------------" << std::endl;
	inititalTransitionMatrixQ.print();
	std::cout << "------------------Q------------------------" << std::endl;

	std::cout << "------------------exp(Q)------------------------" << std::endl;
	transitionMatrices.at(1)->print();
	std::cout << "------------------exp(Q)------------------------" << std::endl;
*/
}


//---------------------------------------------------------------------------------------------------
//TTransitionMatrixMalaspinas
//---------------------------------------------------------------------------------------------------
TTransitionMatrixMalaspinas::TTransitionMatrixMalaspinas(TStatesU* statesU, TRandomGenerator* RandomGenerator):TTransitionMatrixApprox(statesU, RandomGenerator){}

TTransitionMatrixMalaspinas::TTransitionMatrixMalaspinas(int twoN, double s, double h, double mutationRate, TStatesU* statesU, TRandomGenerator* RandomGenerator):TTransitionMatrixApprox(statesU, RandomGenerator){
	calcTransitionMatrixOneGeneration(twoN, s, h, mutationRate);
}


void TTransitionMatrixMalaspinas::calc_a_b(double x, const double & twoN, const double & s, const double & h, double & a, double & b){
	double tmp = x * (1.0 - x);
	a = tmp / twoN;
	double twosh = 2.0 * s * h;
	b = tmp * s * (x + h - twosh) / (1.0 + s*x * (x + 2.0*h - twosh));
};

void TTransitionMatrixMalaspinas::fillQ(int & twoN, double & s, double & h, double & mutationRate){
	//prepare variables
	double x;
	double delta_k, delta_k_minus_1;
	double a, b, sumDelta;

	//loop over states
	for(int k=1; k<(u->numStates-1); ++k){
		//prepare variables
		x = (*u)[k];
		delta_k = (*u)[k+1] - x;
		delta_k_minus_1 = x - (*u)[k-1];

		calc_a_b(x, twoN, s, h, a, b);

		sumDelta = (delta_k_minus_1 + delta_k);
		inititalTransitionMatrixQ(k, k+1) = (b * delta_k_minus_1 + a) / (delta_k * sumDelta);
		inititalTransitionMatrixQ(k, k-1) = (a - b * delta_k) / (delta_k_minus_1 * sumDelta);

		if(inititalTransitionMatrixQ(k, k-1) < 0.0) throw "Problem calculating Malaspinas Q matrix: delta_i < 0.0!";
	}
}

void TTransitionMatrixMalaspinas::fillQNeutral(int & twoN, double & mutationRate){
	//prepare variables
	double x;
	double delta_k, delta_k_minus_1;
	double a, sumDelta;

	//loop over states
	for(int k=1; k<(u->numStates-1); ++k){
		//prepare variables
		x = (*u)[k];
		delta_k = (*u)[k+1] - x;
		delta_k_minus_1 = x - (*u)[k-1];

		a = x * (1.0 - x) / twoN;

		sumDelta = (delta_k_minus_1 + delta_k);
		inititalTransitionMatrixQ(k, k+1) = a / (delta_k * sumDelta);
		inititalTransitionMatrixQ(k, k-1) = a / (delta_k_minus_1 * sumDelta);
	}
}

//---------------------------------------------------------------------------------------------------
//TTransitionMatrixWF
//---------------------------------------------------------------------------------------------------
TTransitionMatrixWF::TTransitionMatrixWF(int twoN, double s, double h, double mutationRate, TRandomGenerator* RandomGenerator):TTransitionMatrix(RandomGenerator){
	initialized = false;
	numStates=twoN+1;
	calcTransitionMatrixOneGeneration(twoN, s, h, mutationRate);
}

//-------------------------------------------------------------------
//calculate transition matrices
bool TTransitionMatrixWF::calcTransitionMatrixOneGeneration(int & twoN, double & s, double & h, double & mutationRate){
	//use binomial sampling to fill transition matrices
	//fill by row to reuse scaling of s
	transitionMatrices.emplace(1,new TSquareMatrixStorage(numStates));
	tmIt=transitionMatrices.find(1);

	//fix absorbing states
	for(int i=0; i<numStates; ++i){
		tmIt->second->at(0, i) = 0.0;
		tmIt->second->at(twoN, i) = 0.0;
	}
	//add mutation
	/*
	tmIt->second->at(0, 1) = mutationRate;
	tmIt->second->at(twoN, twoN-1) = mutationRate;
	tmIt->second->at(0, 0) = 1.0 - mutationRate;
	tmIt->second->at(twoN, twoN) = 1.0 - mutationRate;
	*/

	double pSucess, lpSucess, lOneMinuspSucess;
	double f, oneMinusf, homoNeutral, het, homoSelected;
	double onePlusS = (1.0 + s);
	double onePlusSH = 1.0 + s*h;
	for(int i=1; i<twoN; ++i){
		//scale sampling frequencies
		f = (double) i / (double) twoN;
		oneMinusf = 1.0 - f;
		homoNeutral = oneMinusf * oneMinusf;
		homoSelected = onePlusS * f * f;
		het = onePlusSH * f * oneMinusf;
		pSucess = (homoSelected + het) / (homoNeutral + 2.0 * het + homoSelected);
		lpSucess = log(pSucess);
		lOneMinuspSucess = log(1.0 - pSucess);
		for(int j=0; j<numStates; ++j){
			//binomial sampling
			tmIt->second->atByRef(i,j) = exp(randomGenerator->binomCoeffLn(twoN, j) + j*lpSucess + (double) (twoN-j)*lOneMinuspSucess);
		}
	}
	initialized=true;
	return true;
}




