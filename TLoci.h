/*
 * TLoci.h
 *
 *  Created on: Jun 13, 2014
 *      Author: wegmannd
 */

#ifndef TLOCI_H_
#define TLOCI_H_

#include "TLocus.h"
#include <algorithm>
#include <sys/time.h>
#include "gzstream.h"
#include "stringFunctions.h"
//#include <omp.h>


class TLoci{
private:
	//TParameters* myParameters;
	TRandomGenerator* randomGenerator;
	TLog* logfile;
	//TStatesU* u;
	std::vector<TLocus> loci;
	std::vector<TLocus>::iterator lociIt;
	TLocus** pointerToLocus;
	size_t numLoci;
	std::vector<int> allTimePoints;
	WFParamPriors* priors;
	bool priorsInitialized;
	bool pointersToLociInitialized;
	TStatSettings statSettings;
	std::string lociFilename;

	TTransitionMatrix* transMatNeutral;
	bool transMatNeutralInitialized;

	void fillGrid(double* & grid, double _min, double _max, int positions);

public:
	TLoci(TParameters* MyParameters, TRandomGenerator* RandomGenerator, TLog* Logfile);
	~TLoci(){
		loci.clear();
		if(priorsInitialized) delete priors;
		if(pointersToLociInitialized) delete[] pointerToLocus;
		if(transMatNeutralInitialized){
			delete transMatNeutral;
		}
	};
	void readLociFromFileTransposed(std::string & filename, size_t maxReadLoci);
	void readLociFromFileDefault(std::string & filename, size_t maxReadLoci, int minNumTimepointsFilter, double & minFreq, double & min_fs);
	void addTimePointsFromFile(std::string & filename, int shiftTimeBy);
	void readLociKnownToBeNeutral(std::string & filename);
	void createLociFromParameters(TParameters* myParameters);
	void initializePriors(TParameters* myParameters);
	int getLocusIndexFromName(std::string & name);
	void runMCMC(TParameters* myParameters, TStatesU & u);
	void printMCMCHeader(std::ofstream & out);
	void printMCMC(std::ofstream & out, long iteration, double & curLL);
	void initializeWFParameters(TParameters* myParameters);
	void calcLikelihoodSurface_N(TParameters* myParameters, TStatesU & u);
	void calcLikelihoodSurface_s(TParameters* myParameters, TStatesU & u);
	void simulateData(TParameters* myParameters, TStatesU & u, double & gammaThreshold);
	void simulateDataWF(TParameters* myParameters);
	void print(std::string filename);
	void printWFABCFormat(std::string filename);
	void writeStatHeader(std::ofstream & file);
	void writeStatHeaderTransposed(std::ofstream & file);
	void writeStats(std::ofstream & file, int & run);
	void writeStatsTransposed(std::ofstream & file, int & run);
};



#endif /* TLOCI_H_ */
