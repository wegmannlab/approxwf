/*
 * TTransitionMatrix.h
 *
 *  Created on: May 2, 2014
 *      Author: wegmannd
 */

#ifndef TTRANSITIONMATRIX_H_
#define TTRANSITIONMATRIX_H_

#include "TRandomGenerator.h"
#include "TParameters.h"
#include "TMatrix.h"
#include <stdio.h>
#include <math.h>
#include <map>
//#include <sys/time.h>

class TStatesU{
public:
	double* u;
	bool uInitialized;
	bool uniformStates;
	int numStates;

	//TStatesU(int NumStates, WFParameters & WfParams, bool uniformStates);
	TStatesU(int NumStates, bool uniformStates);
	~TStatesU(){
		if(uInitialized) delete[] u;
	}
	void print();
	void write(std::ofstream & out);
	double operator [](int i){
		return u[i];
	}
	int getBestMatchingState(double f);
};

class TTransitionMatrix{
public:
	bool initialized;
	std::map<int, TSquareMatrixStorage*> transitionMatrices;
	std::map<int, TSquareMatrixStorage*>::iterator tmIt;
	TRandomGenerator* randomGenerator;

	TTransitionMatrix(TRandomGenerator* RandomGenerator);
	TTransitionMatrix(int & twoN, double & s, double & h,  double & mutationRate, TRandomGenerator* RandomGenerator);
	virtual ~TTransitionMatrix(){
		for(tmIt=transitionMatrices.begin(); tmIt!=transitionMatrices.end(); ++tmIt) delete tmIt->second;
	}
	virtual int size(){return 0;};
	virtual void clear(){
		for(tmIt=transitionMatrices.begin(); tmIt!=transitionMatrices.end(); ++tmIt) delete tmIt->second;
		transitionMatrices.clear();
		initialized = false;
	};

	//calculate transition matrices
	bool updateParameters(int twoN, double s, double h, double mutationRate);
	virtual bool calcTransitionMatrixOneGeneration(int & twoN, double & s, double & h, double & mutationRate){throw "calcTransitionMatrixOneGeneration not implemented for base class TTransitionMatrix";};
	void fillExponential(int & deltaT);

	//access matrices / simulate from matrices
	TSquareMatrixStorage* getPointerToTransitionMatrix(int & deltaT);
	int simulate(int & curState, int & deltaT);
	int simulateGeneration(int & curState);

	//printing
	void printTransitionMatrix(int deltaT);
	void writeTransitionMatrix(int deltaT, std::ofstream & out);
	void printRowSums(int deltaT);
	void printColSums(int deltaT);
	double getColSum(int deltaT, int col);
	void printCalculatedDeltaTs();
};

class TTransitionMatrixApprox:public TTransitionMatrix{
private:
	int integrationSteps;
	double* integrationWeights;
	bool integrationWeightsInitialized;
	double gammaThresholdForLargeGammaApproximation;
	void initializeIntegrationWeights();

protected:
	TStatesU* u;
	TBandMatrixStorage inititalTransitionMatrixQ;

	void initializeTransitionMatrixQ();
	void initializeTransitionMatrixQ(double & mutationRate);
	virtual void clear(){
		inititalTransitionMatrixQ.free();
		for(tmIt=transitionMatrices.begin(); tmIt!=transitionMatrices.end(); ++tmIt) delete tmIt->second;
		transitionMatrices.clear();
		initialized = false;
	};

	//calculate transition matrices
	void calcInitialTransitionMatrixQ(int & twoN, double & s, double & , double & mutationRate);
	virtual void fillQNeutral(int & twoN, double & mutationRate);
	virtual void fillQ(int & twoN, double & s, double & h, double & mutationRate);
	virtual bool calcTransitionMatrixOneGeneration(int & twoN, double & s, double & h, double & mutationRate);

public:
	TTransitionMatrixApprox(TStatesU* statesU, TRandomGenerator* RandomGenerator);
	TTransitionMatrixApprox(TStatesU* statesU, double gammaThreshold, TRandomGenerator* RandomGenerator);
	TTransitionMatrixApprox(int twoN, double s, double h, double mutationRate, TStatesU* u, double gammaThreshold, TRandomGenerator* RandomGenerator);
	virtual void init();
	virtual ~TTransitionMatrixApprox(){
		if(integrationWeightsInitialized) delete[] integrationWeights;
	}
	int size(){return u->numStates;};
};

class TTransitionMatrixMalaspinas:public TTransitionMatrixApprox{
protected:
	void calc_a_b(double x, const double & twoN, const double & s, const double & h, double & a, double & b);

public:
	TTransitionMatrixMalaspinas(TStatesU* u, TRandomGenerator* RandomGenerator);
	TTransitionMatrixMalaspinas(int twoN, double s, double h, double mutationRate, TStatesU* u, TRandomGenerator* RandomGenerator);
	void init(){};
	void fillQ(int & twoN, double & s, double & h, double & mutationRate);
	void fillQNeutral(int & twoN, double & mutationRate);
};


class TTransitionMatrixWF:public TTransitionMatrix{
public:
	int numStates;
	TTransitionMatrixWF(int twoN, double s, double h, double mutationRate, TRandomGenerator* RandomGenerator);
	int size(){return numStates;};
	//calculate transition matrices
	virtual bool calcTransitionMatrixOneGeneration(int & twoN, double & s, double & h, double & mutationRate);
};


#endif /* TTRANSITIONMATRIX_H_ */
