/*
  TTimePoints.cpp
 *
 *  Created on: May 28, 2014
 *      Author: wegmannd
 */

#include "TTimePoints.h"

//-----------------------------------------------------------------------------------------
//TEmissionProbs
//-----------------------------------------------------------------------------------------
TEmissionProbs::TEmissionProbs(){
	storageAllocated = false;
	emission = NULL;
	emissionLog = NULL;
	numStates = 0;
	zeroLog = -999999999.0;
	maxForScalingLog = zeroLog;
	maxForScaling = 0.0;
	scalingset = false;
}

void TEmissionProbs::allocate(int NumStates){
	if(storageAllocated){
		delete[] emission;
		delete[] emissionLog;
	}
	numStates = NumStates;
	emission = new double[numStates];
	emissionLog = new double[numStates];
}

void TEmissionProbs::determineScaling(){
	maxForScalingLog = zeroLog;
	for(int i=0; i < numStates; ++i){
		if(emissionLog[i] > maxForScalingLog) maxForScalingLog = emissionLog[i];
	}
	maxForScaling = exp(maxForScalingLog);
	scalingset = true;
}

void TEmissionProbs::setScaling(double & logScale){
	maxForScalingLog = logScale;
	maxForScaling = exp(maxForScalingLog);
	scalingset = true;
}

void TEmissionProbs::update(int & numA, int & numB, TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator){
	if(numStates != U->numStates) allocate(U->numStates);
	int numChromosomes = numA + numB;
	double p;
	double binomCoefLn = RandomGenerator->binomCoeffLn(numChromosomes, numA);


	//emission at p=0.0 and p=1.0
	if(errorRate>0.0){
		emissionLog[0] = binomCoefLn + (double) numA*log(errorRate) + (double) numB*log(1.0 - errorRate);
		emissionLog[numStates-1] = binomCoefLn + (double) numA*log(1.0 - errorRate) + (double) numB*log(errorRate);
	} else {
		emissionLog[0] = zeroLog;
		emissionLog[numStates-1] = zeroLog;
	}

	//at others
	for(int i=1; i < (numStates-1); ++i){
			p = (1.0 - errorRate) * (*U)[i] + errorRate * (1.0 - (*U)[i]);
			emissionLog[i] = binomCoefLn + (double) numA*log(p) + (double) numB*log(1.0 - p);
	}

	//scale and delog
	determineScaling();
	for(int i=0; i < numStates; ++i){
		emissionLog[i] -= maxForScalingLog;
		emission[i] = exp(emissionLog[i]);
	}
}
//-----------------------------------------------------------------------------------------
TEmissionProbsBetaBinom::TEmissionProbsBetaBinom():TEmissionProbs(){}

void TEmissionProbsBetaBinom::update(int & numA, int & numB, TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator){
	if(numStates != U->numStates) allocate(U->numStates);
	int numChromosomes = numA + numB;

	double binomCoefLn = RandomGenerator->binomCoeffLn(numChromosomes, numA);

	//emission at p=0.0 and p=1.0
	if(errorRate>0.0){
		emissionLog[0] = binomCoefLn + (double) numA*log(errorRate) + (double) numB*log(1.0 - errorRate);
		emissionLog[numStates-1] = binomCoefLn + (double) numA*log(1.0 - errorRate) + (double) numB*log(errorRate);
	} else {
		if(numA == 0){
			emissionLog[0] = 0.0;
			emissionLog[numStates-1] = zeroLog;
		} else if(numB == 0){
			emissionLog[0] = zeroLog;
			emissionLog[numStates-1] = 0.0;
		} else {
			emissionLog[0] = zeroLog;
			emissionLog[numStates-1] = zeroLog;
		}
	}

	//at others -> beta-binomial integration
	double alpha, beta, mean, var, tmp1, tmp2;
	double numA_double = numA;
	double numChromosomes_double = numChromosomes;
	double binomialSmear = 1.0 / 4.0;

	for(int i=1; i < (numStates-1); ++i){
		//var is 1/x interval u[i-1], u[i+1]
		var = ((*U)[i+1] - (*U)[i-1]) * binomialSmear;
		var = var * var;
		//mean is (*U)[i] if there is no error. Else it is (*U)[i] with error
		if(errorRate==0.0) mean = (*U)[i];
		else mean = (*U)[i] * (1.0 - errorRate) + (1.0 - (*U)[i]) * errorRate;

		//calc alpha and beta of prior by setting mean = mean and var = var as calculated above
		tmp1 = 1.0 - mean;
		tmp2 = mean * tmp1 / var - 1.0;
		alpha = mean * tmp2;
		beta = tmp1 * tmp2;

		//calc beta-binomial = integral_0^1 Binom * Beta
		emissionLog[i] = binomCoefLn
                       + RandomGenerator->betaln(numA_double + alpha, numChromosomes_double - numA_double + beta)
                       - RandomGenerator->betaln(alpha, beta);
	}

	//scale and delog
	determineScaling();
	for(int i=0; i < numStates; ++i){
		emissionLog[i] -= maxForScalingLog;
		emission[i] = exp(emissionLog[i]);
	}
}


//-----------------------------------------------------------------------------------------
//TTimePointWithSamples
//-----------------------------------------------------------------------------------------

TTimePointWithSamples::TTimePointWithSamples(int NumChromosomes, int NumA){
	numChromosomes=NumChromosomes;
	numA=NumA;
	numB=numChromosomes-numA;
	hasSamples = true;
	numStates = 0;
	binomFilled = false;
	EmissionProbsCur = new TEmissionProbsBetaBinom();
	EmissionProbsOld = new TEmissionProbsBetaBinom();
	isFirstInSeries = false;
}

void TTimePointWithSamples::updateBinomialCoefficients(TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator){
	//save old
	TEmissionProbsBetaBinom* tmp = EmissionProbsOld;
	EmissionProbsOld = EmissionProbsCur;
	EmissionProbsCur = tmp;
	/*
	if(EmissionProbsOld->scalingset){
		EmissionProbsCur->setScaling(EmissionProbsOld->maxForScalingLog);
	}
	*/
	//update
	EmissionProbsCur->update(numA, numB, U, errorRate, RandomGenerator);
}

void TTimePointWithSamples::resetBinomialCoefficients(){
	TEmissionProbsBetaBinom* tmp = EmissionProbsCur;
	EmissionProbsCur = EmissionProbsOld;
	EmissionProbsOld = tmp;
}

void TTimePointWithSamples::simulateData(double & frequency, double & errorRate, TRandomGenerator* RandomGenerator){
	numA = RandomGenerator->getBiomialRand((1.0 - errorRate) * frequency + errorRate * (1.0 -frequency), numChromosomes);
	numB = numChromosomes - numA;
}

double TTimePointWithSamples::getAlleleFrequency(){
	return (double) numA / (double) numChromosomes;
}

void TTimePointWithSamples::setNumA(int & NumA){
	numA = NumA;
	numB = numChromosomes-numA;
}

//------------------------------------------------------------------------------------------------
//TTimePointVector
//------------------------------------------------------------------------------------------------
TTimePointVector::TTimePointVector(){
	init();
}

TTimePointVector::TTimePointVector(TTimePointVector&& other){
	timePoints = std::move(other.timePoints);
	numTimePoints = other.numTimePoints;
	numTimepointsWithSamples = other.numTimepointsWithSamples;
	interval = other.interval;
	pointersInitialized = other.pointersInitialized;
	pointerToTimepoints = other.pointerToTimepoints;
	pointerToTimepointsWithSamples = other.pointerToTimepointsWithSamples;
	hasSomeSamples = other.hasSomeSamples;
	firstTimepointWithSamples = other.firstTimepointWithSamples;
	hasMutationTime = other.hasMutationTime;
	emissionprobabilitiesInitialized = other.emissionprobabilitiesInitialized;

	//statistics
	fsi = other.fsi;
	fsd = other.fsd;
	fsi_l = other.fsi_l;
	fsd_l = other.fsd_l;
	t_l = other.t_l;
	t_h = other.t_h;
	nextTimepointIsFirstInSeries = other.nextTimepointIsFirstInSeries;

	other.init();
}

TTimePointVector& TTimePointVector::operator=(TTimePointVector&& other){
	if(this != &other){
		timePoints = std::move(other.timePoints);
		numTimePoints = other.numTimePoints;
		numTimepointsWithSamples = other.numTimepointsWithSamples;
		interval = other.interval;
		pointersInitialized = other.pointersInitialized;
		pointerToTimepoints = other.pointerToTimepoints;
		pointerToTimepointsWithSamples = other.pointerToTimepointsWithSamples;
		hasSomeSamples = other.hasSomeSamples;
		firstTimepointWithSamples = other.firstTimepointWithSamples;
		hasMutationTime = other.hasMutationTime;
		emissionprobabilitiesInitialized = other.emissionprobabilitiesInitialized;

		//statistics
		fsi = other.fsi;
		fsd = other.fsd;
		fsi_l = other.fsi_l;
		fsd_l = other.fsd_l;
		t_l = other.t_l;
		t_h = other.t_h;
		nextTimepointIsFirstInSeries = other.nextTimepointIsFirstInSeries;

		other.init();
	}
	return *this;
};

void TTimePointVector::init(){
	numTimePoints = 0;
	numTimepointsWithSamples = 0;
	interval = -1;
	pointersInitialized = false;
	pointerToTimepoints = nullptr;
	pointerToTimepointsWithSamples = nullptr;
	hasSomeSamples = false;
	firstTimepointWithSamples = 0;
	hasMutationTime = false;
	emissionprobabilitiesInitialized = false;

	//statistics
	fsi = 0.0;
	fsd = 0.0;
	fsi_l = 0.0;
	fsd_l = 0.0;
	t_l = 0.0;
	t_h = 0.0;
	nextTimepointIsFirstInSeries = true;
};

void TTimePointVector::setNextIsFirstInSeries(){
	nextTimepointIsFirstInSeries = true;
};

void TTimePointVector::setAsFirstInSeries(int TimePoint){
	if(timePoints.find(TimePoint) != timePoints.end())
		timePoints[TimePoint]->setIsFirstInSeries();
};

void TTimePointVector::setInterval(int MutTime, int Interval){
	//check if mutation time is not younger than first sample with alleles
	for(it=timePoints.begin(); it!=timePoints.end(); ++it){
		if(it->first < MutTime && it->second->hasSelectedSaples())
			throw "Mutation cannot appear after samples with selected alleles have been observed!";
	}

	//now fill in time points without samples to sample trajectories
	interval=Interval;
	int last=timePoints.rbegin()->first;
	for(int i=MutTime; i<last; i=i+interval){
		if(timePoints.find(i)==timePoints.end())
			timePoints.insert(std::pair<int, TTimePoint*>(i, new TTimePoint()));
	}
}

void TTimePointVector::update(int MutTime){
	//check if mutation time is before first time point with samples
	if(MutTime > firstTimepointWithSamples) throw "Mutation cannot appear after samples with selected alleles have been observed!";
	//remove interval time points
	if(hasMutationTime){
		//delete all time points before MutTime
		//But check if mutation time is not younger than first sample with alleles
		it=timePoints.begin();
		while(it->first < MutTime){
			delete it->second;
			int toErase=it->first;
			++it;
			timePoints.erase(toErase);
		}
	}
	//add new time points if necessary
	if(interval>0){
		int first=timePoints.begin()->first;
		for(int i=MutTime; i<first; i=i+interval){
			if(timePoints.find(i)==timePoints.end())
				timePoints.insert(std::pair<int, TTimePoint*>(i, new TTimePoint()));
		}
	}
	initDeltaT();
	hasMutationTime = true;
}

void TTimePointVector::addTimepoint(int & TimePoint, std::string & Samples, bool isFirstInSeries){
	if(timePoints.find(TimePoint)!=timePoints.end()) throw "Multiple entries for time point " + toString(TimePoint) + "!";
	//parse samples
	int size, numA;
	unsigned int pos = Samples.find_first_of('/');
	if(pos != std::string::npos){
		std::string tmp = Samples.substr(0,pos);
		numA = stringToInt(tmp);
		Samples.erase(0,pos+1);
		size = stringToInt(Samples);
		if(numA > size) throw "More selected alleles than chromosomes in '" + toString(numA) + "/" + toString(size) + "'!";
		if(size>0){
			timePoints.insert(std::pair<int, TTimePoint*>(TimePoint, new TTimePointWithSamples(size, numA)));
			++numTimePoints;
			if(nextTimepointIsFirstInSeries){
				timePoints[TimePoint]->setIsFirstInSeries();
				nextTimepointIsFirstInSeries = false;
			}
		}
	}
}

void TTimePointVector::addTimepoint(int & TimePoint, int & numA, int & size, bool isFirstInSeries){
	if(timePoints.find(TimePoint)!=timePoints.end()) throw "Multiple entries for time point " + toString(TimePoint) + "!";
	if(numA > size) throw "More selected alleles than chromosomes in '" + toString(numA) + "/" + toString(size) + "'!";
	if(size>0){
		timePoints.insert(std::pair<int, TTimePoint*>(TimePoint, new TTimePointWithSamples(size, numA)));
		++numTimePoints;
		if(nextTimepointIsFirstInSeries){
			timePoints[TimePoint]->setIsFirstInSeries();
			nextTimepointIsFirstInSeries = false;
		}
	}
}

int TTimePointVector::getMinNumTimepointsAllelesAreObserved(double & minFreq){
	//count for each allele the number of timepoints their counts are > 0
	//return minimum of the two alleles
	int numTimepointsWithRef = 0;
	int numTimepointsWithAlt = 0;

	for(it=timePoints.begin(); it!=timePoints.end(); ++it){
		double freq = (double) it->second->getNumA() / (double) it->second->getNumChromosomes();
		if(freq > minFreq) ++numTimepointsWithRef;
		if((1.0 - freq) > minFreq) ++numTimepointsWithAlt;
	}
	return std::min(numTimepointsWithRef, numTimepointsWithAlt);
}

void TTimePointVector::initDeltaT(){
	if(pointersInitialized){
		delete[] pointerToTimepoints;
		delete[] pointerToTimepointsWithSamples;
	}
	//fill vector of pointers in chronological order and calculate deltaT
	numTimePoints = timePoints.size();
	pointerToTimepoints = new TTimePoint*[numTimePoints];
	pointersInitialized=true;
	int i=0;
	int oldTime = timePoints.begin()->first-1;
	numTimepointsWithSamples = 0;
	for(it=timePoints.begin(); it!=timePoints.end(); ++it, ++i){
		pointerToTimepoints[i] = it->second;
		it->second->deltaT = 0;
		it->second->deltaT = it->first - oldTime;
		oldTime = it->first;
		if(it->second->hasSamples) ++numTimepointsWithSamples;
	}

	//fill pointer to those with samples
	pointerToTimepointsWithSamples= new TTimePoint*[numTimepointsWithSamples];
	i = 0;
	for(it=timePoints.begin(); it!=timePoints.end(); ++it){
		if(it->second->hasSamples){
			pointerToTimepointsWithSamples[i] = it->second;
			++i;
		}
	}

	//find first time point with samples
	for(it=timePoints.begin(); it!=timePoints.end(); ++it){
		if(it->second->hasSamples){
			firstTimepointWithSamples = it->first;
			hasSomeSamples = true;
			break;
		}
	}
}

void TTimePointVector::prepareEmissionProbabilities(TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator){
	for(int i = 0; i < numTimepointsWithSamples; ++i)
		pointerToTimepointsWithSamples[i]->updateBinomialCoefficients(U, errorRate, RandomGenerator);

	emissionprobabilitiesInitialized = true;
}

void TTimePointVector::updateEmissionProbabilities(TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator){
	for(int i = 0; i < numTimepointsWithSamples; ++i)
		pointerToTimepointsWithSamples[i]->updateBinomialCoefficients(U, errorRate, RandomGenerator);
}

void TTimePointVector::resetEmissionProbabilities(){
	for(int i = 0; i < numTimepointsWithSamples; ++i)
		pointerToTimepointsWithSamples[i]->resetBinomialCoefficients();
}

double& TTimePointVector::getEmissionProb(int & timePoint, int & k){
	if(!emissionprobabilitiesInitialized) throw "Emission probabilities have not yet been initialized!";
	return pointerToTimepoints[timePoint]->getEmission(k);
}

double& TTimePointVector::getEmissionProbLog(int & timePoint, int & k){
	if(!emissionprobabilitiesInitialized) throw "Emission probabilities have not yet been initialized!";
	return pointerToTimepoints[timePoint]->getEmissionLog(k);
}

void TTimePointVector::currentSimulateData(double frequency, double & errorRate, TRandomGenerator* RandomGenerator){
	it->second->simulateData(frequency, errorRate, RandomGenerator);
}

void TTimePointVector::currentSetData(int numA){
	it->second->setNumA(numA);
}

std::string TTimePointVector::getSampleStringAt(const int & TimePoint){
	it = timePoints.find(TimePoint);
	if(it == timePoints.end()) return "NA";
	else return it->second->getString();
}

int TTimePointVector::getSampleSizeAt(const int & TimePoint){
	it = timePoints.find(TimePoint);
	if(it == timePoints.end()) return 0;
	else return it->second->numChromosomes;
}

int TTimePointVector::getNumAAt(const int & TimePoint){
	it = timePoints.find(TimePoint);
	if(it == timePoints.end()) return 0;
	else return it->second->getNumA();
}

void TTimePointVector::calcStats(TStatSettings & settings){
	if(hasSomeSamples){
		double x_fs, y_fs, z_fs, fsp, nt_fs;
		unsigned int ngen_fs,ngen_fs_l;
		ngen_fs = 0.0;
		ngen_fs_l = 0.0;
		fsi = 0.0;
		fsd = 0.0;
		fsi_l = 0.0;
		fsd_l = 0.0;
		t_l = 0.0;
		t_h = 0.0;

		//find last not fixed / not extinct
		int firstNotfixed = -1;
		int lastNotFixed = 0;
		for(int i = 0; i < numTimepointsWithSamples; ++i){
			x_fs = pointerToTimepointsWithSamples[i]->getAlleleFrequency();
			if(x_fs>0.0 && x_fs<1.0) lastNotFixed = i;
			if(firstNotfixed < 0 && x_fs>0.0 && x_fs<1.0) firstNotfixed = i;
		}

		//go through all pairs of time points
		for(int i = 1; i < numTimepointsWithSamples; ++i){
			//do not add to stats if we jump to a new replicate
			if(pointerToTimepointsWithSamples[i]->isFirstInSeries || pointerToTimepointsWithSamples[i]->numChromosomes < 2) continue;

			//else calculate
			x_fs = pointerToTimepointsWithSamples[i-1]->getAlleleFrequency();
			y_fs = pointerToTimepointsWithSamples[i]->getAlleleFrequency();

			if (!(((x_fs < settings.min_freq_fs) & (y_fs < settings.min_freq_fs)) || ((x_fs > settings.max_freq_fs) & (y_fs > settings.max_freq_fs)))) {
				z_fs = (x_fs + y_fs) / 2.0;
				fsp = (x_fs - y_fs) * (x_fs - y_fs) / (z_fs * (1.0 - z_fs));
				nt_fs = 2.0 / (1.0 / (double) (pointerToTimepointsWithSamples[i-1]->numChromosomes) + 1.0 / (double) (pointerToTimepointsWithSamples[i]->numChromosomes));

				//std::cout << "x = " << x_fs << "\ty = " << y_fs << "\tn = " << nt_fs << "\tny = " << (pointerToTimepointsWithSamples[i]->numChromosomes) << "\tFS = " << fsp << "\tFS' = ";

				fsp = ((fsp*(1.0 - 1.0 / (2.0 * nt_fs)) - 2.0 / nt_fs) / ((1.0 + fsp / 4.0)*(1.0 - 1.0 / ((double) (pointerToTimepointsWithSamples[i]->numChromosomes)))));

				//std::cout << fsp << std::endl;

				// calculate the total number of generation where we could calculate Fs'
				if ((x_fs < settings.fs_threshold_l && y_fs < settings.fs_threshold_l) || (x_fs > settings.fs_threshold_h && y_fs > settings.fs_threshold_h)){
					ngen_fs_l += pointerToTimepointsWithSamples[i]->deltaT;
					if (x_fs < y_fs)
						fsi_l += fsp;
					else //if (x_fs > y_fs)
						fsd_l += fsp;
				} else {
					ngen_fs += pointerToTimepointsWithSamples[i]->deltaT;
					if (x_fs < y_fs)
						fsi += fsp;
					else //if (x_fs > y_fs)
						fsd += fsp;
				}
			}
			//calculate tl and th
			if(i >= firstNotfixed && i <= lastNotFixed){
				if(y_fs < settings.freq_threshold_l){
					if(x_fs < settings.freq_threshold_l) t_l += pointerToTimepointsWithSamples[i]->deltaT;
					else t_l += pointerToTimepointsWithSamples[i]->deltaT * ((settings.freq_threshold_l - y_fs)/(x_fs - y_fs)); //interpolate
				} else if(y_fs > settings.freq_threshold_h){
					if(x_fs > settings.freq_threshold_h) t_h += pointerToTimepointsWithSamples[i]->deltaT;
					else t_h += pointerToTimepointsWithSamples[i]->deltaT * ((y_fs - settings.freq_threshold_h)/(y_fs - x_fs)); //interpolate
				} else if(x_fs < settings.freq_threshold_l) t_l += pointerToTimepointsWithSamples[i]->deltaT * ((settings.freq_threshold_l - x_fs)/(y_fs - x_fs)); //interpolate
				else if(x_fs > settings.freq_threshold_h) t_h += pointerToTimepointsWithSamples[i]->deltaT * ((x_fs - settings.freq_threshold_h)/(x_fs - y_fs)); //interpolate
			}
		}
		if (ngen_fs > 0) {
			fsi = fsi / ngen_fs;
			fsd = fsd / ngen_fs;
		} else {
			fsi = 0.0;
			fsd = 0.0;
		}
		if (ngen_fs_l > 0) {
			fsi_l = fsi_l / ngen_fs_l;
			fsd_l = fsd_l / ngen_fs_l;
		} else {
			fsi_l = 0.0;
			fsd_l = 0.0;
		}
	}
}

void TTimePointVector::writeStats(std::ofstream & file){
	file << "\t" << fsi << "\t" << fsd << "\t" << fsi_l << "\t" << fsd_l << "\t" << t_l << "\t" << t_h;
}
void TTimePointVector::writeStatHeader(std::ofstream & file, std::string & tag){
	file << "\t" << tag << "_fsi";
	file << "\t" << tag << "_fsd";
	file << "\t" << tag << "_fsi_l";
	file << "\t" << tag << "_fsd_l";
	file << "\t" << tag << "_t_l";
	file << "\t" << tag << "_t_h";
}
