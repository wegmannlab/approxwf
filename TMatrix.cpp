/*
 * TMatrix.cpp
 *
 *  Created on: Apr 19, 2014
 *      Author: wegmannd
 */

#include "TMatrix.h"

//---------------------------------------------------------------------------------------------------
//TBaseMatrixStorage
//---------------------------------------------------------------------------------------------------
//Constructors
TBaseMatrixStorage::TBaseMatrixStorage(){
	size=0;
	initialized=false;
	data=NULL;
}
//---------------------------------------------------------------------------------------------------
//access data
double& TBaseMatrixStorage::at(int row, int col){
	return atByRef(row, col);
}

double TBaseMatrixStorage::max(){
	//return largest value
	int i=0;
	double max = atByRef(i, i);
	for(; i<size; ++i){
		for(int j=0; j<size; ++j){
			if(max < atByRef(i, j))
				max = atByRef(i, j);
		}
	}
	return max;
};

double TBaseMatrixStorage::min(){
	//return largest value
	int i=0;
	double min = atByRef(i, i);
	for(; i<size; ++i){
		for(int j=0; j<size; ++j){
			if(min > atByRef(i, j))
				min = atByRef(i, j);
		}
	}
	return min;
};

void TBaseMatrixStorage::print(){
	for(int i=0; i<size; ++i){
		printf("%1.4e", at(i,0));
		for(int j=1; j<size; ++j){
			std::cout << " ";
			printf("%1.4e", atByRef(i,j));
		}
		std::cout << std::endl;
	}
}

void TBaseMatrixStorage::write(std::ofstream & out){
	for(int i=0; i<size; ++i){
		out << at(i,0);
		for(int j=1; j<size; ++j){
			out << " " << atByRef(i,j);
		}
		out << std::endl;
	}
}

void TBaseMatrixStorage::printDiag(){
	std::cout << "Diag:";
	for(int i=0; i<size; ++i){
		std::cout << " ";
		printf("%1.8e", atByRef(i,i));
	}
	std::cout << std::endl;
}

void TBaseMatrixStorage::printRow(int row){
	std::cout << "Row " << row << ":";
	for(int i=0; i<size; ++i){
		std::cout << " ";
		printf("%1.8e", atByRef(row,i));
	}
	std::cout << std::endl;
}

void TBaseMatrixStorage::printCol(int col){
	std::cout << "Col " << col << ":";

	double sum = 0.0;
	for(int i=0; i<size; ++i){
		std::cout << " ";
		printf("%1.8e", atByRef(i, col));
		sum += atByRef(i, col);
	}
	std::cout << std::endl;
	std::cout << "SUM = " << sum << std::endl;
};

void TBaseMatrixStorage::printDiff(TBaseMatrixStorage & other){
	if(size!=other.size) throw "TBaseMatrixStorage::printDiff: only possible for matrices of same size!";
	for(int i=0; i<size; ++i){
		for(int j=0; j<size; ++j){
			if(j>0) std::cout << " ";
			if(at(i,j)!=other.at(i,j)) printf("%1.3e", other.at(i,j)-at(i,j));
			else printf("%10c", '.');
		}
		std::cout << std::endl;
	}
}
int TBaseMatrixStorage::numDiffEntries(TBaseMatrixStorage & other){
	if(size!=other.size) throw "TBaseMatrixStorage::printDiff: only possible for matrices of same size!";
	int res=0;
	for(int i=0; i<size; ++i){
		for(int j=0; j<size; ++j){
			if(at(i,j)!=other.at(i,j)) ++res;
		}
	}
	return res;
}
void TBaseMatrixStorage::printRowSums(){
	for(int i=0; i<size; ++i){
		double sum=0;
		for(int j=0; j<size; ++j){
			sum += atByRef(i,j);
		}
		std::cout << sum << std::endl;
	}
}
//---------------------------------------------------------------------------------------------------
//Modify values
void TBaseMatrixStorage::set(double val){
	setByRef(val);
}
void TBaseMatrixStorage::addToDiagonal(double add){
	addToDiagonalByRef(add);
}


//---------------------------------------------------------------------------------------------------
//TSquareMatrix
//---------------------------------------------------------------------------------------------------
//Constructors
TSquareMatrixStorage::TSquareMatrixStorage(){
	size=0;
	initialized=false;
	data=NULL;
}
TSquareMatrixStorage::TSquareMatrixStorage(int Size){
	initialized=false;
	size=Size;
	initialize();
}
TSquareMatrixStorage::TSquareMatrixStorage(int Size, double Val){
	initialized=false;
	size=Size;
	initialize();
	setByRef(Val);
}
TSquareMatrixStorage::TSquareMatrixStorage(TSquareMatrixStorage & other){
	initialized=false;
	resize(other.size);
	fillFromMatrix(other);
}
TSquareMatrixStorage::TSquareMatrixStorage(TSquareMatrixStorage & other, double Scale){
	initialized=false;
	size=other.size;
	initialize();
	fillFromMatrix(other, Scale);
}
TSquareMatrixStorage::TSquareMatrixStorage(TBandMatrixStorage & other){
	initialized=false;
	size=other.size;
	initialize();
	//copy data
	set(0.0);
	//go through diagonals
	int row, col;
	for(int d=0; d<other.numDiag; ++d){
		if(d>other.bandwidth){
			row=0;
			col = d - other.bandwidth;
		} else {
			row = other.bandwidth - d;
			col = 0;
		}
		for(int i=0; i<other.diagLength[d]; ++i, ++row, ++col){
			data[row][col]=other.atDiagByRef(d, i);
		}
	}
}
//---------------------------------------------------------------------------------------------------
//Initialize / free
void TSquareMatrixStorage::initialize(){
	if(initialized) free();
	if(size>0){
		data=new double*[size];
		for(int i=0; i<size; ++i){
			data[i]=new double[size];
		}
		initialized=true;
	}
}
void TSquareMatrixStorage::resize(int Size){
	if(!initialized || Size!=size){
		size=Size;
		initialize();
	}
}
void TSquareMatrixStorage::resize(int Size, double Val){
	resize(Size);
	setByRef(Val);
}
void TSquareMatrixStorage::free(){
	if(initialized){
		for(int i=0; i<size; ++i) delete[] data[i];
		delete[] data;
		initialized=false;
	}
}

//---------------------------------------------------------------------------------------------------
//access data
double& TSquareMatrixStorage::operator()(int row, int col){
	//this operator does not check if index exists!
	return data[row][col];
}
double& TSquareMatrixStorage::atByRef(int & row, int & col){
	//Checks if index exists and returns zero if not on diagonal
	if (row >= size || col >= size){
		throw "TMatrix subscripts out of bounds";
	}
	return data[row][col];
}
double* TSquareMatrixStorage::operator[] (int row){
	//this operator does not check if index exists!
	return data[row];
}
//---------------------------------------------------------------------------------------------------
//Modify values
void TSquareMatrixStorage::setByRef(double & val){
	for(int row=0; row<size; ++row){
		for(int col=0; col<size; ++col){
			data[row][col]=val;
		}
	}
}
void TSquareMatrixStorage::addToDiagonalByRef(double & add){
	for(int i=0; i<size; ++i){
		data[i][i]+=add;
	}
}
//---------------------------------------------------------------------------------------------------
//fill values
void TSquareMatrixStorage::fillUnifRandom(TRandomGenerator* Randomgenerator){
	for(int row=0; row<size; ++row){
		for(int col=0; col<size; ++col){
			data[row][col]=Randomgenerator->getRand();
		}
	}
}
void TSquareMatrixStorage::fillFromMatrix(TSquareMatrixStorage & other){
	resize(other.size);
	for(int row=0; row<size; ++row){
		for(int col=0; col<size; ++col){
			data[row][col]=other.data[row][col];
		}
	}
}
void TSquareMatrixStorage::fillFromMatrix(TSquareMatrixStorage & other, double & Scale){
	resize(other.size);
	for(int row=0; row<size; ++row){
		for(int col=0; col<size; ++col){
			data[row][col]=other.data[row][col] * Scale;
		}
	}
}
void TSquareMatrixStorage::fillFromMatrix(TBandMatrixStorage & other){
	resize(other.size);
	set(0.0);
	//go through diagonals
	int row, col;
	for(int d=0; d<other.numDiag; ++d){
		if(d>other.bandwidth){
			row=0;
			col = d - other.bandwidth;
		} else {
			row = other.bandwidth - d;
			col = 0;
		}
		for(int i=0; i<other.diagLength[d]; ++i, ++row, ++col){
			data[row][col]=other.atDiagByRef(d, i);
		}
	}
}
void TSquareMatrixStorage::fillFromMatrix(TBandMatrixStorage & other, double & Scale){
	resize(other.size);
	set(0.0);
	//go through diagonals
	int row, col;
	for(int d=0; d<other.numDiag; ++d){
		if(d>other.bandwidth){
			row=0;
			col = d - other.bandwidth;
		} else {
			row = other.bandwidth - d;
			col = 0;
		}
		for(int i=0; i<other.diagLength[d]; ++i, ++row, ++col){
			data[row][col]=other.atDiagByRef(d, i) * Scale;
		}
	}
}
void TSquareMatrixStorage::fillFromProduct(TSquareMatrixStorage & first, TSquareMatrixStorage & second){
	if(first.size!=second.size) throw "TSquareMatrix::fillFromProduct: only implemented for matrices of same size! (provided: " + toString(first.size) + "x" + toString(first.size) + " and " + toString(second.size) + "x" + toString(second.size) + ")";
	resize(first.size);
	//now calculate product
	for(int row=0; row<size; ++row){
		for(int col=0; col<size; ++col){
			//we need to go through the row / col of first / second, respectively
			data[row][col]=0.0;
			for(int i=0; i<size; ++i){
				data[row][col] += first.data[row][i] * second.data[i][col];
			}
		}
	}
}
void TSquareMatrixStorage::fillFromProduct(TBandMatrixStorage & first, TBandMatrixStorage & second){
	TBandMatrixStorage tmp;
	tmp.fillFromProduct(first, second);
	fillFromMatrix(tmp);
}

bool TSquareMatrixStorage::fillAsExponential(TBandMatrixStorage & Q){
	// Choose k such that 3 * 2^k > numStates AND 1/2^k < 0.1 (i.e. k>3)
	int k = Q.size / 3.0;
	k = log(k)/log(2) + 20;
	if(k<4) k=4;

	//now run poor man's algo
	fillAsExponential(Q, k);

	//check if k was sufficient
	while(max() > 1.01 || min() < -0.01){
		if(k > 100) return false;
		k += 10;
		fillAsExponential(Q, k);
	}
	return true;
}

void TSquareMatrixStorage::fillAsExponential(TBandMatrixStorage & Q, int k){
	//We get exp(Q) using a poor man's algorithm:
	// 1) Devide all entries by 1/(2^k)
	// 2) Square the matrix Q k times

	// 1) Divide all entries by 1 / (2^k)
	double scale = 1.0 / (double) pow(2, k);
	TBandMatrixStorage** QScaledBand = new TBandMatrixStorage*[2];
	QScaledBand[0] = new TBandMatrixStorage(Q, scale);
	QScaledBand[0]->addToDiagonal(1.0);
	QScaledBand[1] = new TBandMatrixStorage();
	int QScaledBandIndex = 0;

	// 2) Square matrix k times.
	//Change from bandMarix to Matrix once the diagonals are filled
	for(int i=0; i<k; ++i){
		QScaledBandIndex = 1 - QScaledBandIndex;
		QScaledBand[QScaledBandIndex]->fillFromSquare(*QScaledBand[1 - QScaledBandIndex]);
	}
	fillFromMatrix(*QScaledBand[QScaledBandIndex]);

	//clean up memory
	delete QScaledBand[0];
	delete QScaledBand[1];
	delete[] QScaledBand;
}


//---------------------------------------------------------------------------------------------------
//TBandMatrixStorage
//---------------------------------------------------------------------------------------------------
//Constructors
TBandMatrixStorage::TBandMatrixStorage(){
	size = 0;
	bandwidth = -1;
	numDiag = 0;
	zero = 0.0;
	data = NULL;
	diagLength = NULL;
	initialized = false;
}
TBandMatrixStorage::TBandMatrixStorage(int Size, int Bandwidth){
	initialized = false;
	if(Size < Bandwidth) throw "TBandMatrix: bandwidth (" + toString(Bandwidth) + ") larger than size (" + toString(Size) + ")!";
	size = Size;
	bandwidth = Bandwidth;
	numDiag = 0;
	initialize();
	zero = 0.0;
}
TBandMatrixStorage::TBandMatrixStorage(int Size, int Bandwidth, double val){
	initialized = false;
	size = Size;
	bandwidth = Bandwidth;
	numDiag = 0;
	initialize();
	zero = 0.0;
	setByRef(val);
}
TBandMatrixStorage::TBandMatrixStorage(TBandMatrixStorage & other){
	initialized = false;
	size = 0;
	bandwidth = -1;
	numDiag = 0;
	zero = 0.0;
	fillFromMatrix(other);
}
TBandMatrixStorage::TBandMatrixStorage(TBandMatrixStorage & other, double Scale){
	initialized = false;
	size=0;
	zero=0.0;
	bandwidth = -1;
	numDiag = 0;
	fillFromMatrix(other, Scale);
}
//---------------------------------------------------------------------------------------------------
//Initialize / free
void TBandMatrixStorage::initialize(){
	if(initialized) free();
	if(size>0){
		numDiag = 2*bandwidth+1;
		data = new double*[numDiag];
		diagLength = new int[numDiag];
		for(int i=0; i<numDiag; ++i){
			diagLength[i] = size-abs(i-bandwidth);
			data[i] = new double[diagLength[i]];
		}
		initialized = true;
	}
}
void TBandMatrixStorage::resize(int Size, int Bandwidth){
	if(!initialized || Size != size || Bandwidth != bandwidth){
		size = Size;
		bandwidth = Bandwidth;
		initialize();
	}
}
void TBandMatrixStorage::resize(int Size, int Bandwidth, double Val){
	if(!initialized || Size != size || Bandwidth != bandwidth){
		size = Size;
		bandwidth = Bandwidth;
		initialize();
	}
	setByRef(Val);
}
void TBandMatrixStorage::free(){
	if(initialized){
		for(int i=0; i<numDiag; ++i) delete[] data[i];
		delete[] data;
		delete[] diagLength;
		initialized = false;
	}
}
//---------------------------------------------------------------------------------------------------
//access data
double& TBandMatrixStorage::operator()(int row, int col){
	//this operator does not check if index exists!
	if(row<col) return data[col-row+bandwidth][row];
	else return data[col-row+bandwidth][col];
}
double& TBandMatrixStorage::atByRef(int & row, int & col){
	//Checks if index exists and returns zero if not on diagonal
	if (row >= size || col >= size){
		throw "TBandMatrixStorage subscripts out of bounds";
	}
	if(row < (col-bandwidth) || row > (col+bandwidth)){
		return zero;
	}
	if(row<col) return data[col - row + bandwidth][row];
	else return data[col - row + bandwidth][col];
}

double& TBandMatrixStorage::atNoCheckByRef(int & row, int & col){
	if(row < (col-bandwidth) || row > (col+bandwidth)){
		return zero;
	}
	if(row<col) return data[col-row+bandwidth][row];
	else return data[col-row+bandwidth][col];
}

double& TBandMatrixStorage::atDiag(int diag, int index){
	//this function does not check if index exists!
	return data[diag][index];
}
double& TBandMatrixStorage::atDiagByRef(int & diag, int & index){
	//this function does not check if index exists!
	return data[diag][index];
}
//---------------------------------------------------------------------------------------------------
//Modify values
void TBandMatrixStorage::setByRef(double & val){
	for(int d=0; d<numDiag; ++d){
		for(int i=0; i<diagLength[d]; ++i){
			data[d][i]=val;
		}
	}
}
void TBandMatrixStorage::addToDiagonalByRef(double & add){
	for(int i=0; i<size; ++i){
		data[bandwidth][i]+=add;
	}
}
//---------------------------------------------------------------------------------------------------
//fill values
void TBandMatrixStorage::fillUnifRandom(TRandomGenerator* Randomgenerator){
	for(int d=0; d<numDiag; ++d){
		for(int i=0; i<diagLength[d]; ++i) data[d][i]=Randomgenerator->getRand();
	}
}
void TBandMatrixStorage::fillFromMatrix(TBandMatrixStorage & other){
	//check dimensions
	resize(other.size, other.bandwidth);
	//copy data
	for(int d=0; d<numDiag; ++d){
		for(int i=0; i<diagLength[i]; ++i) data[d][i]=other.data[d][i];
	}
}
void TBandMatrixStorage::fillFromMatrix(TBandMatrixStorage & other, double & Scale){
	//check dimensions
	resize(other.size, other.bandwidth);
	//copy data and scale
	for(int d=0; d<numDiag; ++d){
		for(int i=0; i<diagLength[d]; ++i) data[d][i]=other.data[d][i] * Scale;
	}
}
void TBandMatrixStorage::fillFromProduct(TBandMatrixStorage & first, TBandMatrixStorage & second){
	//set dimensions and fill with zeros
	if(first.size!=second.size) throw "TBandMatrix::fillfromProduct provided matrices are of different size!";
	resize(first.size, std::min(first.size, first.bandwidth + second.bandwidth));
	set(zero);

	//Now use an approach to multiply band matrices based on diagonals.
	//Specifically, we profit from the fact that A=a1 + ... + an) and B=b1 + ... + bn), where
	//ai and bi are matrices with all zeros except the ith diagonal, which corresponds to the
	//ith diagonal of matrices A and B, respectively. In this case, A*B = (a1 + ... + an) * (b1 + ... + bn)
	//So we have to multiply each pair of diagonals, which will only add to values on a diagonal again.
	int fillDiag, r, s;
	int rAndS; //r + s
	int i1, i2, ir; //indexes for first, second and resulting diagonal
	int len; //length of filling diagonal: last index to fill + 1
	for(int d1=0; d1<first.numDiag; ++d1){
		for(int d2=0; d2<second.numDiag; ++d2){
			//set r, s: index of diagonal, counting from longest diagonal. Lower is negative
			r = d1 - first.bandwidth;
			s = d2 - second.bandwidth;
			rAndS = r + s;
			fillDiag = rAndS + bandwidth; //in internal numbering
			len = size - abs(rAndS);
			if(fillDiag>=0 && fillDiag < numDiag){
				//discriminate cases
				if(r >= 0 && s >= 0){ //r,s >= 0
					i1 = 0;
					i2 = r;
					ir = 0;
				} else if(r <= 0 && s <= 0){ //r,s, <= 0
					i1 = -s;
					i2 = 0;
					ir=0;
				} else if(r < 0){
					i1 = 0;
					i2 = 0;
					if(r+s < 0){ //r < 0, s > 0, r+s < 0
						ir = len - (size + r);
					} else { //r < 0, s > 0, r+s >= 0
						ir = len - (size - s);
					}
				} else {
					ir = 0;
					if(r+s < 0){ //r > 0, s < 0, r+s < 0
						i1 = abs(rAndS);
						i2 = 0;
						len = size + s;
					} else { //r > 0, s < 0, r+s >= 0
						i1 = 0;
						i2 = abs(rAndS);
						len = size - r;
					}
				}
				//now fill diagonal
				for(; ir<len; ++ir, ++i1, ++i2){
					data[fillDiag][ir] += first.data[d1][i1] * second.data[d2][i2];
				}
			}
		}
	}
}

bool TBandMatrixStorage::fillAsExponential(TBandMatrixStorage & Q){
	// Choose k such that 3 * 2^k > numStates AND 1/2^k < 0.1 (i.e. k>3)
	int k = Q.size / 3.0;
	k = log(k)/log(2) + 10;
	if(k<4) k=4;

	fillAsExponential(Q, k);

	//check if k was sufficient
	while(max() > 1.01 || min() < -0.01){
		if(k > 100) return false;
		k += 10;
		fillAsExponential(Q, k);
	}
	return true;
}

void TBandMatrixStorage::fillAsExponential(TBandMatrixStorage & Q, int k){
	//We get exp(Q) using a poor man's algorithm:
	// 1) multiply all entries by 1/(2^k)
	// 2) Square the matrix (I+Q*scale) k times

	// 1) Divide all entries by 1 / (2^k)
	double scale = 1.0 / (double) pow(2, k);
	TBandMatrixStorage** QScaledBand = new TBandMatrixStorage*[2];
	QScaledBand[0] = new TBandMatrixStorage(Q, scale);
	QScaledBand[0]->addToDiagonal(1.0);
	QScaledBand[1] = new TBandMatrixStorage();
	int QScaledBandIndex = 0;

	// 2) Square matrix k times.
	//Change from bandMarix to Matrix once the diagonals are filled
	for(int i=0; i<(k-1); ++i){
		QScaledBandIndex = 1 - QScaledBandIndex;
		QScaledBand[QScaledBandIndex]->fillFromSquare(*QScaledBand[1 - QScaledBandIndex]);
	}

	//last into this storage
	fillFromSquare(*QScaledBand[QScaledBandIndex]);

	//clean up memory
	delete QScaledBand[0];
	delete QScaledBand[1];
	delete[] QScaledBand;
}



