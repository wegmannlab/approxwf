/*
 * TTimePoints.h
 *
 *  Created on: May 28, 2014
 *      Author: wegmannd
 */

#ifndef TTIMEPOINTS_H_
#define TTIMEPOINTS_H_

#include <map>
#include <vector>
#include "TTransitionMatrix.h"
#include "stringFunctions.h"

class TStatSettings{
public:
	double min_freq_fs;
	double max_freq_fs;
	double fs_threshold_l;
	double fs_threshold_h;
	double freq_threshold_l;
	double freq_threshold_h;

	TStatSettings(){
		min_freq_fs = 0.001;
		max_freq_fs = 1.0 - min_freq_fs;
		fs_threshold_l = 0.05;
		fs_threshold_h = 1.0 - fs_threshold_l;
		freq_threshold_l = 0.05;
		freq_threshold_h = 1.0 - freq_threshold_l;
	};
	void initialize(double & Min_freq_fs, double & Fs_threshold, double & Freq_threshold_l){
		min_freq_fs = Min_freq_fs;
		max_freq_fs = 1.0 - min_freq_fs;
		fs_threshold_l = Fs_threshold;
		fs_threshold_h = 1.0 - fs_threshold_l;
		freq_threshold_l = Freq_threshold_l;
		freq_threshold_h = 1.0 - freq_threshold_l;
	};
};

class TEmissionProbs{
public:
	int numStates;
	double* emissionLog;
	double* emission;
	bool storageAllocated;
	double zeroLog;
	double maxForScalingLog, maxForScaling;
	bool scalingset;

	TEmissionProbs();
	virtual ~TEmissionProbs(){
		delete[] emissionLog;
		delete[] emission;
	};
	void allocate(int NumStates);
	void determineScaling();
	void setScaling(double & logScale);
	virtual void update(int & numA, int & numB, TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator);
	double& at(int i){return emission[i];};
	double& atLog(int i){return emissionLog[i];};
	double& getScale(){return maxForScaling;};
	double& getScaleLog(){return maxForScalingLog;};
};

class TEmissionProbsBetaBinom:public TEmissionProbs{
public:

	TEmissionProbsBetaBinom();
	~TEmissionProbsBetaBinom(){	};

	void update(int & numA, int & numB, TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator);
};


class TTimePoint{
public:
	double one;
	double zero;
	bool hasSamples;
	int deltaT;
	int numChromosomes;
	bool isFirstInSeries; //mark if a time point is the first in a series (e.g. first of an experimental replicate)

	TTimePoint(){hasSamples=false;one=1.0;zero=0.0;deltaT=-1;numChromosomes=0;isFirstInSeries=false;};
	virtual ~TTimePoint(){};
	virtual void updateBinomialCoefficients(TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator){};
	virtual void resetBinomialCoefficients(){};
	virtual double& getEmissionScaleLog(){return zero;};
	virtual double& getEmission(int & k){return one;}
	virtual double& getEmissionLog(int & k){return zero;}
	virtual std::string getString(){ return "NA"; }
	virtual bool hasSelectedSaples(){return false;};
	virtual void simulateData(double & frequency, double & errorRate, TRandomGenerator* RandomGenerator){};
	virtual double getAlleleFrequency(){ throw "Unable to calculate allele frequency at a time point without samples!";};
	virtual void setNumA(int & numA){};
	bool HasSamples(){ return hasSamples; };
	int getNumChromosomes(){return numChromosomes;};
	virtual int getNumA(){return 0;};
	void setIsFirstInSeries(){ isFirstInSeries = true; };
};

class TTimePointWithSamples:public TTimePoint{
public:
	int numA;
	int numB;
	int numStates;
	TEmissionProbsBetaBinom* EmissionProbsCur;
	TEmissionProbsBetaBinom* EmissionProbsOld;
	bool binomFilled;

	TTimePointWithSamples(int NumChromosomes, int NumA);
	//TTimePointWithSamples(TTimePointWithSamples&&) = default;
	~TTimePointWithSamples(){
		delete EmissionProbsCur;
		delete EmissionProbsOld;
	}
	void updateBinomialCoefficients(TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator);
	void resetBinomialCoefficients();
	double& getEmissionScaleLog(){return EmissionProbsCur->getScaleLog();};
	double& getEmission(int & k){return EmissionProbsCur->at(k);}
	double& getEmissionLog(int & k){return EmissionProbsCur->atLog(k);}
	std::string getString(){  return toString(numA) + "/" + toString(numChromosomes); };
	bool hasSelectedSaples(){ if(numA>0) return true; else return false;};
	void simulateData(double & frequency, double & errorRate, TRandomGenerator* RandomGenerator);
	double getAlleleFrequency();
	void setNumA(int & NumA);
	virtual int getNumA(){return numA;};
};


class TTimePointVector{
private:

	TTimePoint** pointerToTimepoints;
	TTimePoint** pointerToTimepointsWithSamples;
	bool pointersInitialized;
	int interval;
	int firstTimepointWithSamples;
	bool hasSomeSamples;
	bool hasMutationTime;
	bool emissionprobabilitiesInitialized;
	//iterator
	std::map<int, TTimePoint*>::iterator it;
	std::map<int, TTimePoint*>::reverse_iterator rIt;
	//statistics
	//ToDo: add derived class with stats to save memory in base class
	double fsi, fsd, fsi_l, fsd_l, t_h, t_l;
	bool nextTimepointIsFirstInSeries;


public:

	std::map<int,TTimePoint*> timePoints;

	int numTimePoints;
	int numTimepointsWithSamples;

	TTimePointVector();
	TTimePointVector(TTimePointVector&& other);
	TTimePointVector& operator=(TTimePointVector&& other);
	~TTimePointVector(){
		for(it=timePoints.begin(); it!=timePoints.end(); ++it) delete it->second;
		if(pointersInitialized){
			delete[] pointerToTimepoints;
			delete[] pointerToTimepointsWithSamples;
		}
	};
	void init();
	void setNextIsFirstInSeries();
	void setAsFirstInSeries(int TimePoint);
	void setInterval(int MutTime, int Interval);
	void update(int MutTime);
	void addTimepoint(int & TimePoint, std::string & Samples, bool isFirstInSeries=false);
	void addTimepoint(int & TimePoint, int & numA, int & size, bool isFirstInSeries=false);
	int getMinNumTimepointsAllelesAreObserved(double & minFreq);
	void initDeltaT();
	bool& hasSamples(){return hasSomeSamples;};
	void prepareEmissionProbabilities(TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator);
	void updateEmissionProbabilities(TStatesU* U, double & errorRate, TRandomGenerator* RandomGenerator);
	void resetEmissionProbabilities();
	double& getEmissionProb(int & timePoint, int & k);
	double& getEmissionProbLog(int & timePoint, int & k);
	int& getFirstTimePointsWithSamples(){return firstTimepointWithSamples;};
	void beginInteration(){it=timePoints.begin();};
	void rbeginInteration(){rIt=timePoints.rbegin();};
	bool nextCheck(){++it; if(it==timePoints.end()) return false; else return true;};
	bool previousCheck(){++rIt; if(rIt==timePoints.rend()) return false; else return true;};
	void previous(){++rIt;};
	int& currentDeltaT(){return it->second->deltaT;};
	int& rCurrentDeltaT(){return rIt->second->deltaT;};
	bool& currentHasSamples(){return it->second->hasSamples;}
	double& currentEmissionProb(int & k){return it->second->getEmission(k);};
	double& currentEmissionScaleLog(){return it->second->getEmissionScaleLog();};
	void currentSimulateData(double frequency, double & errorRate, TRandomGenerator* RandomGenerator);
	void currentSetData(int numA);
	bool curIsFirstInSeries(){return it->second->isFirstInSeries;};
	std::string getSampleStringAt(const int & TimePoint);
	int getSampleSizeAt(const int & TimePoint);
	int getNumAAt(const int & TimePoint);
	void calcStats(TStatSettings & settings);
	void writeStats(std::ofstream & file);
	void writeStatHeader(std::ofstream & file, std::string & tag);
	double get_fsi(){ return fsi; };
	double get_fsd(){ return fsd; };
};


#endif /* TTIMEPOINTS_H_ */
