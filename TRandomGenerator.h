#ifndef TRandomGenerator_H_
#define TRandomGenerator_H_

#include <cmath>
//#include <time.h>
#include <sys/time.h>
#include <iostream>

class TRandomGenerator{
public:
	long usedSeed;

	TRandomGenerator(long addToSeed){
		init(addToSeed);
	};
	TRandomGenerator(long addToSeed, bool seedIsFixed){
		if(!seedIsFixed) init(addToSeed);
		else {
			if(addToSeed<0) addToSeed=-addToSeed;
	        usedSeed=addToSeed;
	        _Idum=-addToSeed;
		}
	};
	TRandomGenerator(){
			init(0);
	};
	~TRandomGenerator(){};
	void init(long addToSeed);
	double getRand(){ return ran3(); };
	double getRand(double min, double max);
	int getRand(int min, int maxPlusOne);
	long getRand(long min, long maxPlusOne);
	double getNormalRandom (double dMean, double dStdDev);
	long get_randomSeedFromCurrentTime(long & addToSeed);
	double getBiomialRand(double pp, int n);
	double gammaln(const double xx);
	double binomCoeffLn(int n, int k);
	double getGammaRand(double a, double b);
	double getGammaRand(double a);
	double getGammaRand(int ia);
	double betaln(const double x, const double y);
	double getBetaRandom (double alpha, double beta, double a, double b);
	double getBetaRandom (double alpha, double beta);
	double getPoissonRandom(double lambda);

private:
	long _Idum;
	double ran3();
};

#endif /* TRandomGenerator_H_ */
